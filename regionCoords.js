
module.exports = {

  REGIONS: [

    // index 0
    {
      label: 'Lazio',
      names: ['laz','lazio'],
      lat: '41.881422',
      lon: '12.461445'
    },

    {
      label: 'Abruzzo',
      names: ['abr','abruzzo'],
      lat: '42.257622',
      lon: '13.947026'
    },

    {
      label: 'Basilicata',
      names: ['bas','basilicata'],
      lat: '40.562365',
      lon: '16.448761'
    },

    {
      label: 'Calabria',
      names: ['cal','calabria'],
      lat: '39.243393',
      lon: '16.563521'
    },

    {
      label: 'Campania',
      names: ['cam','campania'],
      lat: '40.866092',
      lon: '14.269316'
    },

    {
        label: 'Emilia Romagna',
        names: ['emr', 'emilia', 'romagna', 'emilia romagna'],
        lat: '44.409479',
        lon: '11.207055'
    },

    {
        label: 'Friuli Venezia Giulia',
        names: ['friuli venezia giulia'],
        lat: '46.193747',
        lon: '13.078144'
    },



    {
      label: 'Liguria',
      names: ['liguria', 'lig'],
      lat: '44.405016',
      lon: '9.007620'
    },

    {
      label: 'Lombardia',
      names: ['lombardia','lombardy'],
      lat: '45.589052',
      lon: '9.195408'
    },

    {
      label: 'Marche',
      names: ['marche'],
      lat: '43.617025',
      lon: '13.351805'
    },

    {
      label: 'Molise',
      names: ['mol','molise'],
      lat: '41.688536',
      lon: '14.629198'
    },

    {
      label: 'Piemonte',
      names: ['piemonte'],
      lat: '45.062852',
      lon: '7.798929'
    },

    {
      label: 'Puglia',
      names: ['puglia'],
      lat:  '41.128471',
      lon: '16.793198'
    },

    {
      label: 'Sardegna',
      names: ['sardegna'],
      lat: '40.060441',
      lon: '9.042748'
    },

    {
      label: 'Sicilia',
      names: ['sic','sicilia','sicily'],
      lat: '37.570503',
      lon: '14.189107'
    },

    {
      label: 'Toscana',
      names: ['tos','toscana'],
      lat: '43.667100',
      lon: '11.086205'
    },

    {
        label: 'Trentino Alto Adige',
        names: ['trentino-alto adige', 'trentino alto adige'],
        lat: '46.603435',
        lon: '11.602563'
    },

    {
        label: 'Umbria',
        names: ['umb','umbria'],
        lat: '43.106779',
        lon: '12.400921'
    },

    {
        label: 'Veneto',
        names: ['veneto'],
        lat: '45.457315',
        lon: '11.235892'
    },

    {
      label: 'Valle d\'Aosta',
      names: ["valle d'aosta"],
      lat: '45.762885',
      lon: '7.417380'
    },
	
    {
      label: 'Italy',
      names: ["italy", "italie"],
      lat: '42.767998',
      lon: '12.801944'
     // lat: '47.176023',
     // lon: '2.814544'	  
    },

    {
      label: 'France',
      names: ["france"],
      lat: '47.176023',
      lon: '2.814544'
    },	
	
    {
      label: 'United States of America',
      names: ["united states of america"],
      lat: '36.163360',
      lon: '-80.230804'
    }
	
  ]

}
