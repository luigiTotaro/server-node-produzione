
module.exports = {

    SOCKET_EVENTS: {
        MESSAGE: 'MESSAGE',
        CONNECT: 'CONNECT',
        DISCONNECT: 'DISCONNECT'
    },
    repository_root: 'repository',
    mongodb_url: 'mongodb://localhost:27017',
    socket_port: 3030,
    //bind_ip: '94.177.167.64'
    bind_ip: '0.0.0.0',

    mysql_connections: {

        mysql_gz: {
            host     : '46.37.4.164',
            port     : 3306,
            user     : 'root',
            password : 'pro90000',
            database : 'livengine'
        }

    },
    

    // ###################################### //
    // applicazioni gestite dall'istanza node //
    // ###################################### //
    appConfigurations: {

        //  *****  MONDADORI  *****
        
        giallo_zafferano: {

            label: 'Giallo Zafferano',
            bundleId: "com.mediasoft.lve.mondadori",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }

        },
        
        tv_sorrisi_canzoni: {

            label: 'Tv Sorrisi e Canzoni',
            bundleId: "com.mediasoft.lve.mondadori",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedItemsCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }

        },

        focus_junior: {

            label: 'Focus Junior',
            bundleId: "com.mediasoft.lve.mondadori",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedItemsCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }

        },

        chi: {

            label: 'Chi',
            bundleId: "com.mediasoft.lve.mondadori",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }
        },

        donna_moderna: {

            label: 'Donna Moderna',
            bundleId: "com.mediasoft.lve.mondadori",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }
        },


        // *****  CLAIM EXPERT  ***** 

        claimexpert_ramovita: {

            label: 'Ramo Vita',
            bundleId: "com.mediasoft.lve.clex",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }
        }, 

        
        // *****  MEDIASOFT  ***** 

        ewo: {
            label: 'ewo',
            bundleId: "com.mediasoft.lve.arplus"
        },
        
        good_life: {
            label: 'Good Life',
            bundleId: "com.mediasoft.lve.arplus"
        },

        internazionale: {
            label: 'Internazionale',
            bundleId: "com.mediasoft.lve.arplus"
        },

        muse: {
            label: 'Muse',
            bundleId: "com.mediasoft.lve.arplus"
        },

        chi_mediasoft: {
            label: 'Chi',
            bundleId: "com.mediasoft.lve.arplus"
        },
         
        lefrecce: {
            label: 'Le Frecce',
            bundleId: "com.mediasoft.lve.arplus"
        },            
        
	quattroruote: {
            label: 'Quattroruote',
            bundleId: "com.mediasoft.lve.arplus"
        },            

        eni: {
            label: 'Eni',
            bundleId: "com.mediasoft.lve.arplus"
        },            

        farmamagazine: {
            label: 'Farmamagazine',
            bundleId: "com.mediasoft.lve.arplus"
        },            
        
        eurospin: {
            label: 'Eurospin',
            bundleId: "com.mediasoft.lve.arplus"
        },            

        trentino: {
            label: 'Trentino',
            bundleId: "com.mediasoft.lve.arplus"
        },  

        modulistica: {
            label: 'modulistica',
            bundleId: "com.mediasoft.lve.arplus"
        },

        pegaso: {
            label: 'Pegaso',
            bundleId: "com.mediasoft.lve.arplus"
        },            

        vinitaly: {
            label: 'Vinitaly',
            bundleId: "com.mediasoft.lve.arplus"
        },         

		quorumitalia: {
            label: 'Quorum Italia',
            bundleId: "com.mediasoft.lve.arplus"
        },   

        // *****  MEDIASOFT US  ***** 

        wake_forest: {
            label: 'Wake Forest',
            bundleId: "com.mediasoft.lve.usar"
        },


        // *****  MEDIASOFT FR  ***** 

        mondadori_fr: {
            label: 'Mondadori Fr',
            bundleId: "com.mediasoft.lve.frar"
        },            


        // *****  ALTAVIA  ***** 
		
        vivi_di_gusto: {
            label: 'Vivi di gusto',
            bundleId: "com.mediasoft.lve.altv"
        },           
        
        // *****  ALTAVIA  ***** 
        
        moments: {
            label: 'Moments',
            bundleId: "com.mediasoft.lve.moments",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }
        },

        // ***** PROMOWE - BLALA  ***** 

        blala: {

            label: 'B.La.La',
            bundleId: "com.mediasoft.lve.fft",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedRecipeCounter',
                    //'ExportGameStats',
                    //'ExportGameWinners',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }
        },

	// ***** BFC ******


        cosmo: {

            label: 'Cosmo',
            bundleId: "com.mediasoft.lve.dam",

            exports: {
                mysql_db: 'mysql_gz',
                commands: [
                    'AllAccessiDevice',
                    'AllAccessi',
                    'RivisteViewCounter',
                    'AccessiByRegion',
                    'AccessiByDevice',
                    'ContenutiAumentatiViewCounter',
                    'MappeConsultateViewCounter',
                    'SavedItemsCounter',
                    'ExportGameStats',
                    'ExportGameWinners',
                    //'ExportGameEndings',
                    'EmptyEvents',
                    'ResetAccessActivityTime'
                ]
            }

        }
 


    }
    // ###################################### //
    // applicazioni gestite dall'istanza node //
    // ###################################### //

}
