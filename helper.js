var Config = require('./config');
var DatabaseManager = require('./databaseManager');
var fs = require('fs');
var winston = require('winston');
var Path = require('path');
var apn = require('apn');
var gcm = require('node-gcm');
var _ = require('underscore');

function Helper(express, namespaces) {
    this.express = express;
    this.namespaces = namespaces;
    this.Initialize();
}

String.prototype.bool = function() {
    return (/^true$/i).test(this);
};


Helper.prototype.Initialize = function() {

  var self = this;

  self.last_push_notification_time = [];
  self.start = [];
  self.limit = [];

  self.prjId = {
      data: [],
      creation: []
  };

  self.usersCount = {
      totale: [],
      creation: []
  };

  self.ApnPushNotification = new apn.Provider({

       token: {
          // Path to the key p8 file
          key: fs.readFileSync(Path.normalize(__dirname + "/apn_certificates/APNsAuthKey.p8")),
          // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
          keyId: 'MSENBG3NY2',
          // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
          teamId: '23G63WKZ5A'
      },
      // Set to true if sending a notification to a production iOS app
      production: true

  });



  self.GcmPushNotification = new gcm.Sender('AAAAqfImdbM:APA91bHXCshcUHIQeM15fOSKTmMBBb19S3LyikQMJtFxH5M5XyVGgyEG7hVm4SImGnlmFwZAHpzLv6C7ic_pN4ml_1kHfBtcoTP8eH2WutuJqRwtCXwp9qsCix6ubROlgwoS0asnXaqN');

  
  self.express.get('/:application/:message/test_push_gcm', function(req, res) {

      if(!Config.appConfigurations[req.params.application]) {
          res.status(404).send('Endpoint "' + req.params.application + '" not found');
          return;
      }
 
	console.log("TEST SEND PUSH NOTIFICATION TO ANDROID");


	  var tokens = ['dMJuLzlqd5Q:APA91bFOa8-AF9xachrBLPG7rQeeVSEZKMGSILBPeK99IwGZ_WVrVxRPSQFarge--fKtv99DmcMn3zjwfQR04AQN8nnbg5fImxok4vcGFZPwcnCtUfF19fC-h7UWK9qjpL3j9Rw1H7Vd'];

 

		var message = new gcm.Message({
			timeToLive: 3,
			restrictedPackageName: "com.mediasoft.lve.mondadori",
			//dryRun: true,
			notification: {
				title: Config.appConfigurations[ req.params.application ].label,
				icon: "ic_launcher",
				body: req.params.message
			}
		});

		console.log("sending " + tokens.length + " messages to android");

		self.GcmPushNotification.send(message, { registrationTokens: tokens }, function (err, response) {
			if (err)
			  console.error(err);
			else
			  console.log(response);
		});
 

      res.send('ok');

  });
  
  /*
  self.express.get('/:application/:message/push_apn', function(req, res) {

      if(!Config.appConfigurations[req.params.application]) {
          res.status(404).send('Endpoint "' + req.params.application + '" not found');
          return;
      }

      DatabaseManager.getTable(req.params.application, 'access', function(err, table) {

          if(!err) {

              table.find({
                // find section
                os: 'ios',
                pushId:{$ne:null}

              }).toArray(function(err, docs) {

                  var tokens = [];

                  _.each(docs, function(access_data) {
                      if(access_data.pushId) {
                          tokens.push(access_data.pushId);
                      }
                  });


                  if(tokens.length > 0) {

                    var notification = new apn.Notification({
                        topic: 'com.mediasoft.lve.mondadori',
                        expiry:  Math.floor(Date.now() / 1000) + 3600,
                        badge: 1,
                        sound: 'ping.aiff',
                        alert: req.params.message,
                        title: Config.appConfigurations[ application_name ].label
                    });

                    console.log("sending " + tokens.length + " messages to ios");

                    self.ApnPushNotification.send(notification, tokens).then(function(result) {
                        console.log(result);
                    });

                  }


              });

          }

      });

      res.send('ok');

  });
  */





  // parameters.application_name
  // parameters.tokens[]
  // parameters.message
  // parameters.onComplete
  self.sendApnPushNotification = function(parameters) {

      if(parameters.tokens.length == 0) {

          parameters.onComplete();

      } else {

        var notification = new apn.Notification({
          topic: parameters.packageId,
          expiry:  Math.floor(Date.now() / 1000) + 3600,
          //badge: 1,
          sound: 'ping.aiff',
          alert: parameters.message,
          title: Config.appConfigurations[ parameters.application_name ].label
        });

        console.log("Mando il messaggio ai terminali ios. PackageId: " + parameters.packageId);
        
        self.ApnPushNotification.send(notification, parameters.tokens).then(function(result) {

          parameters.onComplete();

        });

      }

  };


  // parameters.application_name
  // parameters.tokens[]
  // parameters.message
  // parameters.onComplete
  self.sendGcmPushNotification = function(parameters) {

      if(parameters.tokens.length == 0) {

          parameters.onComplete();

      } else {

        var message = new gcm.Message({
          timeToLive: 3,
          restrictedPackageName: parameters.packageId,
          notification: {
              title: Config.appConfigurations[ parameters.application_name ].label,
              icon: "ic_launcher",
              body: parameters.message
          }
        });

        console.log("Mando il messaggio ai terminali android. PackageId: " + parameters.packageId);

        
        self.GcmPushNotification.send(message, { registrationTokens: parameters.tokens }, function (err, response) {

            if(err) {
              console.error(err);
              winston.error("sendGcmPushNotification: " , err);
            }

            parameters.onComplete();

        });
        
      }

  };



  self.processPushQueue = function(params) {

      DatabaseManager.getTable(params.application_name, 'access', function(err, table) {

          if(!err) {
			  

              table.find({
                pushId:{
					$ne:null
				}
              })
              .skip(self.start[params.application_name])
              .limit(self.limit[params.application_name])
              .toArray(function(err, docs) {

                  var ios_tokens = [];
                  var android_tokens = [];
                
                  _.each(docs, function(access_data) {
                      if(access_data.pushId) {
                        switch(access_data.os) {
                          case 'ios':
                            ios_tokens.push(access_data.pushId);
                            break;
                          case 'android':
                            android_tokens.push(access_data.pushId);
                            break;
                        }
                      }
                  });


                  self.sendGcmPushNotification({
                      application_name: params.application_name,
                      packageId: params.packageId,
                      tokens: android_tokens,
                      message: params.message,
                      onComplete: function() {

                          self.sendApnPushNotification({
                              application_name: params.application_name,
                              packageId: params.packageId,
                              tokens: ios_tokens,
                              message: params.message,
                              onComplete: function() {

                                  if(docs.length == self.limit[params.application_name]) {

                                      // devo fare un'altro fetch
                                      self.start[params.application_name] += self.limit[params.application_name];
                                      self.processPushQueue(params);

                                  } else {

                                      // ho finito
                                      params.onComplete();

                                  }

                              }
                          });

                      }
                  });
				  



              });

          }

      });

  };






  self.express.get('/:application/:message/push', function(req, res) {

        if(!Config.appConfigurations[req.params.application]) {
            res.status(404).send('Endpoint "' + req.params.application + '" not found');
            return;
        }

        var NOW = (new Date()).getTime();

        if(self.last_push_notification_time[req.params.application] && (NOW - self.last_push_notification_time[req.params.application]) < 10000) {
            res.status(404).send('Non puoi eseguire ora una nuova notifica push su ' + req.params.application);
            return;
        }

        self.last_push_notification_time[req.params.application] = NOW;
        self.start[req.params.application] = 0;
        self.limit[req.params.application] = 1000;

        //trovo il package abbinato a questa testata (application)


        self.processPushQueue({
            application_name: req.params.application,
            packageId: Config.appConfigurations[ req.params.application ].bundleId,
            startTime: NOW,
            tLimit: NOW - (1000 * 60 * 60 * 24 * 365),
            message: req.params.message,
            onComplete: function() {
                console.log("complete query");
            }
        });

        res.send( 'ok' );

  });


  self.express.get('/usersCountGlobal', function(req, res) {

    self.userCount = {
        data: {}
    };
    
    self.userCount.data = {
        total: 0,
        testate_list: []
    };


    for(var Namespace in self.namespaces) {
      
      var tmp;
      tmp = {
          totale: Object.keys(self.namespaces[Namespace].sockets).length,
          testata: Namespace
      }
      self.userCount.data.testate_list.push(tmp);

      self.userCount.data.total += Object.keys(self.namespaces[Namespace].sockets).length;
      
      //console.log(Namespace);
      //console.log(Object.keys(self.namespaces[Namespace].sockets).length);
    }

    res.status(200).send(self.userCount.data);    

  });



  self.express.get('/:application/usersCount', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var NOW = (new Date()).getTime();

    if (self.usersCount.creation[req.params.application]) //se esiste quella chiave
    {
      if((NOW - self.usersCount.creation[req.params.application]) > 20000) 
      {
            if (Object.keys(self.namespaces[req.params.application].sockets).length==0)
            {
              self.usersCount.totale[req.params.application]=0;
              self.usersCount.creation[req.params.application]= 0; //in modo che una prossima richiesta, se ci sta almenu un utente, venga soddisfatta, e non rimanga 0 per molto tempo
            }
            else
            {
              self.usersCount.totale[req.params.application]= (Object.keys(self.namespaces[req.params.application].sockets).length*2)+Math.floor((Math.random() * 5) + 0);
              self.usersCount.creation[req.params.application]= NOW;
            }
       }
    }
    else
    {
            if (Object.keys(self.namespaces[req.params.application].sockets).length==0)
            {
              self.usersCount.totale[req.params.application]=0;
              self.usersCount.creation[req.params.application]= 0; //in modo che una prossima richiesta, se ci sta almenu un utente, venga soddisfatta, e non rimanga 0 per molto tempo
            }
            else
            {
              self.usersCount.totale[req.params.application]= (Object.keys(self.namespaces[req.params.application].sockets).length*2)+Math.floor((Math.random() * 5) + 0);
              self.usersCount.creation[req.params.application]= NOW;
            }
    }

    res.status(200).send({
      totale: self.usersCount.totale[req.params.application]
    });

  });



  self.express.get('/:application/usersCountByPrjId', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var NOW = (new Date()).getTime();

    if (self.prjId.creation[req.params.application]) //se esiste quella chiave
    {
      if((NOW - self.prjId.creation[req.params.application]) > 20000) 
      {
        
        self.prjId.creation[req.params.application] = NOW;
        
        self.prjId.data[req.params.application] = {
            total: Object.keys(self.namespaces[req.params.application].sockets).length,
            prjId_list: []
        };

        var tmp = [];

        _.each(self.namespaces[req.params.application].sockets, function(currentClientConnection) {

            var lista = _.keys(currentClientConnection.applicationData.rooms);
            if (lista.length>0)
            {
                var prjId=lista[0];
                if(!tmp[ prjId ]) {

                    tmp[ prjId ] = {
                        totale: 0,
                        name: prjId.substring(14), //tolgo la scritta "room_progetto_"
                    }
                }
                tmp[ prjId ].totale++;
            }
        });

        for(var name in tmp) {
          tmp[name].totale=(tmp[name].totale*2)+Math.floor((Math.random() * 5) + 0);
          self.prjId.data[req.params.application].prjId_list.push(tmp[name]);
        }
      }
    }
    else
    {
        self.prjId.creation[req.params.application] = NOW;
        
        self.prjId.data[req.params.application] = {
            total: Object.keys(self.namespaces[req.params.application].sockets).length,
            prjId_list: []
        };

        var tmp = [];

        _.each(self.namespaces[req.params.application].sockets, function(currentClientConnection) {

            var lista = _.keys(currentClientConnection.applicationData.rooms);
            if (lista.length>0)
            {
                var prjId=lista[0];
                if(!tmp[ prjId ]) {

                    tmp[ prjId ] = {
                        totale: 0,
                        name: prjId.substring(14), //tolgo la scritta "room_progetto_"
                    }
                }
                tmp[ prjId ].totale++;
            }
        });

        for(var name in tmp) {
          tmp[name].totale=(tmp[name].totale*2)+Math.floor((Math.random() * 5) + 0);
          self.prjId.data[req.params.application].prjId_list.push(tmp[name]);
        }
      
    }

    res.status(200).send(self.prjId.data[req.params.application]);

  });


  self.express.get('/:application/usersCountByPrjId_TEST', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var NOW = (new Date()).getTime();

	console.log(self.prjId.creation);
    if (self.prjId.creation[req.params.application]) //se esiste quella chiave
    {
      console.log("la chiave esiste");
	  if((NOW - self.prjId.creation[req.params.application]) > 1000) 
      {
        
        self.prjId.creation[req.params.application] = NOW;
        
        self.prjId.data[req.params.application] = {
            total: Object.keys(self.namespaces[req.params.application].sockets).length,
            prjId_list: []
        };
		console.log(self.prjId.data);

        var tmp = [];
		//console.log("sockets dell'applicazione "+ req.params.application);
		//console.log(self.namespaces[req.params.application].sockets);

        _.each(self.namespaces[req.params.application].sockets, function(currentClientConnection) {
			//console.log("currentClientConnection");
			//console.log(currentClientConnection);

            var lista = _.keys(currentClientConnection.applicationData.rooms);
			console.log("lista");
			console.log(lista);
            if (lista.length>0)
            {
                var prjId=lista[0];
                if(!tmp[ prjId ]) {

                    tmp[ prjId ] = {
                        totale: 0,
                        name: prjId.substring(14), //tolgo la scritta "room_progetto_"
                    }
                }
                tmp[ prjId ].totale++;
            }
        });

        for(var name in tmp) {
          tmp[name].totale=(tmp[name].totale*2)+Math.floor((Math.random() * 5) + 0);
          self.prjId.data[req.params.application].prjId_list.push(tmp[name]);
        }
      }
    }
    else
    {
		console.log("la chiave non esiste");
        self.prjId.creation[req.params.application] = NOW;
        
		console.log("Creo la chiave...");
        self.prjId.data[req.params.application] = {
            total: Object.keys(self.namespaces[req.params.application].sockets).length,
            prjId_list: []
        };
		console.log(self.prjId.data);

        var tmp = [];

		//console.log("sockets dell'applicazione "+ req.params.application);
		//console.log(self.namespaces[req.params.application].sockets);

        _.each(self.namespaces[req.params.application].sockets, function(currentClientConnection) {

			//console.log("currentClientConnection");
			//console.log(currentClientConnection);
           
			var lista = _.keys(currentClientConnection.applicationData.rooms);
			console.log("lista");
			console.log(lista);


            if (lista.length>0)
            {
                var prjId=lista[0];
                if(!tmp[ prjId ]) {

                    tmp[ prjId ] = {
                        totale: 0,
                        name: prjId.substring(14), //tolgo la scritta "room_progetto_"
                    }
                }
                tmp[ prjId ].totale++;
            }
        });

        for(var name in tmp) {
          tmp[name].totale=(tmp[name].totale*2)+Math.floor((Math.random() * 5) + 0);
          self.prjId.data[req.params.application].prjId_list.push(tmp[name]);
        }
      
    }

    res.status(200).send(self.prjId.data[req.params.application]);

  });


  self.express.get('/:application/config', function(req, res) {

      if(!Config.appConfigurations[req.params.application]) {
          res.status(404).send('Endpoint "' + req.params.application + '" not found');
          return;
      }

      res.send(Config.appConfigurations[req.params.application]);

  });


  self.express.get('/:application/getLibraries', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var libraryPath = Path.normalize(__dirname + "/" + Config.repository_root + "/" + req.params.application + "/client/");

    if(!fs.existsSync(libraryPath)) {
        res.status(404).send('Directory '+libraryPath+' non trovata');
        return;
    }

    var libraries = [];

    fs.readdir(libraryPath, function(err, files) {

      res.status(200).send(files);

    });



  });

  self.express.get('/:application/getARLibraries', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var libraryPath = Path.normalize(__dirname + "/" + Config.repository_root + "/" + req.params.application + "/clientAR/");

    if(!fs.existsSync(libraryPath)) {
        res.status(404).send('Directory '+libraryPath+' non trovata');
        return;
    }

    var libraries = [];

    fs.readdir(libraryPath, function(err, files) {

      res.status(200).send(files);

    });



  });


  self.express.get('/:application/:fileName/library/get', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var filePath =
      Path.normalize(__dirname + "/" + Config.repository_root + "/" + req.params.application + "/client/" + req.params.fileName);

    if(!fs.existsSync(filePath)) {
        res.status(404).send('File "' + req.params.fileName + '" non trovato per "' + req.params.application + '"');
        return;
    }

    res.download(filePath);

  });

  self.express.get('/:application/:fileName/ARlibrary/get', function(req, res) {

    if(!Config.appConfigurations[req.params.application]) {
        res.status(404).send('Endpoint "' + req.params.application + '" not found');
        console.log('Endpoint "' + req.params.application + '" not found');
        return;
    }

    var filePath =
      Path.normalize(__dirname + "/" + Config.repository_root + "/" + req.params.application + "/clientAR/" + req.params.fileName);

    if(!fs.existsSync(filePath)) {
        res.status(404).send('File "' + req.params.fileName + '" non trovato per "' + req.params.application + '"');
        console.log('File "' + req.params.fileName + '" non trovato per "' + req.params.application + '"');
        return;
    }

    console.log('Download: ' + filePath);
    res.download(filePath);

  });

  
    // ######################################################################## //
    // lista di tutti widget disponibili per livello (testata/numero/contenuto) //
    // ######################################################################## //    
    self.express.get('/:application/configurable/:level/:identifier', function(req, res) {
        
        var keys = [];      
        var widgets = [];
        
        if(!Config.appConfigurations[req.params.application]) {
            res.status(404).send('Endpoint "' + req.params.application + '" not found');
            return;
        }

        if(!(req.params.level == 'testata' || req.params.level == 'progetto' || req.params.level == 'articolo')) {
            res.status(404).send('Level "' + req.params.level + '" not recognized');
            return;
        }

        if(!req.params.identifier) {
            res.status(404).send('Identifier required');
            return;
        }
        
                
        if(!(global.configurables[ req.params.application ] 
        && global.configurables[ req.params.application ][ req.params.level ])) {
            
            res.status(200).send(widgets);
            return;
            
        }
        
        _.each(global.configurables[ req.params.application ][ req.params.level ], function(item) {     
            
            keys.push(item.key);            
            widgets.push(Object.assign({ 
              visible: false,
              forDebugOnly: false 
            },item));            
            
        });
        
        DatabaseManager.getTable(req.params.application, 'config', function(err, table) {
            
            if(err) {
                res.status(404).send(err);
                return;
            }
                        
            table.find({
                
                identifier: req.params.identifier,
                level: req.params.level,
                key: {
                    $in: keys
                }
                
            }).toArray(function(err, docs) {
                
                if(err) {
                    res.status(404).send(err);
                    return;
                }                
                
                _.each(docs, function(doc) {                    
                    var widget = _.find(widgets, function(widget) { return doc.key == widget.key; });
                    widget.values = doc.values;     
                    widget.visible = doc.visible ? doc.visible : false;
                    widget.forDebugOnly = doc.forDebugOnly ? doc.forDebugOnly : false;
                });
                
                res.status(200).send(widgets);
                
                widgets = null;
                
            });   
                                 
        });

    });
    
    
    // ###################################### //
    // recupero configurazione singolo widget //
    // ###################################### //    
    self.express.get('/:application/configurable/:level/:identifier/:key', function(req, res) {
             
        var widget = {};
        
        if(!Config.appConfigurations[req.params.application]) {
            res.status(404).send('Endpoint "' + req.params.application + '" not found');
            return;
        }

        if(!(req.params.level == 'testata' || req.params.level == 'progetto' || req.params.level == 'articolo')) {
            res.status(404).send('Level "' + req.params.level + '" not recognized');
            return;
        }

        if(!req.params.identifier) {
            res.status(404).send('Identifier required');
            return;
        }
        
        if(!req.params.key) {
            res.status(404).send('Key required');
            return;
        }        
                
        if(!(global.configurables[ req.params.application ] 
        && global.configurables[ req.params.application ][ req.params.level ])) {
            
            res.status(200).send(widget);
            return;
            
        }
        
        var match = _.find(global.configurables[ req.params.application ][ req.params.level ], function(item) {
            return item.key == req.params.key;
        });
        
        if(!match) {
            res.status(404).send('Widget con chiave "' + req.params.key + '" non trovato');
            return;
        }
        
        widget = Object.assign({ 
          visible: false, 
          forDebugOnly: false
        }, match);

        widget.values = null;
        
        DatabaseManager.getTable(req.params.application, 'config', function(err, table) {
            
            if(err) {
                res.status(404).send(err);
                return;
            }
                        
            table.findOne({
                
                identifier: req.params.identifier,
                level: req.params.level,
                key: req.params.key
                
            }, function(err, doc) {
              
                if(err) {
                    res.status(404).send(err);
                    return;
                }                
                
                if(doc) {
                    widget.values = doc.values;
                    widget.visible = doc.visible ? doc.visible : false;
                    widget.forDebugOnly = doc.forDebugOnly ? doc.forDebugOnly : false;
                }
                             
                res.status(200).send(widget); 
                widget = null;
                
            });   
                                 
        });
        

    });
    

    // ################### //
    // update di un widget //
    // ################### //
    self.express.post('/:application/configurable/:level/:identifier', function (req, res) {
        
        if(!Config.appConfigurations[req.params.application]) {
            res.status(404).send('Endpoint "' + req.params.application + '" not found');
            return;
        }

        if(!(req.params.level == 'testata' || req.params.level == 'progetto' || req.params.level == 'articolo')) {
            res.status(404).send('Level "' + req.params.level + '" not recognized');
            return;
        }

        if(!req.params.identifier) {
            res.status(404).send('Identifier required');
            return;
        }
        
        if(!req.body['key']) {
            res.status(404).send('Widget Key required');
            return;
        }
        
        DatabaseManager.getTable(req.params.application, 'config', function(err, table) {
            
            if(err) {
                res.status(404).send(err);
                return;
            }                                    
            
            table.findOneAndUpdate({
              
                // find section
                identifier: req.params.identifier,
                level: req.params.level,
                key: req.body['key']

              }, {

                  // modify section
                  $set: {
                        identifier: req.params.identifier,
                        level: req.params.level,
                        key: req.body['key'],
                        values: req.body['values'],
                        visible: req.body['visible'] ? req.body['visible'].bool() : false,
                        forDebugOnly: req.body['forDebugOnly'] ? req.body['forDebugOnly'].bool() : false
                  }

              }, {
                // options section
                upsert: true
              }, function(err, result) {
                
                if(err) {
                    res.status(404).send(err);
                } else {
                    res.status(200).send('Operation Completed');
                }
                
            });
            
                                 
        });
                    
    });    




    self.express.post('/:application/user_info/', function (req, res) {
        
        if(!Config.appConfigurations[req.params.application]) {
            res.status(404).send('Endpoint "' + req.params.application + '" not found');
            return;
        }
        

        DatabaseManager.getTable(req.params.application, 'access', function(err, table) {
            
            if(err) {
                res.status(404).send(err);                
                return;
            }                                    
            
            table.findOneAndUpdate({
                
                uid: req.body['uid']

              }, {

                  // modify section
                  $set: {
                      email: req.body['email']                        
                  }

              }, {
                // options section
                upsert: true
              }, function(err, result) {
                
                if(err) {

                    res.status(500).send(err);

                } else {

                    res.status(200).send('Operation Completed');
                     
                }
                
            });
            
                                 
        });
                    
    });    
    
  
  self.express.get('/:application/getAccess', function(req, res) {

      if(!Config.appConfigurations[req.params.application]) {
          res.status(404).send('Endpoint "' + req.params.application + '" not found');
          return;
      }

      var ret="";

      DatabaseManager.getTable(req.params.application, 'access', function(err, table) {

          //console.log("ho preso il db");
          if(!err) {

              table.find({
              }).toArray(function(err, docs) {

                  ret+='UID,OS,VER,APPVERSION,LAST_ACTIVITY_TIME,TOTAL_ACTIVITY_TIME,REGIONE<br>';
                  _.each(docs, function(access_data) {
                      var regione;
                      try {
                        regione=access_data.position.regione.name;
                      } catch(err) {
                        regione="---";
                      }
                      
                      var tempRet="";
                      try {
                        tempRet=access_data.uid+','+access_data.os+','+access_data.version+','+access_data.appVersion+','+access_data.last_activity_time+','+access_data.total_activity_time+','+regione+'<br>';
                      } catch(err) {
                        tempRet="Errore recuperando una riga:"+err+",,,,,<br>";
                      }
                      ret+=tempRet;
                  });

                  
                  res.send(ret);

              });

          }

      });

  });   


  self.express.get('/:application/getEvents', function(req, res) {

      if(!Config.appConfigurations[req.params.application]) {
          res.status(404).send('Endpoint "' + req.params.application + '" not found');
          return;
      }

      var ret="";

      //res.set('Content-Type', 'text/plain');
      
      DatabaseManager.getTable(req.params.application, 'event', function(err, table) {

          if(!err) {

              //progetto
              table.find({type:'view_progetto'
              }).toArray(function(err, docs) {
                  

                  ret+='TYPE,UID,ID,NAME,DESCRIPTION,TIME<br>';
                  _.each(docs, function(access_data) {
                      
                      var tempRet="";
                      try {
                        tempRet=access_data.type+','+access_data.uid+','+access_data.id+','+access_data.name+','+access_data.description+','+access_data.time+'<br>';
                      } catch(err) {
                        tempRet="Errore recuperando una riga:"+err+",,,,,<br>";
                      }
                      ret+=tempRet;
                  });
                  
                  //contenuti aumentati
                  table.find({type:'view_augmented_content'
                  }).toArray(function(err, docs) {
                      

                      ret+='<br>TYPE,UID,ID,ID_PROGETTO,TITOLO,TIME<br>';
                      _.each(docs, function(access_data) {
                          
                          var tempRet="";
                          try {
                            tempRet=access_data.type+','+access_data.uid+','+access_data.id+','+access_data.id_progetto+','+access_data.titolo+','+access_data.time+'<br>';
                          } catch(err) {
                            tempRet="Errore recuperando una riga:"+err+",,,,,<br>";
                          }
                          ret+=tempRet;
                      });
                      
                      //mappa
                      table.find({type:'has_opened_map'
                      }).toArray(function(err, docs) {
                          

                          ret+='<br>TYPE,UID,TIME<br>';
                          _.each(docs, function(access_data) {
                              
                              var tempRet="";
                              try {
                                tempRet=access_data.type+','+access_data.uid+','+access_data.time+'<br>';
                              } catch(err) {
                                tempRet="Errore recuperando una riga:"+err+",,<br>";
                              }
                              ret+=tempRet;
                          });
                          
                          //item salvati
                          table.find({type:'has_saved_item'
                          }).toArray(function(err, docs) {
                              

                              ret+='<br>TYPE,UID,ID_PROGETTO,DESCRIZIONE,TIME<br>';
                              _.each(docs, function(access_data) {
                                  
                                  var tempRet="";
                                  try {
                                    tempRet=access_data.type+','+access_data.uid+','+access_data.id_progetto+','+access_data.descrizione+','+access_data.time+'<br>';
                                  } catch(err) {
                                    tempRet="Errore recuperando una riga:"+err+",,,,<br>";
                                  }
                                  ret+=tempRet;
                              });

                              //partite 
                              table.find({type:'play_game'
                              }).toArray(function(err, docs) {
                                  

                                //console.log("recupero play_game");

                                 ret+='<br>TYPE,ID_PROGETTO,GIOCO,STATUS,WINNER_UID,WINNER_NAME,TIME<br>';
                                  _.each(docs, function(access_data) {
                                      
                                      var tempRet="";
                                      try {
                                        if (access_data.status=="completed") 
                                          tempRet=access_data.type+','+access_data.identifier+','+access_data.game_title+','+access_data.status+','+access_data.winner.uid+','+access_data.winner.user+','+access_data.request_time+'<br>';
                                        else
                                          tempRet=access_data.type+','+access_data.identifier+','+access_data.game_title+','+access_data.status+',---,---,'+access_data.request_time+'<br>';
                                      } catch(err) {
                                        tempRet="Errore recuperando una riga:"+err+",,,,<br>";
                                      }
                                      ret+=tempRet;
                                  });
                                  
                                  res.send(ret);

                              }); 

                              

                          });
                      });
                  });

              });



          }

      });

  });    




  self.express.get('/:package/:uid/:pushid/setPushId', function(req, res) {

    

    //prendo il bundle id associato a questa testata
    var bundleId =  req.params.package;
    var uid =  req.params.uid;
    var pushid =  req.params.pushid;

    res.send(bundleId+"<br>"+uid+"<br>"+pushid);

    console.log("setPushId - bundleid da cercare: " + bundleId);

    //per ogni testata associata a quel bundle id, aggiorno il db, relativamente a pushid

    _.each(Config.appConfigurations, function(item,key) {

        if(item.bundleId) {

            //console.log("key: " + key);
            //console.log(JSON.stringify(item));

            if(item.bundleId==bundleId){

                console.log("trovato: " + key);
          
                DatabaseManager.getTable(key, 'access', function(err, table) {

                    if(!err) {

                        table.findOneAndUpdate({
                            // find section
                            uid: uid
                          }, {
                              // modify section
                              $set: {
                                uid: uid,
                                pushId: pushid
                              }

                          }, {
                            
                            // options section
                            upsert: false //altriemnti, se non lo trova crea un nuovo record, anche in testate dove non � presente

                          }, function(err, result) {
                            
                            if(err) {
                              winston.error("setDeviceData: " , err);
                            }

                            if(err) {
                                next(err);
                            }

                        });

                    }

                });

                


            }

        }

    });



  });   





}





module.exports = Helper;
