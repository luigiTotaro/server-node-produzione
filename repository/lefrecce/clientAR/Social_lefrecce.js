var Social_lefrecce = function(container,dataObj, closeFn, externalFn) {
  
  this.container = container;
  this.baseUrl = dataObj.contenutiUrl;
  this.oggettoTarget = dataObj.oggetto;
  this.nome = "social";

  this.ARclose=closeFn;
  this.externalFunction=externalFn;

  console.log("Classe dinamica: "+this.nome);
  console.log("window.plugins (dentro classe):");
  console.log(JSON.stringify(window.plugins));


  this.fbLogo="img/fbLogo.png";
  this.twitterLogo="img/twitterLogo.png";
  this.instagramLogo="img/instagramLogo.png";

  this.arrayImages=new Array();
  this.arrayImages.push(this.fbLogo);
  this.arrayImages.push(this.twitterLogo);
  this.arrayImages.push(this.instagramLogo);

  //this.preloadImages();
  this.descrizione = "";
  this.link = "";
  this.text = "";
  this.image = "";
  this.fbStyle = "";
  this.twStyle = "";
  this.instaStyle = "";




}

Social_lefrecce.prototype.renderMain = function(immagine, punto, deviceId) {

    //console.log("RenderMain Social, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var label=this.oggettoTarget[immagine].punti[punto].label;
    var config=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    //trovo quello che riguarda la condivisione social
    for (var z=0;z<config.contenuto.length;z++)
    {
      if (config.contenuto[z].tipo=="Social")
      {
        this.descrizione = config.contenuto[z].descrizione;
        this.link = config.contenuto[z].link;
        this.text = config.contenuto[z].text;
        this.image = config.contenuto[z].image;
        this.fbStyle = config.contenuto[z].fbStyle;
        this.twStyle = config.contenuto[z].twStyle;
        this.instaStyle = config.contenuto[z].instaStyle;
      }
    }

    this.continueRenderMain(label, deviceId);
}


Social_lefrecce.prototype.renderMainExt = function(immagine, punto, indice, deviceId) {

    //console.log("RenderMain Social, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var label=this.oggettoTarget[immagine].punti[punto].label;
    var oggettoContenuti=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    var oggettoPunto=oggettoContenuti[indice];
    var config=JSON.parse(oggettoPunto.config);
    //var config=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    //trovo quello che riguarda la condivisione social
    this.descrizione = config.descrizione;
    this.link = config.link;
    this.text = config.text;
    this.image = config.image;
    this.fbStyle = config.fbStyle;
    this.twStyle = config.twStyle;
    this.instaStyle = config.instaStyle;

    this.continueRenderMain(label, deviceId);
}



Social_lefrecce.prototype.continueRenderMain = function(label, deviceId) {

    var self = this;
	var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //finestra a tutto schermo
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,2555,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();


    //mostro i 3 loghi, o di meno, a seconda di quello configurato
    var numSocial=0;
    if (this.fbStyle!="none")  numSocial++;
    if (this.twStyle!="none")  numSocial++;
    if (this.instaStyle!="none")  numSocial++;

    //alert(this.fbStyle + " - " + this.twStyle + " - " + this.instaStyle);

    //grandezza div
    var styleDiv="";
    if (numSocial==0) styleDiv="width:0%;";
    else if (numSocial==1) styleDiv="width:33%;margin-left:33%;";
    else if (numSocial==2) styleDiv="width:33%;margin-left:11%;";
    else if (numSocial==3) styleDiv="width:33%;";


    html="";
    html+="<div><p class='dynLabel' style='color:#000000;'>Condividi sui social: "+label+"</p>";
    html+="<div id='"+this.nome+"_itemContainer' style='width:100%;'>";
    
    if (this.fbStyle!="none") html+="<div style='"+styleDiv+"text-align:center;float:left;'><img id='"+this.nome+"_fb' src='"+this.baseUrl+"/"+this.fbLogo+"' style='width:50%'></div>";
    if (this.twStyle!="none") html+="<div style='"+styleDiv+"text-align:center;float:left;'><img id='"+this.nome+"_twitter' src='"+this.baseUrl+"/"+this.twitterLogo+"' style='width:50%'></div>";
    if (this.instaStyle!="none") html+="<div style='"+styleDiv+"text-align:center;float:left;'><img id='"+this.nome+"_instagram' src='"+this.baseUrl+"/"+this.instagramLogo+"' style='width:50%'></div>";

    html+="</div><div style='clear:both;'></div>";

    html+="<div><p class='dynText' style='color:#000000;'>"+this.descrizione+"</p>";



    html+="<div id='"+this.nome+"_annulla' class='dynButton' style='color:#000000;border:1px solid #000000;'>RITORNA</div>";

    html+="</div>";

    var htmlObj = $(html);

    //htmlObj.appendTo($("#"+self.nome+"_container"));
    $("#"+this.nome+"_container").html(htmlObj);
    //$("#"+this.nome+"_container").append(html);

    
    $("#"+this.nome+"_fb").click(function(e) {
        self.itemClickFb();
    });

    $("#"+this.nome+"_twitter").click(function(e) {
        self.itemClickTwitter();
    });

    $("#"+this.nome+"_instagram").click(function(e) {
        self.itemClickInstagram();
    });


    $("#"+this.nome+"_annulla").click(function(e) {
        self.container.html("");
        self.container.hide();
        self.ARclose();
    });

    this.resizeBasedWindows();

    $("#loader").hide();

}


Social_lefrecce.prototype.resizeBasedWindows = function() {

    var self = this;
    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    //console.log("Altezza container: " + altezzaContenitore);
    $("#"+this.nome+"_container").css("width",(larghezzaContenitore-(padding*2))+"px").css("left",(padding)+"px").css("height",(altezzaContenitore-(padding*2))+"px").css("top",(padding)+"px");

    var altezzaInner=$("#"+this.nome+"_container").height();
    var larghezzaInner=$("#"+this.nome+"_container").width();

    if (altezzaContenitore>larghezzaContenitore) //siamo in verticale
    {
      $("#"+this.nome+"_itemContainer").css("width","90%").css("margin-left","5%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.03).css("padding-top",altezzaContenitore*0.015).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.02).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.09).css("width","90%").css("margin-left",(larghezzaInner*0.05)-(altezzaContenitore*0.015));
      $(".dynButton").css("font-size",altezzaContenitore*0.03).css("padding",altezzaContenitore*0.015).css("width","50%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.25)-(altezzaContenitore*0.015));

    }
    else //siamo in orizzontale
    {
      $("#"+this.nome+"_itemContainer").css("width","50%").css("margin-left","25%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.04).css("padding-top",altezzaContenitore*0.02).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.03).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.09).css("width","60%").css("margin-left",(larghezzaInner*0.20)-(altezzaContenitore*0.015));
      $(".dynButton").css("font-size",altezzaContenitore*0.05).css("padding",altezzaContenitore*0.015).css("width","30%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.35)-(altezzaContenitore*0.015));
   }

}

Social_lefrecce.prototype.itemClickFb = function() 
{
  
  $("#loader").show();
  setTimeout(function() {
    $("#loader").hide();
  }, 10000);

  console.log("Sono in itemClickFb" );
  
  var oggetto= new Object();
  oggetto.type="fb";
  oggetto.link=null;
  oggetto.image=null;

  //cosa deve condividere su fb?
  if (this.fbStyle=="link") oggetto.link=decodeURIComponent(this.link);
  else if (this.fbStyle=="image") oggetto.image=this.baseUrl +"/"+ decodeURIComponent(this.image);
  
  document.location = 'architectsdk://socialPublish_' + encodeURIComponent(JSON.stringify(oggetto));

}

Social_lefrecce.prototype.itemClickTwitter = function() 
{
  
  $("#loader").show();
  setTimeout(function() {
    $("#loader").hide();
  }, 10000);

  console.log("Sono in itemClickTwitter" );

  var oggetto= new Object();
  oggetto.type="tw";
  oggetto.text="";
  oggetto.image="";

  //cosa deve condividere su twitter?
  if (this.twStyle=="text") oggetto.text=decodeURIComponent(this.text);
  else if (this.twStyle=="image") oggetto.image=this.baseUrl +"/"+ decodeURIComponent(this.image);
  else if (this.twStyle=="text_image")
  {
    oggetto.image=this.baseUrl +"/"+ decodeURIComponent(this.image);
    oggetto.text=decodeURIComponent(this.text);
  }
  
  document.location = 'architectsdk://socialPublish_' + encodeURIComponent(JSON.stringify(oggetto));


}

Social_lefrecce.prototype.itemClickInstagram = function() 
{
  
  $("#loader").show();
  setTimeout(function() {
    $("#loader").hide();
  }, 10000);

  console.log("Sono in itemClickTwitter" );

  var oggetto= new Object();
  oggetto.type="insta";
  oggetto.text="";
  oggetto.image="";

  //cosa deve condividere su twitter?
  if (this.instaStyle=="image") oggetto.image=this.baseUrl +"/"+ decodeURIComponent(this.image);
  
  document.location = 'architectsdk://socialPublish_' + encodeURIComponent(JSON.stringify(oggetto));


}

Social_lefrecce.prototype.test = function() {

    var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Social_lefrecce.prototype.chiSono = function() {

    return "Social";
    //var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Social_lefrecce.prototype.getIcon = function() {

    var obj= new Object();
    obj.img="dynamicIcons/share_fj.png";
    obj.label="Condividi";
    return obj;
    //var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Social_lefrecce.prototype.preloadImages = function() {

    //console.log("sono in preloadImages");
    
    var imgs = new Array();
    for (var i=0;i<this.arrayImages.length;i++)
    {
      //console.log("Preload: " + this.baseUrl+"/"+ this.arrayImages[i]);
      imgs[i] = new Image();
      imgs[i].src = this.baseUrl+"/"+ this.arrayImages[i];
    }

    var iconaObj=this.getIcon();
    //console.log("Preload: " + this.baseUrl+"/"+ iconaObj.img);
    imgs[this.arrayImages.length] = new Image();
    imgs[this.arrayImages.length].src = this.baseUrl+"/"+ iconaObj.img;

}

