var Config = require('./config');
var DatabaseManager = require('./databaseManager');
var fs = require('fs');
var Path = require('path');
var _ = require('underscore');
var winston = require('winston');
var mongo = require('mongodb');
var mysql = require('mysql');

var srv_concorso_trova_img_lefrecce = function(parameters) {

    var self = this;
     
    self.config = {
        
        name: 'Gioco: trova le immmagini nel minor tempo.',
        description: 'Trova tutte le immagini nel minor tempo possibile',
        key: 'mediasoft.concorso.trova.img.lefrecce',
        time_to_live: 60000,
        short_key: 'MCTLEFR0',
        with_results: true,


        parameters: {

            background: {
                type: 'img',
                description: 'Immagine di sfondo usata nella lista dei giochi'
            },

            advice: {
                type: 'text',
                description: 'Consiglio generico su dove cercare le immagini'
            },

            tempo_limite: {
                type: 'number',
                description: 'Tempo Massimo per trovare tutte le immagini'
            },

            data_chiusura_gioco: {
                type: 'timestamp',
                description: 'Data entro cui il gioco si conclude'
            },

            lista_immagine_da_trovare: {
                type: 'wtc_image_list',
                description: 'lista di immagini da usare per il riconoscimento'
            }
                    
        }

    };


    // ############################################################### //
    // sezione dedicata alla validazione dei parametri del costruttore //
    // ############################################################### //
    if(!parameters.socket_namespace) {
      throw "Parametro 'parameters.socket_namespace' obbligatorio here.";
    }

    if(!parameters.application_name) {
      throw "Parametro 'parameters.application_name' obbligatorio.";
    }

    if(!parameters.express_instance) {
      throw "Parametro 'parameters.express_instance' obbligatorio.";
    }


    self.socket_namespace = parameters.socket_namespace;
    self.application_name = parameters.application_name;
    self.express_instance = parameters.express_instance;

    

    if(!self.socket_namespace.additional_callbacks) {
      self.socket_namespace.additional_callbacks = {};
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT] = [];
    }
    

    

    // ########################################### //
    // auto plug del descrittore di configurazione //
    // ########################################### //
    if(!global.configurables[parameters.application_name]) {
        global.configurables[parameters.application_name] = {};
    }
    
    if(!global.configurables[parameters.application_name]['progetto']) {
        global.configurables[parameters.application_name]['progetto'] = [];
    }
    
    global.configurables[parameters.application_name]['progetto'].push(self.config);


    // ######################################################################### //
    // una classe espone ed aggiunge callback che possono agire su eventi socket //
    // ######################################################################### //
    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE].push(function(clientConnection, message) {

        if(message.msg_body.key && message.msg_body.key == self.config.key) {

            
        
        }

    });

 

    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT].push(function(clientConnection) {

         

    });



    


    // endpoint che persiste i rirsultati del gioco
    self.express_instance.post('/' + self.application_name + '/' + self.config.short_key + "/save_play_result", function(req, res) {        

        DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

            if(!err) {

                table.insertOne({

                    identifier: req.body['identifier'],
                    game_title: self.config.name,
                    key: self.config.key,                    
                    type: 'play_game',
                    status: 'completed',
                    request_time: (new Date()).getTime(),                    
                    duration: (req.body['total_time'] * 1000)

                }, function(err, record) {

                    DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

                        if(err) {

                            res.status(500).send(err);

                        } else {


                            table.insertOne({
                                uid: req.body['uid'],
                                user: req.body['user'],
                                email: req.body['email'].toLowerCase(),
                                key: self.config.key,
                                identifier: req.body['identifier'],
                                time: (new Date()).getTime(),
                                total_time: req.body['total_time'], 
                                punteggio: req.body['punteggio']
                            });

                            res.status(200).send('ok');
                        }

                    });


                });

            }

        });
        

    });




    // endpoint che persiste i rirsultati del gioco
    self.express_instance.get('/' + self.application_name + '/' + self.config.short_key + "/:identifier/get_classifica", function(req, res) {        

         
        DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

            if(err) {

                res.status(500).send(err);
                 

            } else {
 
                table.find({
                    identifier: req.params.identifier,
                    key: self.config.key
                })
                .sort({
                    punteggio: -1,
                    total_time: 1
                })
                .limit(1000)
                .toArray(function(err, docs) {
                    
                    res.status(200).send(docs);

                });
                
            }

        });

    });



    // endpoint che persiste i rirsultati del gioco
    self.express_instance.post('/' + self.application_name + '/' + self.config.short_key + "/get_classifica_persistita", function(req, res) {        
console.log("GET CLASSIFICA PERSISTITA");
        try {

            var dbName = Config.appConfigurations[self.application_name].exports.mysql_db;
            var dbConfig = Config.mysql_connections[dbName];

            var mysql_connection = mysql.createConnection(dbConfig);   
            //mysql_connection.connect();

            mysql_connection.query("SELECT * FROM game_classifica WHERE testata='" + self.application_name + 
            "' AND game_key='" + self.config.key + "' AND identifier=" + req.body['identifier'] + 
            " ORDER BY punteggio DESC, total_time ASC LIMIT 100", function (error, result) {

                if(error) {

                    res.status(200).send( {
                        score: 0
                    } );

                } else {

                    console.log(req.body['uid']);
                    console.log(req.body['email'].toLowerCase());

                    var positionIdx = 1 + _.findIndex(result, function(d) { return (d.uid == req.body['uid'] && d.email.toLowerCase() == req.body['email'].toLowerCase()); });

                    res.status(200).send( {
                        score:positionIdx
                    } );

                }
 
                mysql_connection.end();

            });
 

        } catch(err) {
        console.log(err);
            res.status(500).send(err);

        }
        

    });




    self.checkRelativePosition = function(params) {

        DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

            if(err) {

                params.onComplete(err, null);

            } else {
 
                table.find({
                    identifier: params.identifier,
                    key: self.config.key
                })
                .sort({
                    punteggio: -1,
                    total_time: 1
                })
                .limit(100)
                .toArray(function(err, docs) {
                    
                    var positionIdx = 1 + _.findIndex(docs, function(d) { return (d.uid == params.uid && d.email == params.email.toLowerCase()); });

                    params.onComplete(null, positionIdx);

                });
                
            }

        });

    };



    // endpoint che persiste i rirsultati del gioco
    self.express_instance.post('/' + self.application_name + '/' + self.config.short_key + "/relative_position", function(req, res) {
        
        console.log("TEST");

        self.checkRelativePosition({
            identifier: req.body['identifier'],
            uid: req.body['uid'],
            email: req.body['email'],
            onComplete: function(err, positionIdx) {

                if(err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send({
                        score: positionIdx
                    });
                }
            }
        });

    });



    // endpoint che verifica se questo utente può giocare
    self.express_instance.post('/' + self.application_name + '/' + self.config.short_key + "/check_play", function(req, res) {
    

        DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

            if(err) {

                onComplete(err);

            } else {

                table.findOne({

                    uid: req.body['uid'],                    
                    email: req.body['email'].toLowerCase(),
                    key: self.config.key,
                    identifier: req.body['identifier']

                },{

                }, function(err, result) {


                    if(err || result == null) {

                        res.status(200).send({
                            result: true
                        });

                    } else {

                        self.checkRelativePosition({
                            identifier: req.body['identifier'],
                            uid: req.body['uid'],
                            email: req.body['email'].toLowerCase(),
                            onComplete: function(err, result) {

                                if(err) {

                                    res.status(500).send(err);
                                
                                } else {


                                    res.status(200).send({
                                        result: false,
                                        score: result
                                    });
                                }
                            }
                        });
                        
                    }

                });

            }

        });



    });



}



