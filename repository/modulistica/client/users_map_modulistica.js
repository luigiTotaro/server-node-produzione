var users_map_modulistica = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.LanguageService = null;
    this.scope = null;


    var viola = '#009fe3';
    var arancio = '#777777';

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 

    '.div-users-now {' +
    '   position: absolute;' + 
    '   top: 5px;' +
    '   left: 5px;' +
    '   height: auto !important;' +
    '   z-index: 99999;' +
    '   padding: 6px 8px !important;'+    
    '   background-repeat: no-repeat;'+
    '   background-position: 102% 5%;'+
    '   background-size: 30%;'+
    '   max-width: 85%;'+
    '}' +

    '.gz-popup {' +
    '   width: 120px;' + 
    '   margin-left:-60px;' +
    '   height: 51px;' +
    '   margin-top: -50px;' + 
    '   white-space: nowrap;' +    
    '}' +    

    '.gz-popup-body {' +
    '   margin: 0 auto;' + 
    '   display: table;' + 
    '}' +

    '.gz-popup-inner {' +
    '   color: white;' + 
    '   border:solid 2px ' + arancio + ';' +
    '   font-size: 12px;' +
    '   font-family: sans-serif;' +
    '   height: 35px;' +
    '   background-color:' + viola + ';' +
    '   text-align:center;' +
    '   border-radius: 5px;' + 
    '   padding: 2px 4px;' + 
    '   line-height: 13px;' + 
    '}'  +

    '.gz-arrow-ctr {' +
    '   height: 15px;' + 
    '}'  +

    '.gz-arrow-ctr-1 {' +
    '   width: 0;' + 
    '   height: 0;' + 
    '   border-style: solid;' + 
    '   border-width: 13.0px 7.5px 0 7.5px;' + 
    '   border-color: ' + arancio + ' transparent transparent transparent;'+
    '   margin:0 auto;' + 
    '}' +

    '.gz-arrow-ctr-2 {' +
    '   width: 0;' + 
    '   height: 0;' + 
    '   border-style: solid;' + 
    '   border-width: 12px 6px 0 6.5px;' + 
    '   border-color: ' + viola + ' transparent transparent transparent;' + 
    '   margin:0 auto;' + 
    '   margin-top: -15px;' + 
    '   margin-left: -6px;' + 
    '}'
    ;


    document.getElementsByTagName('head')[0].appendChild(style);

 

}


L.HtmlIcon = L.Icon.extend({
    options: {
        /*
        html: (String) (required)
        iconAnchor: (Point)
        popupAnchor: (Point)
        */
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    createIcon: function () {
        var div = document.createElement('div');
        div.innerHTML = this.options.html;
        return div;
    },

    createShadow: function () {
        return null;
    }
});


users_map_modulistica.Instance = null;

users_map_modulistica.Initialize = function(params) {

    if(users_map_modulistica.Instance != null) {
      return;
    }

    users_map_modulistica.Instance = new users_map_modulistica();
    users_map_modulistica.Instance.ionicModal = params.ionicModal;
    users_map_modulistica.Instance.ionicPopup = params.ionicPopup;
    users_map_modulistica.Instance.HelperService = params.HelperService;
    users_map_modulistica.Instance.http = params.http;
    users_map_modulistica.Instance.GlobalVariables = params.GlobalVariables;
    users_map_modulistica.Instance.LoggerService = params.LoggerService;
    users_map_modulistica.Instance.SocketService = params.SocketService;
    users_map_modulistica.Instance.LanguageService = params.LanguageService;


    // rimuovo la voce di menu dalla mappa di giallo_zafferano quando entro nella homepage //
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeModulisticaKey', function() {
        delete users_map_modulistica.Instance;
        users_map_modulistica.Instance = null;
        params.rootScope.removeItemFromMenu('modulistica_map_key');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeModulisticaKey', null);
    });

    params.rootScope.addItemToMenu('modulistica_map_key', params.LanguageService ? params.LanguageService.getLabel("MAPPA_DEGLI_UTENTI") : 'Mappa degli utenti', 'ion-ios-location', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = users_map_modulistica.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            users_map_modulistica.Instance.scope = params.rootScope.$new(true);
            users_map_modulistica.Instance.run();

        }


       
        

    });


}



users_map_modulistica.prototype.getData = function(params) {

    var self = this;


    self.http({
        url: self.GlobalVariables.application.applicationUrl + '/getUsersByRegion',
        responseType: 'json',
        method: 'GET',
        timeout: 10000
    }).then(function(json) {
        params.onComplete(null, json.data);
    }, function(error, xhr) {
        params.onComplete(self.LanguageService ? self.LanguageService.getLabel("VERIFICATO_ERRORE_CONNESSIONE") : 'Si è verificato un errore di connessione Internet', null);
    });

}


users_map_modulistica.prototype.run = function() {

  	var self = this;

    self.scope.destroyView = function() {
        self.scope.mappa.remove();
        self.mapView.remove();
        self.scope.$destroy();
        delete self.scope;
        
    };

    self.SocketService.emit('message','has_opened_map', {}, {
        uid: self.GlobalVariables.deviceUUID
    });



    self.mapView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" id="appFirstWelcomeScreen" style="background-color:rgba(0,0,0,0.5);">' +
            '<ion-content id="messageScrollableArea" scroll="false" padding="false">' +
                '<a ng-click="destroyView()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +
                '<div id="mapDiv" style="position:absolute; top:6px; left:5px; right:5px; bottom:6px;"></div>' +
            '</ion-content>' +
        '</ion-modal-view>',

        {
            scope: self.scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false
        }

    );


    self.mapView.show().then(function() {

        self.scope.mappa = L.map('mapDiv',{
          zoomControl:false
        }).setView([42.508899, 12.830778], 5);

        L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYWtpcmFwaXgiLCJhIjoiRjRaNDFNNCJ9.w19L-nOBP5GGemvdlb0n7w', {
            attribution: '',
            maxZoom: 7,
            minZoom: 4
        }).addTo( self.scope.mappa );

        self.getData({
            onComplete: function(err, json) {

                if(err) {

                    var alertErr = self.ionicPopup.alert({
                      title: self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione',
                      subTitle: self.LanguageService ? self.LanguageService.getLabel("RIPROVARE_PIU_TARDI") : 'Riprovare più tardi',
                      template: err
                    });

                    alertErr.then(function(res) {
                    	self.scope.destroyView();
                    });

                    return;
                    

                } else {

                	var viola = '#009fe3';
                	var arancio = '#777777';
                	var markerArray = [];

                    var utenteConTeLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTE_CON_TE_SU") : 'utente è ora con te su';
                    var utentiConTeLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTI_CON_TE_SU") : 'utenti sono ora con te su';
                    var utenteLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTE") : 'Utente';
                    var utentiLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTI") : 'Utenti';

                    var divUsersNow = document.createElement('div');
                    divUsersNow.className = 'gz-popup-inner div-users-now';
                    divUsersNow.appendChild(document.createTextNode( json.total + " " + (json.total == 1 ? utenteConTeLabel : utentiConTeLabel) + " Modulistica" ));
                    document.getElementById('mapDiv').appendChild(divUsersNow);

                    setTimeout(function() {

    
                        _.each(json.region_list, function(item) {

                             var helloLondonHtmlIcon = new L.HtmlIcon({
                                html: 
                                	'<div class="gz-popup">' + 
                                		'<div class="gz-popup-body">' +
                                			'<div class="gz-popup-inner">' +
                                				item.totale + " " + (item.totale == 1 ? utenteLabel : utentiLabel) + " in<br>" + item.name + 
                                			'</div>' +
                                			'<div class="gz-arrow-ctr">' +
                                				'<div class="gz-arrow-ctr-1">'+
                                					'<div class="gz-arrow-ctr-2"></div>' +
                                				'</div>' +
                                			'</div>' +
                                			
                                		'</div>' +
                                	'</div>'
                            });

                             var marker = new L.Marker(new L.LatLng(item.lat, item.lon), {icon: helloLondonHtmlIcon});

                             self.scope.mappa.addLayer(marker);

                             markerArray.push(marker);
                
                        });

                        if(markerArray.length > 0) {
                            var group = L.featureGroup(markerArray);
                            self.scope.mappa.fitBounds(group.getBounds());    
                        }
                        

                    }, 500);

                    


                }

            }

        });

    });

}
