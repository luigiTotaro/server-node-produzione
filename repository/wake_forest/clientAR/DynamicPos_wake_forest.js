var DynamicPos_wake_forest = function() {
   console.log("Classe dinamica: DynamicPos");

}



DynamicPos_wake_forest.prototype.getContainerHeight = function(itemNumber) {

    var self = this;
	//console.log
	//alert("itemNumber: " + itemNumber);
    if (itemNumber==1) return 1.05;
    else if (itemNumber==2) return 0.65;
    else if (itemNumber==3) return 0.55;
    else return 0.5;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

DynamicPos_wake_forest.prototype.getItemData = function(itemNumber) {

    var self = this;
    var obj = new Object();
    if (itemNumber==1)
    {
      obj.itemHeight=0.70;
      obj.labelHeight=0.14;
    }
    else if (itemNumber==2)
    {
      obj.itemHeight=0.35;
      obj.labelHeight=0.10;
    }
    else if (itemNumber==3)
    {
      obj.itemHeight=0.25;
      obj.labelHeight=0.07;
    }
    else
    {
      obj.itemHeight=0.40;
      obj.labelHeight=0.038;
    }


    return obj;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

DynamicPos_wake_forest.prototype.getMainLabelData = function(dimIniziale,itemNumber) {

    var self = this;
    var altezzaContenitore=dimIniziale*self.getContainerHeight(itemNumber);
    var obj = new Object();
    obj.scala=0.15;
    obj.offX=0;
    obj.offY=+(altezzaContenitore*0.5);
    return obj;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}


DynamicPos_wake_forest.prototype.getItemOffset = function(itemNumber,item,dimIniziale) {

    var self = this;
    //var oggettoLabelPrincipale=self.getMainLabelData();
    var datiElemento=self.getItemData(itemNumber);
    var altezzaElemento=datiElemento.itemHeight*dimIniziale;
    var offset= new Object();
    if (itemNumber==1)
    {
      offset.x=0;
      offset.y=0;
      offset.labelY=-altezzaElemento*0.5;
    }
    else if (itemNumber==2)
    {
      if (item==1)
      {
        offset.x=-(dimIniziale*0.5)+(dimIniziale/4);
        offset.y=0;
        offset.labelY=-altezzaElemento*0.5;
      }
      else if (item==2)
      {
        offset.x=(dimIniziale*0.5)-(dimIniziale/4);
        offset.y=0;
        offset.labelY=-altezzaElemento*0.5;
      }
    }
    else if (itemNumber==3)
    {
      if (item==1)
      {
        offset.x=-(dimIniziale*0.5)+(dimIniziale/6);
        offset.y=0;
        offset.labelY=-altezzaElemento*0.7;
      }
      else if (item==2)
      {
        offset.x=0;
        offset.y=0;
        offset.labelY=-altezzaElemento*0.7;
      }
      else if (item==3)
      {
        offset.x=(dimIniziale*0.5)-(dimIniziale/6);
        offset.y=0;
        offset.labelY=-altezzaElemento*0.7;
      }
    }
    else
    {
      offset.x=0;
      offset.y=0;
      offset.labelY=-altezzaElemento*0.5;
    }

    return offset;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

DynamicPos_wake_forest.prototype.chiSono = function() {

    return "DynamicPos";
    //var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}