var Commento_eurospin = function(container,dataObj, closeFn, externalFn) {
  
   
   this.container = container;
   this.baseUrl = dataObj.contenutiUrl;
   this.oggettoTarget = dataObj.oggetto;
   this.nome = "commento";
   this.showCommentsDisclaimer=dataObj.showCommentsDisclaimer;
   this.commentsNickname=dataObj.commentsNickname;
   console.log("Classe dinamica: "+this.nome);
   //console.log(this.showCommentsDisclaimer);
   //console.log(this.commentsNickname);

   this.sdkVersion = dataObj.sdkVersion; //iniziato a mandare dalla versione 2.3, sdk 8.1
   console.log("Versione SDK: "+this.sdkVersion);

   this.immagine;
   this.punto;
   this.indice;
   this.deviceId;
   
   this.ARclose=closeFn;
   this.externalFunction=externalFn;
    console.log("externalFunction: "+this.externalFunction);
    
    this.descrizione = "";
   
   //console.log("AR:");
   //console.log(this.AR);

/*
    document.location = 'architectsdk://salvaConfig_' + encodeURIComponent(JSON.stringify({
      showCommentsDisclaimer: true,
      commentsNickname: commentsNickname
    }));  
*/
}


Commento_eurospin.prototype.renderMain = function(immagine, punto, deviceId) {

    //console.log("RenderMain, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,255,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();

    this.immagine=immagine;
    this.punto=punto;
    this.indice=null;
    this.deviceId=deviceId;   

    this.mostraMessaggi();

}

Commento_eurospin.prototype.renderMainExt = function(immagine, punto, indice, deviceId) {

    //console.log("RenderMain, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,255,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();

    this.immagine=immagine;
    this.punto=punto;
    this.indice=indice;   
    this.deviceId=deviceId;   

    this.mostraMessaggi();

}


Commento_eurospin.prototype.mostraMessaggi = function() {

    var self = this;
    //leggo tutti i commenti per quel punto
    this.idPunto=this.oggettoTarget[this.immagine].punti[this.punto].id;
    this.label=this.oggettoTarget[this.immagine].punti[this.punto].label;
    var url=this.baseUrl + "/checkCommento.php?idPunto="+this.idPunto+"&deviceId="+this.deviceId;
    //console.log("URL per checkCommento: " + url);

    //trovo quello che riguarda la votazione
    if (this.indice==null) //vecchia modalità
    {
      var config=JSON.parse(this.oggettoTarget[this.immagine].punti[this.punto].config);
      for (var z=0;z<config.contenuto.length;z++)
      {
        if (config.contenuto[z].tipo=="Commento")
        {
          if (typeof config.contenuto[z].descrizione != 'undefined')
          {
            this.descrizione = config.contenuto[z].descrizione;
          }
          else
          {
            this.descrizione = "";
          }
        }
      }
    }
    else
    {
      var oggettoContenuti=JSON.parse(this.oggettoTarget[this.immagine].punti[this.punto].config);
      var oggettoPunto=oggettoContenuti[this.indice];
      var config=JSON.parse(oggettoPunto.config);
      this.descrizione = config.descrizione;
    }

    $.ajax({
      url: url,
      dataType: 'json'
      })
      .done(function( json )
      {
        //console.log("arrivata risposta dal checkCommento");
        //console.log(JSON.stringify(json));

        $("#loader").hide();

        //ci sono commenti da mostrare?
        var commenti=json.commenti;

        var htmlCommenti="";
        if (commenti.length==0)
        {
          htmlCommenti="Nessun commento presente...";
        }
        else
        {
          for (var i=0;i<commenti.length;i++)
          {
            htmlCommenti+="<div style='color:#000000;margin-top:10px;margin-left:10px;'><b>"+commenti[i].nickname+"</b> - <i>"+commenti[i].data+"</i>: "+commenti[i].commento+"</div>";
          }
        }


        //scrivo tutta la pagina
        html="";
        html+="<p class='dynText' style='color:#000000;'>"+self.descrizione+"</p>";
        html+="<div><p class='dynLabel' style='color:#000000;'>Tutti i commenti per: "+self.label+"</p>";
        html+="<div id='"+self.nome+"_commentiContainer' style='width:90%;border:1px solid gray;overflow-y: auto;text-align:left;margin-left:5%;margin-top:5%;'>";
        html+=htmlCommenti;
        html+="</div>";

        //metto il pulsante per chiudere
        html+="<div style='width:100%;margin-top:20px;'>";
        html+="  <div id='"+self.nome+"_buttSX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>SCRIVI</div>";
        html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:right;'>CHIUDI</div>";
        html+="</div>";


        var htmlObj = $(html);

        $("#"+self.nome+"_container").html(htmlObj);


        $("#"+self.nome+"_buttDX").click(function(e) {
            self.container.html("");
            self.container.hide();
            self.ARclose();
        });

        //puo' commentare?
        if (json.mycommento==null)
        {
          $("#"+self.nome+"_buttSX").click(function(e) {
            self.checkDisclaimer();
          });          
        }
        else
        {
          $("#"+self.nome+"_buttSX").hide();
        }



        self.resizeBasedWindows();


      })
      .fail(function( jqXHR, textStatus ) {
          //console.log( "Request failed: " + textStatus );
          //alert("Errore2");
          //probabile mancanza di rete
          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Attenzione, probabili problemi di connessione</p></div>";
          html+="<div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          $("#"+self.nome+"_buttDX").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();

          $("#loader").hide();      
      });

}

Commento_eurospin.prototype.resizeBasedWindows = function() {

    var self = this;
    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    //console.log("Altezza container: " + altezzaContenitore);
    $("#"+this.nome+"_container").css("width",(larghezzaContenitore-(padding*2))+"px").css("left",(padding)+"px").css("height",(altezzaContenitore-(padding*2))+"px").css("top",(padding)+"px");

    var altezzaInner=$("#"+this.nome+"_container").height();
    var larghezzaInner=$("#"+this.nome+"_container").width();

    if (altezzaContenitore>larghezzaContenitore) //siamo in verticale
    {
      $(".dynLabel").css("font-size",altezzaContenitore*0.03).css("padding-top",altezzaContenitore*0.005).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.025).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.0).css("width","90%").css("margin-left","5%");
      //quanto sono alti i 2 testi?
      var altezzaDescrizione = $(".dynText").height();
      var altezzaLabel = $(".dynLabel").height();
      var altezzaUtileCommenti=altezzaInner-altezzaDescrizione-altezzaLabel;
      $("#"+this.nome+"_commentiContainer").css("width","90%").css("margin-left","5%").css("height",(altezzaUtileCommenti*0.75)+"px").css("margin-top","5%").css("font-size",altezzaContenitore*0.02); 
      $("#"+this.nome+"_iframeDisclaimer").css("width","86%").css("height",(altezzaInner*0.8)+"px").css("margin-top","4%").css("font-size",altezzaContenitore*0.025); 
     //ci sono 1 o 2 pulsanti?
      if ($("#"+self.nome+"_buttSX").is(":visible"))
      {
        var padding=larghezzaInner*0.01;
        var larghezzaPulsante=larghezzaInner*0.4;
        var larghezzaTotalePulsante=larghezzaPulsante+(2*padding)+2; //l'ultimo 2 è il bordo
        var margine=((larghezzaInner*0.5)-larghezzaTotalePulsante)/2;
        //console.log(larghezzaInner + " - " + larghezzaPulsante + " - " + padding + " - " + larghezzaTotalePulsante + " - " + margine);
        $(".dynButton").css("font-size",altezzaContenitore*0.03).css("padding",padding).css("width",larghezzaPulsante);
        $("#"+self.nome+"_buttSX").css("margin","0px").css("margin-left",margine);
        $("#"+self.nome+"_buttDX").css("margin","0px").css("margin-right",margine);
      }
      else
      {
        var padding=larghezzaInner*0.01;
        var larghezzaPulsante=larghezzaInner*0.4;
        var larghezzaTotalePulsante=larghezzaPulsante+(2*padding)+2; //l'ultimo 2 è il bordo
        var margine=(larghezzaInner-larghezzaTotalePulsante)/2;
        $("#"+self.nome+"_buttDX").css("font-size",altezzaContenitore*0.03).css("padding",padding).css("width",larghezzaPulsante).css("float","none").css("margin-left",margine);
      }
    }
    else //siamo in orizzontale
    {
      $(".dynLabel").css("font-size",altezzaContenitore*0.035).css("padding-top",altezzaContenitore*0.005).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.035).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.0).css("width","90%").css("margin-left","5%");
      //quanto sono alti i 2 testi?
      var altezzaDescrizione = $(".dynText").height();
      var altezzaLabel = $(".dynLabel").height();
      var altezzaUtileCommenti=altezzaInner-altezzaDescrizione-altezzaLabel;
 
      $("#"+this.nome+"_commentiContainer").css("width","90%").css("margin-left","5%").css("height",(altezzaUtileCommenti*0.65)+"px").css("margin-top","3%").css("font-size",altezzaContenitore*0.03); 
      $("#"+this.nome+"_iframeDisclaimer").css("width","86%").css("height",(altezzaInner*0.7)+"px").css("margin-top","2%").css("font-size",altezzaContenitore*0.035); 

      $(".dynButton").css("font-size",altezzaContenitore*0.05).css("padding",altezzaContenitore*0.015).css("width","30%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.35)-(altezzaContenitore*0.015));
      //ci sono 1 o 2 pulsanti?
      if ($("#"+self.nome+"_buttSX").is(":visible"))
      {
        var padding=larghezzaInner*0.01;
        var larghezzaPulsante=larghezzaInner*0.3;
        var larghezzaTotalePulsante=larghezzaPulsante+(2*padding)+2; //l'ultimo 2 è il bordo
        var margine=((larghezzaInner*0.5)-larghezzaTotalePulsante)/2;
        //console.log(larghezzaInner + " - " + larghezzaPulsante + " - " + padding + " - " + larghezzaTotalePulsante + " - " + margine);
        $(".dynButton").css("font-size",altezzaContenitore*0.03).css("padding",padding).css("width",larghezzaPulsante);
        $("#"+self.nome+"_buttSX").css("margin","0px").css("margin-left",margine);
        $("#"+self.nome+"_buttDX").css("margin","0px").css("margin-right",margine);
      }
      else
      {
        var padding=larghezzaInner*0.01;
        var larghezzaPulsante=larghezzaInner*0.3;
        var larghezzaTotalePulsante=larghezzaPulsante+(2*padding)+2; //l'ultimo 2 è il bordo
        var margine=(larghezzaInner-larghezzaTotalePulsante)/2;
        $("#"+self.nome+"_buttDX").css("font-size",altezzaContenitore*0.03).css("padding",padding).css("width",larghezzaPulsante).css("float","none").css("margin-left",margine);
      }

   }

}

Commento_eurospin.prototype.checkDisclaimer = function() {

    var self = this;
    $("#"+self.nome+"_container").html("");
    if (showCommentsDisclaimer==true)
    {
      //alert("devo mostrare il disclaimer, il nome è: " + commentsNickname + ".");
      /*
      showCommentsDisclaimer=false;
      commentsNickname="pippo";
      document.location = 'architectsdk://salvaConfig_' + encodeURIComponent(JSON.stringify({
        showCommentsDisclaimer: showCommentsDisclaimer,
        commentsNickname: commentsNickname
      }));
      */
      var url=this.baseUrl + "/disclaimers/commenti.txt";
      //console.log("URL per disclaimer: " + url);

      var testoDisclaimer="";
    
    $.ajax({
      url: url,
      dataType: 'text'
      })
      .done(function( testo )
      {
        testoDisclaimer=testo;
        var html="";
        html+="<div><p class='dynLabel' style='color:#000000;'>Disclaimer</p>";
        
        html+="<div id='"+self.nome+"_iframeDisclaimer' style='width:86%;border:1px solid gray;overflow-y: auto;text-align:left;margin-left:5%;margin-top:5%;padding:2%;'>";
        html+=testoDisclaimer;
        html+="</div>";
        //html+="<iframe id='"+self.nome+"_iframeDisclaimer' src='"+url+"' height='200' style='margin: 0;width:90%;overflow: scroll;padding: 0;border: 1px solid gray;'></iframe>";

        html+="</div>";

        //metto il pulsante per chiudere
        html+="<div style='width:100%;margin-top:20px;'>";
        html+="  <div id='"+self.nome+"_buttSX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>ACCETTA</div>";
        html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:right;'>RIFIUTA</div>";
        html+="</div>";


        var htmlObj = $(html);

        $("#"+self.nome+"_container").html(htmlObj);


        $("#"+self.nome+"_buttDX").click(function(e) {
            self.mostraMessaggi();
        });

        $("#"+self.nome+"_buttSX").click(function(e) {
            self.scriviMessaggio();
        });          


        self.resizeBasedWindows();
      })
      .fail(function( jqXHR, textStatus ) {
          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Attenzione, possibili problemi di connessione</p></div>";
          html+="<div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          $("#"+self.nome+"_buttDX").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();
      });









    }
    else
    {
      //alert("NON devo mostrare il disclaimer, il nome è: " + commentsNickname + ".");
      self.scriviMessaggio();
    }
}

Commento_eurospin.prototype.scriviMessaggio = function() {

    console.log("Sono in ScriviMessaggio" );

    var oggetto= new Object();
    oggetto.label=this.label;
    oggetto.nickname=this.commentsNickname;
    oggetto.idPunto=this.idPunto;
    oggetto.deviceId=this.deviceId;

    if (this.externalFunction==undefined)
    {
      document.location = 'architectsdk://writeComment_' + encodeURIComponent(JSON.stringify(oggetto));
    }
    else
    {
      oggetto.topic="writeComment";
      this.externalFunction(oggetto);
    }
    /*

    var self = this;
    $("#"+self.nome+"_container").html("");

    var html="";
    html+="<div>";
    html+="<p class='dynLabel' style='color:#000000;'>Inserisci un commento per "+this.label+"</p>";
    if (this.commentsNickname=="")
    {
      html+="<p class='dynLabel' style='color:#000000;text-align:left;margin-left:5%;'>Inserisci un nickName con il quale verrà pubblicato il tuo commento.</p>";
    }
    html+="<p class='dynLabel' style='color:#000000;text-align:left;margin-left:5%;'>Nickname</p>";
    html+="<input class='dynLabel' type='text' id='"+self.nome+"_nickname' maxlength='20' style='border:1px solid;color:#000000;text-align:left;margin-left:-40%;width:50%;' placeholder='Inserisci il tuo nickname' value='"+this.commentsNickname+"'>";

    html+="<p class='dynLabel' style='color:#000000;text-align:left;margin-left:5%;'>Nickname</p>";
    html+="<textarea class='dynLabel' id='"+self.nome+"_commento' rows='10' style='color:#000000;text-align:left;margin-left:0%;width:90%;' placeholder='Inserisci il tuo commento' value=''></textarea>";

    html+="</div>";

    //metto il pulsante per chiudere
    html+="<div style='width:100%;margin-top:20px;'>";
    html+="  <div id='"+self.nome+"_buttSX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>INVIA</div>";
    html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:right;'>ANNULLA</div>";
    html+="</div>";

    html+="<div style='clear:both;'></div>";

    //metto l'eventuale messaggio di caratteri non ammessi
    html+="<div id='"+self.nome+"_errorCommento' style='display:none;width:90%'>";
    html+="<p class='dynLabel' style='color:#FF0000;margin-left:5%;'><br>Attenzione nel commento vi sono dei caratteri non permessi. Sono accettati solo i caratteri alfanumerici (A-Z a-z 0-9) e i seguenti simboli:  .,!&:_@$^;|<>\"'=+*()</p>";
    html+="</div>";

    //metto l'eventuale messaggio di caratteri non ammessi nel nickname
    html+="<div id='"+self.nome+"_errorNick' style='display:none;width:90%'>";
    html+="<p class='dynLabel' style='color:#FF0000;margin-left:5%;width:90%;'><br>Attenzione nel nickname vi sono dei caratteri non permessi. Sono accettati solo i caratteri alfanumerici (A-Z a-z 0-9) e i seguenti simboli:  .,!&:_@$^;|<>\"'=+*()</p>";
    html+="</div>";



    var htmlObj = $(html);

    $("#"+self.nome+"_container").html(htmlObj);


    $("#"+self.nome+"_buttDX").click(function(e) {
        self.mostraMessaggi();
    });

    $("#"+self.nome+"_buttSX").click(function(e) {
        self.inviaMessaggio();
    });          


    self.resizeBasedWindows();
    */

}

Commento_eurospin.prototype.inviaMessaggio = function() {

    var self = this;
    $("#loader").show();
   
    var objParam = new Object();
    objParam.idPunto = this.idPunto;
    objParam.deviceId = this.deviceId;
    objParam.commento = $("#"+self.nome+"_commento").val();
    objParam.nickname = $("#"+self.nome+"_nickname").val();
    var url=this.baseUrl + "/setCommento.php";

    this.commentsNickname = objParam.nickname;
    
    //console.log(JSON.stringify(objParam));
    //console.log(url);

    //controllo caratteri non validi sul commento
    var reg = /[^-A-Za-z0-9 .,!&:_@$^;|<>"'’=+*()]/;
    var test = reg.test(objParam.commento);    
    if (test==true)
    {
      $("#loader").hide();
      console.log("caratteri non ammessi nel commento");
      $("#"+self.nome+"_errorCommento").show();
      setTimeout(function() {
        $("#"+self.nome+"_errorCommento").hide();
      }, 7000);
      return;
    }

    var test = reg.test(objParam.nickname);    
    if (test==true)
    {
      $("#loader").hide();
      console.log("caratteri non ammessi nel nickname");
      $("#"+self.nome+"_errorNick").show();
      setTimeout(function() {
        $("#"+self.nome+"_errorNick").hide();
      }, 7000);
      return;
    }


    $.ajax({
      url: url,
      type:"POST",
      data:objParam
    }).done(function ( data ) {
      //console.log("tornato da setCommento");

      //console.log(JSON.stringify(data));

      $("#loader").hide();

      $("#"+self.nome+"_container").html("");

      var html="";
      html+="<div>";
      html+="<p class='dynLabel' style='color:#000000;'>Il tuo commento è stato inserito.<br>Verrà sottoposto a controllo da parte della redazione e mostrato tra gli altri commenti<br><br>Grazie!</p>";
      html+="</div>";

      //metto il pulsante per chiudere
      html+="<div style='width:100%;margin-top:20px;'>";
      html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>RITORNA</div>";
      html+="</div>";


      var htmlObj = $(html);

      $("#"+self.nome+"_container").html(htmlObj);

      $("#"+self.nome+"_buttDX").click(function(e) {
          self.mostraMessaggi();
      });          


      self.resizeBasedWindows();


    }).fail(function(jqXHR, textStatus) {
      //console.log( "error " +textStatus);
      $("#loader").hide();

      $("#"+self.nome+"_container").html("");

      var html="";
      html+="<div>";
      html+="<p class='dynLabel' style='color:#000000;'>Oh Oh... Qualcosa sembra non essere andato bene<br>Ti preghiamo di riprovare più tardi<br><br>Grazie!</p>";
      html+="</div>";

      //metto il pulsante per chiudere
      html+="<div style='width:100%;margin-top:20px;'>";
      html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>RITORNA</div>";
      html+="</div>";


      var htmlObj = $(html);

      $("#"+self.nome+"_container").html(htmlObj);

      $("#"+self.nome+"_buttDX").click(function(e) {
          self.mostraMessaggi();
      });          


      self.resizeBasedWindows();

    });



}


Commento_eurospin.prototype.inviaMessaggioControllato = function(oggetto) {

    var self = this;
    $("#loader").show();
   
    var objParam = new Object();
    objParam.idPunto = this.idPunto;
    objParam.deviceId = this.deviceId;
    objParam.commento = oggetto.commento;
    objParam.nickname = oggetto.nick;
    var url=this.baseUrl + "/setCommento.php";

    //alert(JSON.stringify(objParam));
    //alert(url);


    this.commentsNickname = objParam.nickname;
    
    $.ajax({
      url: url,
      type:"POST",
      data:objParam
    }).done(function ( data ) {
      //console.log("tornato da setCommento");

      //alert(JSON.stringify(data));

      $("#loader").hide();

      $("#"+self.nome+"_container").html("");

      var html="";
      html+="<div>";
      html+="<p class='dynLabel' style='color:#000000;'>Il tuo commento è stato inserito.<br>Verrà sottoposto a controllo da parte della redazione e mostrato tra gli altri commenti<br><br>Grazie!</p>";
      html+="</div>";

      //metto il pulsante per chiudere
      html+="<div style='width:100%;margin-top:20px;'>";
      html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>RITORNA</div>";
      html+="</div>";


      var htmlObj = $(html);

      $("#"+self.nome+"_container").html(htmlObj);

      $("#"+self.nome+"_buttDX").click(function(e) {
          self.mostraMessaggi();
      });          


      self.resizeBasedWindows();


    }).fail(function(jqXHR, textStatus) {
      //console.log( "error " +textStatus);
      $("#loader").hide();

      $("#"+self.nome+"_container").html("");

      var html="";
      html+="<div>";
      html+="<p class='dynLabel' style='color:#000000;'>Oh Oh... Qualcosa sembra non essere andato bene<br>Ti preghiamo di riprovare più tardi<br><br>Grazie!</p>";
      html+="</div>";

      //metto il pulsante per chiudere
      html+="<div style='width:100%;margin-top:20px;'>";
      html+="  <div id='"+self.nome+"_buttDX' class='dynButton' style='color:#000000;border:1px solid #555555;float:left;'>RITORNA</div>";
      html+="</div>";


      var htmlObj = $(html);

      $("#"+self.nome+"_container").html(htmlObj);

      $("#"+self.nome+"_buttDX").click(function(e) {
          self.mostraMessaggi();
      });          


      self.resizeBasedWindows();

    });



}


Commento_eurospin.prototype.test = function() {

    var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Commento_eurospin.prototype.chiSono = function() {

    return "Commento";
    //var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Commento_eurospin.prototype.getIcon = function() {

    var obj= new Object();
    obj.img="dynamicIcons/commenta_fj.png";
    obj.label="Commenta";
    return obj;
    //var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

