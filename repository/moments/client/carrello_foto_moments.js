var carrello_foto_moments = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.ionicLoading = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.LanguageService = null;
    this.LazyLoader = null;
    this.myWindow = null;
    this.FileSystemService = null;
    this.SerializerJQLike = null;
    this.scope = null;


    var coloreInterno = '#FFFFFF';
    var coloreBordo = '#9bbe3a';

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 


    '.fotoCarrelloContainer {' +
    '   float:left;' + 
    '   border: 0px;' + 
    '   height:200px;' + 
    '   width:42%;' + 
    '   padding:1px;' + 
    '   margin:4%;' +
    '   background-color: transparent;' + 
    '}' + 

    '.fotoCarrello {' +
    '   width:100%;' +
    '   height:100%;' +
    '   background-size:contain;' +
    '   background-repeat: no-repeat;' +    
    '   background-position:center center;' +
    '   background-color: transparent;' +
    '   padding:0px;' +
    '   margin:0px;' +
    '   pointer-events: none;' +
    '   text-decoration:none !important;' +
    '   color:inherit;' +
    '}' +

    '.chiudi {' +
    '   -webkit-filter: drop-shadow(0px 0px 3px white);' +
    '   filter: drop-shadow(0px 0px 3px white);'+
    '   opacity: 0.8 !important;' + 
    '}' +


'@media all and (orientation:portrait) {' +

    '.fotoCarrelloContainer {' +
    '   height:450px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:80px;' + 
    '}' + 

'  @media (max-width:1600px) {' +
    '.fotoCarrelloContainer {' +
    '   height:450px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:80px;' + 
    '}' + 
' }' +
  
'  @media (max-width:1100px) {' +
    '.fotoCarrelloContainer {' +
    '   height:320px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:60px;' + 
    '}' + 
'  }' +
  
'  @media (max-width:800px) {' +
    '.fotoCarrelloContainer {' +
    '   height:230px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:46px;' + 
    '}' + 
'  }' +
  
'  @media (max-width:680px) {' +
    '.fotoCarrelloContainer {' +
    '   height:200px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:44px;' + 
    '}' + 
'  }' +
  
'  @media (max-width:500px) {' +
    '.fotoCarrelloContainer {' +
    '   height:140px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:40px;' + 
    '}' + 
'  }' +
  
'  @media (max-width:400px) {' +
    '.fotoCarrelloContainer {' +
    '   height:130px;' + 
    '}' + 
    '.cancellaIcon {' +
    '   font-size:30px;' + 
    '}' + 


'  }' +

'}'





    document.getElementsByTagName('head')[0].appendChild(style);



}


L.HtmlIcon = L.Icon.extend({
    options: {
        /*
        html: (String) (required)
        iconAnchor: (Point)
        popupAnchor: (Point)
        */
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    createIcon: function () {
        var div = document.createElement('div');
        div.innerHTML = this.options.html;
        return div;
    },

    createShadow: function () {
        return null;
    }
});


carrello_foto_moments.Instance = null;

carrello_foto_moments.Initialize = function(params) {

    if(carrello_foto_moments.Instance != null) {
      return;
    }

    carrello_foto_moments.Instance = new carrello_foto_moments();
    carrello_foto_moments.Instance.ionicModal = params.ionicModal;
    carrello_foto_moments.Instance.ionicPopup = params.ionicPopup;
    carrello_foto_moments.Instance.HelperService = params.HelperService;
    carrello_foto_moments.Instance.http = params.http;
    carrello_foto_moments.Instance.GlobalVariables = params.GlobalVariables;
    carrello_foto_moments.Instance.LoggerService = params.LoggerService;
    carrello_foto_moments.Instance.SocketService = params.SocketService;
    carrello_foto_moments.Instance.LanguageService = params.LanguageService;
    carrello_foto_moments.Instance.LazyLoader = params.LazyLoader;
    carrello_foto_moments.Instance.myWindow = params.myWindow;
    carrello_foto_moments.Instance.FileSystemService = params.FileSystemService;
    carrello_foto_moments.Instance.ionicLoading = params.ionicLoading;
    carrello_foto_moments.Instance.SerializerJQLike = params.SerializerJQLike;


    /*
    // rimuovo la voce di menu quando entro nella homepage //
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsCarrelloFotoKey', function() {
        delete carrello_foto_moments.Instance;
        carrello_foto_moments.Instance = null;
        params.rootScope.removeItemFromMenu('moments_foto_carrello_key_old');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsCarrelloFotoKey', null);
    });

    params.rootScope.addItemToMenu('moments_foto_carrello_key_old', params.LanguageService ? params.LanguageService.getLabel("CARRELLO_FOTO") : 'Carrello Foto', 'ion-ios-cart', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = carrello_foto_moments.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            carrello_foto_moments.Instance.scope = params.rootScope.$new(true);
            carrello_foto_moments.Instance.run();

        }

    });
*/


    params.rootScope.addItemToHeader('moments_foto_carrello_key', params.LanguageService ? params.LanguageService.getLabel("CARRELLO_FOTO") : 'Carrello Foto', 'ion-ios-cart', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = carrello_foto_moments.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            //params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            carrello_foto_moments.Instance.scope = params.rootScope.$new(true);
            carrello_foto_moments.Instance.run();

        }


       
        

    });


}



carrello_foto_moments.prototype.aggiornaListaImmagini = function() {

    var self = this;
    self.scope.images_list = [];
    self.scope.numeroFotoPresenti=0;
    self.scope.arrayLocalStorage = [];
    
    var arrayStr = self.myWindow.localStorage.getItem("immaginiCarrello");
    
    console.log("immaginiCarrello: " + arrayStr);
    
    //var array = new Array();
    if (arrayStr==null)
    {
      //non ci sono immagini
      self.scope.images_list=[];
    }
    else
    {
      self.scope.arrayLocalStorage=JSON.parse(arrayStr);
      if (self.scope.arrayLocalStorage.length==0)
      {
        //non ci sono immagini
        self.scope.images_list=[];
      }
      else
      {
        //ci sono immagini, prendo solo quelle del progetto
        for (var i=0;i<self.scope.arrayLocalStorage.length;i++)
        {
            if (self.scope.arrayLocalStorage[i].prjId==self.GlobalVariables.application.currentProgetto.idProgetto)
            {
                self.scope.images_list.push(self.scope.arrayLocalStorage[i].targetUrl);
            }
        }
        self.scope.numeroFotoPresenti = self.scope.images_list.length;
      }

    }

    console.log("images_list: " + self.scope.images_list);

    
    //e riempio anche email e tel, se sono stati già inseriti in precedenza
    var email = self.myWindow.localStorage.getItem("emailCarrello");
    var tel = self.myWindow.localStorage.getItem("telCarrello");
    if (email!=null) self.scope.tmp_email=email;
    if (tel!=null) self.scope.tmp_tel=tel;


    self.ionicLoading.hide();
    //alert(self.scope.numeroFotoPresenti);



}


carrello_foto_moments.prototype.run = function() {

  	var self = this;

    self.scope.destroyView = function() {
        self.fotoView.remove();
        self.scope.$destroy();
        delete self.scope;
    };

    self.scope.showPhoto = function(url) {
        if (self.scope.stato==1)
        {
            self.scope.bigImage=url;
            self.scope.stato=2;
        }
    };

    self.scope.deleteImage = function(url) {
        carrello_foto_moments.Instance.ionicPopup.show({
          template: '',
          title: self.LanguageService.getLabel("CARRELLO_VUOI_ELIMINARE_FOTO"),
          //subTitle: 'Please use normal things',
          scope: self.scope,
          buttons: [
            { text: self.LanguageService.getLabel("SI"),
              onTap: function(e) {
                //la cancello dalla lista delle immagini generale
                console.log("Array Carrello prima:");
                console.log(self.scope.arrayLocalStorage);

                for (var i=0;i<self.scope.arrayLocalStorage.length;i++)
                {
                    if (self.scope.arrayLocalStorage[i].prjId==self.GlobalVariables.application.currentProgetto.idProgetto)
                    {
                        if (self.scope.arrayLocalStorage[i].targetUrl==url)
                        {
                            self.scope.arrayLocalStorage.splice(i,1);
                        }
                    }
                }

                console.log("Array Carrello dopo:");
                console.log(self.scope.arrayLocalStorage);

                //salvo nuovamente questo array nella localStorage
                self.myWindow.localStorage.setItem("immaginiCarrello", JSON.stringify(self.scope.arrayLocalStorage));
                self.aggiornaListaImmagini();

              }
            },
            {
              text: self.LanguageService.getLabel("NO"),
              type: 'button-positive',
              onTap: function(e) {
                //alert("NON RA");
                
              }
            }
          ]
        }); 
    };

    self.scope.aggiungiFoto = function() {
        //il nickname è valido?
        console.log("aggiungiFoto - nickname: " + self.scope.nickname);
        if (self.scope.nickname=="")
        {
            self.scope.stato=3;
        }
        else
        {
            //vado direttamente al caricamento delle foto
            self.scope.selectImages();  
        }

    };

    self.scope.richiediStampa = function(tmp_email,tmp_tel) {
        self.scope.warn_message="";

        if (self.scope.images_list.length==0)
        {
            self.scope.warn_message=self.LanguageService.getLabel("CARRELLO_NESSUNA_IMMAGINE_PRESENTE");
        }
        else
        {
            console.log(tmp_email,tmp_tel)
            if ((tmp_email!=undefined) && (tmp_email!="")) //almeno l'email è stata inserita
            {
                if ((tmp_tel==undefined) || (tmp_tel=="")) //ma il telefono no....
                {
                    self.scope.warn_message=self.LanguageService.getLabel("CARRELLO_INSERIRE_TELEFONO");
                }
                else //sia email che telefono inseriti
                {
                    //controllo la correttezza dell'email
                    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    if (reg.test(tmp_email))
                    {
                        //tutto ok, anche email valida

                    }
                    else
                    {
                        self.scope.warn_message=self.LanguageService.getLabel("CARRELLO_INSERIRE_EMAIL_VALIDA");
                    }

                }
            }
            else //sicuramente l'email non è stata inserita
            {
                if ((tmp_tel==undefined) || (tmp_tel=="")) //e anche il telefono non è stato inserito....
                {
                    self.scope.warn_message=self.LanguageService.getLabel("CARRELLO_INSERIRE_EMAIL_TELEFONO");
                }
                else //il telefono almeno si....
                {
                    self.scope.warn_message=self.LanguageService.getLabel("CARRELLO_INSERIRE_EMAIL");
                }

            }
        }        

        if (self.scope.warn_message!="")
        {
            self.scope.show_warn_data=true;
            setTimeout(function() {
                self.scope.show_warn_data=false;
            }, 4000);       
            return;
        } 

        //tutto ok, faccio uscire il disclaimer
        self.scope.showDisclaimer=true;


    }

    self.scope.annullaStampa = function(tmp_email,tmp_tel) {
        self.scope.showDisclaimer=false;
    }

    self.scope.richiediStampa2 = function(tmp_email,tmp_tel) {

        self.ionicLoading.show({
          template: self.LanguageService.getLabel("CARRELLO_ATTENDERE_INVIO_FOTO")
        });

        //mi memorizzo l'email che ha inserito, per la prossima volta
        self.myWindow.localStorage.setItem("emailCarrello", tmp_email);
        self.myWindow.localStorage.setItem("telCarrello", tmp_tel);

        var xsrf = self.SerializerJQLike({
                prjID: self.GlobalVariables.application.currentProgetto.idProgetto,
                devID: self.GlobalVariables.deviceUUID,
                mail: tmp_email,
                tel: tmp_tel,
                lista: JSON.stringify(self.scope.images_list),
                numFoto: self.scope.images_list.length
            });

        self.http({
            url: self.GlobalVariables.baseUrl + "/momentsOrdineFoto.php",
            responseType: 'json',
            method: 'POST',
            timeout: 20000,
            data: xsrf,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}       
        })
        .then(function(json) {

            console.log("ritorno dall'invio foto:");
            console.log(JSON.stringify(json));
            self.ionicLoading.hide();
            if (json.data.returnCode=="OK")
            {
                carrello_foto_moments.Instance.ionicPopup.alert({
                 title: self.LanguageService.getLabel("CARRELLO_PREVENTIVO_RICHIESTO")
                });

                //cancello tutto l'array delle foto    
                //console.log("Array Carrello prima:");
                //console.log(self.scope.arrayLocalStorage);

                for (var i=0;i<self.scope.arrayLocalStorage.length;i++)
                {
                    //console.log("considero l'elemento "+i);
                    if (self.scope.arrayLocalStorage[i].prjId==self.GlobalVariables.application.currentProgetto.idProgetto)
                    {
                        //console.log("trovata una foto con quel prjId, in posizione "+i+", la cancello");
                        self.scope.arrayLocalStorage.splice(i,1);
                        //console.log("Risultato dell'Array Carrello dopo la cancellazione:");
                        //console.log(self.scope.arrayLocalStorage);
                        //porto i indietro di 1, visto che cancellando, gli elementi dell'array sono shiftati indietro di 1...
                        i=i-1;
                    }
                }
                //console.log("Risultato dell'Array Carrello alla fine della cancellazione totale:");
                //console.log(self.scope.arrayLocalStorage);


                //salvo nuovamente questo array nella localStorage
                self.myWindow.localStorage.setItem("immaginiCarrello", JSON.stringify(self.scope.arrayLocalStorage));
                self.aggiornaListaImmagini();

            }
            else
            {
                carrello_foto_moments.Instance.ionicPopup.alert({
                 title: self.LanguageService.getLabel("CARRELLO_ERRORE_INVIO_ORDINE")
                });                
            }

            self.scope.showDisclaimer=false;
            
            

        }, function(error, xhr) {
            console.log("ritorno dall'invio foto con ERRORE:");
            console.log(JSON.stringify(error));
            self.ionicLoading.hide();
            carrello_foto_moments.Instance.ionicPopup.alert({
             title: self.LanguageService.getLabel("ERRORE_INTERNO")
            });
            self.scope.showDisclaimer=false;

        });







  

    }





    //self.scope.nickname=self.LazyLoader.appConfiguration.commentsNickname;
    //console.log("Nickname: " + self.scope.nickname);

    self.scope.warn_message="";
    self.scope.show_warn_data=false;
    self.scope.numeroFotoPresenti=-1;
    self.scope.showDisclaimer=false;
    //self.scope.images_list = [];
    //self.scope.users_list = [];
    //self.scope.lastImage = "";
    //self.scope.stato = 1; //mostra lista immagini
    //self.scope.bigImage = "";
    //self.scope.tmp_nickname = "";
    //self.scope.show_warn_nickname = false;

    
    self.fotoView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" id="appCarrelloScreen" style="background-color: rgba(255,255,255,0.98);background-image: none;">' +
            
            '<a ng-click="destroyView()" style="position:absolute; top: 0px; right: 0px; z-index:99999;" class="button button-icon icon ion-ios-close activated chiudi"></a>' +

            '<ion-content  id="messageScrollableArea" scroll="true" padding="false">' +

                '<div ng-if="numeroFotoPresenti>0">' +

                    '<div ng-show="!showDisclaimer">'+
                        '<p  style="text-align: center;font-size: large;margin-top: 10%;" class="">'+self.LanguageService.getLabel('CARRELLO_INSERISCI_DATI')+'</p>' +

                        '<p style="color:#000000;text-align:left;margin-left:5%;float: left;margin-top: 7px;margin-bottom: 0px;width: 15%;">Email</p>' + 
                        '<input type="text" ng-model="tmp_email" id="email" maxlength="50" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:70%;float: left;margin-bottom: 3%;" placeholder="" value=""/>' + 
                        '<div style="clear:both;"></div>' +
                        '<p style="color:#000000;text-align:left;margin-left:5%;float: left;margin-top: 7px;margin-bottom: 0px;width: 15%;">Tel.</p>' + 
                        '<input type="text" ng-model="tmp_tel" id="tel" maxlength="50" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:70%;float: left;" placeholder="" value=""/>' + 
                        '<div style="clear:both;"></div>' +
                        
                        '<div ng-show="show_warn_data" style="width:90%"><p style="color:#FF0000;margin-left:5%;">{{warn_message}}</p></div>' +
                        
                        '<button ng-click="richiediStampa(tmp_email,tmp_tel)" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 50%;margin-left: 25%;">'+self.LanguageService.getLabel('CARRELLO_RICHIEDI_STAMPA')+'</button>' +
                    '</div>'+


                    '<div ng-show="showDisclaimer" style="border:1px solid black;width:90%;margin-left:5%;font-size: smaller;padding: 2%;margin-top: 5%;">'+
                    
                        'Cliccando su RICHIEDI STAMPA l\'Utente è consapevole che la presente richiesta di preventivo:<br>'+
                        '- non è vincolate per le parti (Fotografo, Utente della App Moments);<br>'+
                        '- veicolerà i dati personali dichiarati dall\'Utente, che dovranno necessariamente essere corretti, pena la impossibilità a procedere con la richiesta;<br>'+
                        '- sarà inviata, via email, al fotografo, che potrà utilizzare i dati personali ricevuti al fine di contattare in modo autonomo l\'Utente. Successivamente alla suddetta email, la responsabilità per qualsiasi azione e/o comunicazione relativa al servizio di stampa ed intercorsa tra l\'Utente ed il fotografo ricade sulle parti. L\'Utente esonera Mediasoft da qualsiasi responsabilità in relazione al servizio di stampa richiesto;<br>'+
                        '- sarà inviata, via email, all\'Utente a conferma del corretto esito della richiesta e come promemoria della stessa.<br>'+
                        'Dopo aver ricevuto il promemoria via email, i dati dell\'Utente saranno inseriti nei nostri database e conservati fino alla eventuale fruizione del servizio di stampa o sino alla richiesta di cancellazione, che dovrà essere inviata a Mediasoft srl all\'indirizzo moments@mediasoftonline.com. '+ 

                    '</div>'+
                    '<div ng-show="showDisclaimer" style="width:90%;margin-left:5%;">'+
                        '<button ng-click="richiediStampa2(tmp_email,tmp_tel)" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 46%;margin-right:8%;float:left;">'+self.LanguageService.getLabel('ACCETTA')+'</button>' +
                        '<button ng-click="annullaStampa()" class="button button-positive" style="background-color: #777777;margin-top:5%;width: 46%;float:left;">'+self.LanguageService.getLabel('ANNULLA')+'</button>' +
                    '</div>'+

                    '<ion-list>' +

                        '<ion-item ng-repeat="url in images_list" class="fotoCarrelloContainer" style="overflow: visible;" >' +

                            '<a ng-click="deleteImage(\'{{url}}\')" style="position:absolute; top: 5px; left: 5px; z-index:99999;color: red;" class="ion-ios-trash chiudi cancellaIcon"></a>' +
//                            '<a ng-click="deleteImage()" style="position:absolute; top: 5px; left: 5px; z-index:99999;color: red;" class="button button-icon icon ion-ios-trash activated chiudi"></a>' +

                            '<div id="" class="fotoCarrello" ion-img-cache-bg style="background-image:url({{url}});">' +
                            '</div>' +

                        '</ion-item>' +

                    '</ion-list>' +

                '</div>' +

                '<div ng-if="numeroFotoPresenti==0" style="padding: 5%;">' +

                    '<p  style="text-align: center;font-size: large;margin-top: 10%;" class="">'+self.LanguageService.getLabel('CARRELLO_NON_SONO_PRESENTI_FOTO')+'</p>' +

                '</div>' +

            '</ion-content>' +

/*
            '<div ng-if="stato == 3" style="position:absolute;background-color: white;padding-top: 10%;padding-left: 0%;padding-right: 5%;height: 100%;width: 100%;">' + 
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+self.LanguageService.getLabel('FOTO_NICKNAME')+'</p>' +
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+self.LanguageService.getLabel('NICKNAME')+'</p>' + 
                '<input type="text" ng-model="tmp_nickname" id="photo_nickname" maxlength="20" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:50%;" placeholder="" value="'+self.LazyLoader.appConfiguration.commentsNickname+'"/>' + 

                '<div ng-show="show_warn_nickname" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+self.LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_NICK')+'</p></div>' +
                '<button ng-click="salvaNick(tmp_nickname)" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 50%;margin-left: 25%;">Salva Nickname</button>' +
            '</div>' +
 */             

        '</ion-modal-view>',

        {
            scope: self.scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false
        }

    );


    self.fotoView.show().then(function() {

        self.ionicLoading.show({
          template: self.LanguageService.getLabel('ATTENDERE')
        });

        self.aggiornaListaImmagini();

        
/*
        self.getData({
            onComplete: function(err, json) {
                
                self.ionicLoading.hide();

                if(err) {

                    var alertErr = self.ionicPopup.alert({
                      title: self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione',
                      subTitle: self.LanguageService ? self.LanguageService.getLabel("RIPROVARE_PIU_TARDI") : 'Riprovare più tardi',
                      template: err
                    });

                    alertErr.then(function(res) {
                    	self.scope.destroyView();
                    });

                    return;
                    

                } else {

                	console.log(json);
                    //ciclo su tutte le immagini, quando cambia il devID è come se cambiasse utente
                    var deviId_corrente = "";
                    var nome_corrente = "";
                    var images_list_corrente = [];
                    var timestamp_ultimo = 0;
                    var nome_ultimo = "";
                    for (var i=0;i<json.files.length;i++)
                    {
                        console.log("considero l'immagine: " + json.files[i]);
                        //prendo il devId di questa immagine
                        var start=json.files[i].indexOf("_PRJ_");
                        var end=json.files[i].indexOf("_DEV_");
                        var deviId_immagine = json.files[i].substring(start+5, end);

                        if (i==0)
                        {
                            deviId_corrente=deviId_immagine;
                        }

                        console.log("devId corrente: " + deviId_corrente);
                        console.log("devId immagine: " + deviId_immagine);

                        if (deviId_corrente!=deviId_immagine)
                        {
                            console.log("Sono passato a un altro devId");
                            //sono passato a un altro devId
                            //come nome prendo il nome che sta sull'ultima immagine
                            var item = new Object();
                            item.name=nome_ultimo;
                            item.images_list = images_list_corrente;
                            self.scope.users_list.push(item);

                            images_list_corrente = [];
                            timestamp_ultimo=0;
                            nome_ultimo="";
                            deviId_corrente=deviId_immagine;
                        }

                        start=json.files[i].indexOf("_DEV_");
                        end=json.files[i].indexOf("_NICK_");
                        var nome_corrente = json.files[i].substring(start+5, end);
                        console.log("nome corrente: " + nome_corrente);

                        //prendo il timestamp, perchè devo prendere il nome più recente
                        start=json.files[i].indexOf("_NICK_");
                        end=json.files[i].indexOf(".jpg");
                        var timestamp_corrente = parseInt(json.files[i].substring(start+6, end));
                        if (timestamp_corrente>timestamp_ultimo)
                        {
                            nome_ultimo=nome_corrente;
                            timestamp_ultimo=timestamp_corrente;
                        }
                        console.log("nome ultimo: " + nome_ultimo);
                        

                        images_list_corrente.push(self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i]);

                        self.scope.lastImage=self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i];
                    }
                    //ho finito, sicuramente sarà rimasto appeso l'ultimo devId
                    var item = new Object();
                    item.name=nome_corrente;
                    item.images_list = images_list_corrente;
                    self.scope.users_list.push(item);



                    console.log(self.scope.users_list);



                }

            }

        });
*/

    });

}
