var contributors_foto_moments = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.ionicLoading = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.LanguageService = null;
    this.LazyLoader = null;
    this.myWindow = null;
    this.FileSystemService = null;
    this.scope = null;


    var coloreInterno = '#FFFFFF';
    var coloreBordo = '#9bbe3a';

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 


    '.fotoContainer {' +
    '   float:left;' + 
    '   border: solid 1px gray;' + 
    '   height:200px;' + 
    '   width:42%;' + 
    '   padding:1px;' + 
    '   margin:11px;' + 
    '}' + 

    '.foto {' +
    '   width:100%;' +
    '   height:100%;' +
    '   background-size:cover;' +
    '   background-position:center center;' +
    '   background-color: rgba(220, 222, 205, 0.41);' +
    '   padding:0px;' +
    '   margin:0px;' +
    '   pointer-events: none;' +
    '   text-decoration:none !important;' +
    '   color:inherit;' +
    '}' +

    '.fotoBig {' +
    '   width:100%;' +
    '   height:100%;' +
    '   background-size: contain;' +
    '   background-repeat: no-repeat;' +
    '   background-position:center center;' +
    '   background-color: rgba(255, 255, 255, 1.0);' +
    '   padding:0px;' +
    '   margin:0px;' +
    '   pointer-events: none;' +
    '   text-decoration:none !important;' +
    '   color:inherit;' +
    '   z-index:9999;' +
    '}' +

    '.chiudi {' +
    '   -webkit-filter: drop-shadow(0px 0px 3px white);' +
    '   filter: drop-shadow(0px 0px 3px white);'+
    '   opacity: 0.8 !important;' + 
    '}' 



    document.getElementsByTagName('head')[0].appendChild(style);



}


L.HtmlIcon = L.Icon.extend({
    options: {
        /*
        html: (String) (required)
        iconAnchor: (Point)
        popupAnchor: (Point)
        */
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    createIcon: function () {
        var div = document.createElement('div');
        div.innerHTML = this.options.html;
        return div;
    },

    createShadow: function () {
        return null;
    }
});


contributors_foto_moments.Instance = null;

contributors_foto_moments.Initialize = function(params) {

    if(contributors_foto_moments.Instance != null) {
      return;
    }

    contributors_foto_moments.Instance = new contributors_foto_moments();
    contributors_foto_moments.Instance.ionicModal = params.ionicModal;
    contributors_foto_moments.Instance.ionicPopup = params.ionicPopup;
    contributors_foto_moments.Instance.HelperService = params.HelperService;
    contributors_foto_moments.Instance.http = params.http;
    contributors_foto_moments.Instance.GlobalVariables = params.GlobalVariables;
    contributors_foto_moments.Instance.LoggerService = params.LoggerService;
    contributors_foto_moments.Instance.SocketService = params.SocketService;
    contributors_foto_moments.Instance.LanguageService = params.LanguageService;
    contributors_foto_moments.Instance.LazyLoader = params.LazyLoader;
    contributors_foto_moments.Instance.myWindow = params.myWindow;
    contributors_foto_moments.Instance.FileSystemService = params.FileSystemService;
    contributors_foto_moments.Instance.ionicLoading = params.ionicLoading;


    // rimuovo la voce di menu dalla mappa di giallo_zafferano quando entro nella homepage //
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsContributorFotoKey', function() {
        delete contributors_foto_moments.Instance;
        contributors_foto_moments.Instance = null;
        params.rootScope.removeItemFromMenu('moments_foto_contributor_key');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsContributorFotoKey', null);
    });

    params.rootScope.addItemToMenu('moments_foto_contributor_key', params.LanguageService ? params.LanguageService.getLabel("CONTRIBUTI_FOTO") : 'Condividi le foto', 'ion-ios-photos', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = contributors_foto_moments.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            contributors_foto_moments.Instance.scope = params.rootScope.$new(true);
            contributors_foto_moments.Instance.run();

        }


       
        

    });


}



contributors_foto_moments.prototype.getData = function(parameters) {

    var self = this;

    console.log("getData della classe dinamica");
    var devID="";
    if (self.GlobalVariables.application.currentProgetto.tipologia=="GUEST")
    {
        //se è guest vede solo le sue foto
        devID=self.GlobalVariables.deviceUUID;
        devID="b1421d20-cead-f821-8622-580363944858";
    }
    
    self.http({
        url: self.GlobalVariables.baseUrl + "/momentsContributi/scandir.php",
        responseType: 'json',
        method: 'GET',
        timeout: 10000,
        params: {
            prjID: self.GlobalVariables.application.currentProgetto.idProgetto,
            //devID: "b1421d20-cead-f821-8622-580363944858"
            devID: devID
        }
    })
    .then(function(json) {
        parameters.onComplete(null, json.data);
    }, function(error, xhr) {
        parameters.onComplete(self.LanguageService ? self.LanguageService.getLabel("VERIFICATO_ERRORE_CONNESSIONE") : 'Si è verificato un errore di connessione Internet', null);
    });

}

contributors_foto_moments.prototype.aggiornaListaImmagini = function() {

    var self = this;
    self.scope.users_list = [];

    self.getData({
        onComplete: function(err, json) {
            
            self.ionicLoading.hide();

            if(err) {

                var alertErr = self.ionicPopup.alert({
                  title: self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione',
                  subTitle: self.LanguageService ? self.LanguageService.getLabel("RIPROVARE_PIU_TARDI") : 'Riprovare più tardi',
                  template: err
                });

                alertErr.then(function(res) {
                    self.scope.destroyView();
                });

                return;
                

            } else {

                console.log(json);
                //ciclo su tutte le immagini, quando cambia il devID è come se cambiasse utente
                var deviId_corrente = "";
                var nome_corrente = "";
                var images_list_corrente = [];
                var timestamp_ultimo = 0;
                var nome_ultimo = "";
                for (var i=0;i<json.files.length;i++)
                {
                    console.log("considero l'immagine: " + json.files[i]);
                    //prendo il devId di questa immagine
                    var start=json.files[i].indexOf("_PRJ_");
                    var end=json.files[i].indexOf("_DEV_");
                    var deviId_immagine = json.files[i].substring(start+5, end);

                    if (i==0)
                    {
                        deviId_corrente=deviId_immagine;
                    }

                    console.log("devId corrente: " + deviId_corrente);
                    console.log("devId immagine: " + deviId_immagine);

                    if (deviId_corrente!=deviId_immagine)
                    {
                        console.log("Sono passato a un altro devId");
                        //sono passato a un altro devId
                        //come nome prendo il nome che sta sull'ultima immagine
                        if (self.GlobalVariables.application.currentProgetto.tipologia=="GUEST") nome_ultimo="Le foto che hai condiviso";
                        else nome_ultimo="Le foto di " + nome_ultimo;

                        var item = new Object();
                        item.name=nome_ultimo;
                        item.images_list = images_list_corrente;
                        self.scope.users_list.push(item);

                        images_list_corrente = [];
                        timestamp_ultimo=0;
                        nome_ultimo="";
                        deviId_corrente=deviId_immagine;
                    }

                    start=json.files[i].indexOf("_DEV_");
                    end=json.files[i].indexOf("_NICK_");
                    var nome_corrente = json.files[i].substring(start+5, end);
                    console.log("nome corrente: " + nome_corrente);

                    //prendo il timestamp, perchè devo prendere il nome più recente
                    start=json.files[i].indexOf("_NICK_");
                    end=json.files[i].indexOf(".jpg");
                    var timestamp_corrente = parseInt(json.files[i].substring(start+6, end));
                    if (timestamp_corrente>timestamp_ultimo)
                    {
                        nome_ultimo=nome_corrente;
                        timestamp_ultimo=timestamp_corrente;
                    }

                    
                    console.log("nome ultimo: " + nome_ultimo);
                    

                    images_list_corrente.push(self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i]);

                    self.scope.lastImage=self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i];
                }
                //ho finito, sicuramente sarà rimasto appeso l'ultimo devId
                if (self.GlobalVariables.application.currentProgetto.tipologia=="GUEST") nome_ultimo="Le foto che hai condiviso";
                else nome_ultimo="Le foto di " + nome_ultimo;
                var item = new Object();
                item.name=nome_ultimo;
                item.images_list = images_list_corrente;
                self.scope.users_list.push(item);
                
                console.log(self.scope.users_list);


            }

        }

    });
}


contributors_foto_moments.prototype.run = function() {

  	var self = this;

    self.scope.destroyView = function() {
        if (self.scope.stato==1)
        {
            self.fotoView.remove();
            self.scope.$destroy();
            delete self.scope;
        }
        else
        {
            self.scope.bigImage = "";
            self.scope.stato=1;
        }        
    };

    self.scope.showPhoto = function(url) {
        if (self.scope.stato==1)
        {
            self.scope.bigImage=url;
            self.scope.stato=2;
        }
    };

    self.scope.deleteImage = function() {
        if (self.scope.stato==2)
        {
            console.log("deleteImage");
            console.log("url: " + self.scope.bigImage);
            var filename = self.scope.bigImage.substring(self.scope.bigImage.lastIndexOf("/")+1);
            console.log(filename);

            self.http({
                url: self.GlobalVariables.baseUrl + "/momentsContributi/removeFile.php",
                responseType: 'json',
                method: 'GET',
                timeout: 10000,
                params: {
                    imageUrl: filename
                }
            })
            .then(function(json) {
                console.log(json);
                self.scope.bigImage = "";
                self.scope.stato=1;
                self.aggiornaListaImmagini();                          
            }, function(error, xhr) {
                console.log(error);
                self.scope.bigImage = "";
                self.scope.stato=1;
                self.aggiornaListaImmagini();                
            });            


        }
    };

    self.scope.aggiungiFoto = function() {
        //il nickname è valido?
        console.log("aggiungiFoto - nickname: " + self.scope.nickname);
        if (self.scope.nickname=="")
        {
            self.scope.stato=3;
        }
        else
        {
            //vado direttamente al caricamento delle foto
            self.scope.selectImages();  
        }

    };

    self.scope.salvaNick = function(tmp_nickname) {
        //controllo il nickname e il commento
        self.scope.tmp_nickname = tmp_nickname;
        console.log("Controllo il nick: " + tmp_nickname);  

        var reg = /[^-A-Za-z0-9 .,!&:_@$^;|<>"'=+*()èéòàùì]/;
        var testNick = reg.test(self.scope.tmp_nickname);    
          
        self.scope.show_warn_nickname=false;

        if (testNick==true)
        {
            console.log("caratteri non ammessi nel nickname");
            self.scope.show_warn_nickname=true;
            setTimeout(function() {
                self.scope.show_warn_nickname=false;
            }, 4000);       
            return;
        }        
          
        //salvo il nickname
        self.LazyLoader.appConfiguration.commentsNickname=self.scope.tmp_nickname;
        self.LazyLoader.saveConfiguration({
        onComplete: function(err) {
            
            self.scope.selectImages();  
            

            }
          
        });
    }

    self.scope.selectImages = function() {

        console.log("selectImages");
        //faccio fare l'upload delle foto
        var options = {
          width: 1000,
          height: 1000,
          quality: 65
        }

        var image2upload = [];
        
        self.myWindow.imagePicker.getPictures(
            function(results) {
                for (var i = 0; i < results.length; i++) {
                    console.log('Image URI: ' + results[i]);
                    image2upload.push(results[i]);
                }
                //chiamo l'upload delle foto
                self.ionicLoading.show({
                  template: 'Attendere, caricamento delle foto in corso'
                });      

                self.scope.uploadImages(image2upload, 0); //prima foto da uploadare

            }, function (error) {
                console.log('Error: ' + error);
            }, options
        );

    
    }

    
    self.scope.uploadImages = function(array, index) {

        /*
        self.ionicLoading.show({
          template: self.LanguageService.getLabel('ATTENDERE')
        });      
        */

        console.log('uploadImages - indice: ' + index);


        //esiste fuesta foto nell'array?
        if (array.length>index) //lo posso fare
        {
            console.log('Pronto a uploadare: ' + array[index]);
            self.FileSystemService.uploadFile({
                fileName: array[index],
                baseUrl: self.GlobalVariables.baseUrl,
                devID: self.GlobalVariables.deviceUUID,
                nick: self.LazyLoader.appConfiguration.commentsNickname,
                prjID: self.GlobalVariables.application.currentProgetto.idProgetto,
                onComplete: function(err) {

                    if(err) {
                        console.log("upload error: " + err);
                    } else {
                        console.log("upload completato");
                    }
                    self.scope.uploadImages(array, index+1);  
                } 
            });
        }
        else
        {
            //ho finito
            self.ionicLoading.hide(); 
            self.aggiornaListaImmagini();              
        }


/*        
        for (var i = 0; i < array.length; i++) {
          console.log('Pronto a uploadare: ' + array[i]);

          self.FileSystemService.uploadFile({
            fileName: array[i],
            baseUrl: self.GlobalVariables.baseUrl,
            devID: self.GlobalVariables.deviceUUID,
            nick: self.LazyLoader.appConfiguration.commentsNickname,
            prjID: self.GlobalVariables.application.currentProgetto.idProgetto 
          });

        }  

        self.ionicLoading.hide();    
*/
    }


    self.scope.nickname=self.LazyLoader.appConfiguration.commentsNickname;
    console.log("Nickname: " + self.scope.nickname);

    //self.scope.images_list = [];
    self.scope.users_list = [];
    self.scope.lastImage = "";
    self.scope.stato = 1; //mostra lista immagini
    self.scope.bigImage = "";
    self.scope.tmp_nickname = "";
    self.scope.show_warn_nickname = false;

    
    self.fotoView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" id="appContributiMapScreen" style="background-color:rgba(255,255,255,0.98);">' +
            
            '<a ng-click="destroyView()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated chiudi"></a>' +

            '<a ng-if="stato==2" ng-click="deleteImage()" style="position:absolute; top: 5px; left: 5px; z-index:99999;color: red;" class="button button-icon icon ion-ios-trash activated chiudi"></a>' +


            '<ion-content  id="messageScrollableArea" scroll="true" padding="false">' +

                '<div>' +

                    '<p  style="text-align: center;font-size: large;margin-top: 5%;" class="">Condividi le tue foto</p>' +
                    
                    '<button ng-click="aggiungiFoto()" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 50%;margin-left: 25%;margin-bottom: 5%;">Aggiungi foto</button>' +


                    '<ion-list>' +

                        '<ion-item ng-repeat="user in users_list" style="overflow: visible;background-color: transparent;" ng-click="">' +

                            '<p  style="text-align: center;font-size: large;margin-top: 5px;" class="">{{user.name}}</p>' +

                            '<ion-list>' +

                                '<ion-item ng-repeat="url in user.images_list" class="fotoContainer" style="overflow: visible;" ng-click="showPhoto(\'{{url}}\')">' +

                                    '<div id="" class="foto" ion-img-cache-bg style="background-image:url({{url}});">' +
                                    '</div>' +

                                '</ion-item>' +

                            '</ion-list>' +

                            '<div style="clear:both"></div>' +

                        '</ion-item>' +

                    '</ion-list>' +

                '</div>' +

            '</ion-content>' +

            '<div id="" ng-if="stato==2" class="fotoBig" ion-img-cache-bg style="background-image:url({{bigImage}});position:absolute;">' +
            '</div>' +  

            '<div ng-if="stato == 3" style="position:absolute;background-color: white;padding-top: 10%;padding-left: 0%;padding-right: 5%;height: 100%;width: 100%;">' + 
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+self.LanguageService.getLabel('FOTO_NICKNAME')+'</p>' +
                '<p style="color:#000000;text-align:left;margin-left:5%;">'+self.LanguageService.getLabel('NICKNAME')+'</p>' + 
                '<input type="text" ng-model="tmp_nickname" id="photo_nickname" maxlength="20" style="border:1px solid;color:#000000;text-align:left;margin-left:5%;width:50%;" placeholder="" value="'+self.LazyLoader.appConfiguration.commentsNickname+'"/>' + 

                '<div ng-show="show_warn_nickname" style="width:90%"><p style="color:#FF0000;margin-left:5%;"><br>'+self.LanguageService.getLabel('SOCIAL_COMMENTO_ERRORE_NICK')+'</p></div>' +
                '<button ng-click="salvaNick(tmp_nickname)" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 50%;margin-left: 25%;">Salva Nickname</button>' +
            '</div>' +
              

        '</ion-modal-view>',

        {
            scope: self.scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false
        }

    );


    self.fotoView.show().then(function() {

        self.ionicLoading.show({
          template: self.LanguageService.getLabel('ATTENDERE')
        });

        self.aggiornaListaImmagini();

        
/*
        self.getData({
            onComplete: function(err, json) {
                
                self.ionicLoading.hide();

                if(err) {

                    var alertErr = self.ionicPopup.alert({
                      title: self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione',
                      subTitle: self.LanguageService ? self.LanguageService.getLabel("RIPROVARE_PIU_TARDI") : 'Riprovare più tardi',
                      template: err
                    });

                    alertErr.then(function(res) {
                    	self.scope.destroyView();
                    });

                    return;
                    

                } else {

                	console.log(json);
                    //ciclo su tutte le immagini, quando cambia il devID è come se cambiasse utente
                    var deviId_corrente = "";
                    var nome_corrente = "";
                    var images_list_corrente = [];
                    var timestamp_ultimo = 0;
                    var nome_ultimo = "";
                    for (var i=0;i<json.files.length;i++)
                    {
                        console.log("considero l'immagine: " + json.files[i]);
                        //prendo il devId di questa immagine
                        var start=json.files[i].indexOf("_PRJ_");
                        var end=json.files[i].indexOf("_DEV_");
                        var deviId_immagine = json.files[i].substring(start+5, end);

                        if (i==0)
                        {
                            deviId_corrente=deviId_immagine;
                        }

                        console.log("devId corrente: " + deviId_corrente);
                        console.log("devId immagine: " + deviId_immagine);

                        if (deviId_corrente!=deviId_immagine)
                        {
                            console.log("Sono passato a un altro devId");
                            //sono passato a un altro devId
                            //come nome prendo il nome che sta sull'ultima immagine
                            var item = new Object();
                            item.name=nome_ultimo;
                            item.images_list = images_list_corrente;
                            self.scope.users_list.push(item);

                            images_list_corrente = [];
                            timestamp_ultimo=0;
                            nome_ultimo="";
                            deviId_corrente=deviId_immagine;
                        }

                        start=json.files[i].indexOf("_DEV_");
                        end=json.files[i].indexOf("_NICK_");
                        var nome_corrente = json.files[i].substring(start+5, end);
                        console.log("nome corrente: " + nome_corrente);

                        //prendo il timestamp, perchè devo prendere il nome più recente
                        start=json.files[i].indexOf("_NICK_");
                        end=json.files[i].indexOf(".jpg");
                        var timestamp_corrente = parseInt(json.files[i].substring(start+6, end));
                        if (timestamp_corrente>timestamp_ultimo)
                        {
                            nome_ultimo=nome_corrente;
                            timestamp_ultimo=timestamp_corrente;
                        }
                        console.log("nome ultimo: " + nome_ultimo);
                        

                        images_list_corrente.push(self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i]);

                        self.scope.lastImage=self.GlobalVariables.baseUrl + "/momentsContributi/" + json.files[i];
                    }
                    //ho finito, sicuramente sarà rimasto appeso l'ultimo devId
                    var item = new Object();
                    item.name=nome_corrente;
                    item.images_list = images_list_corrente;
                    self.scope.users_list.push(item);



                    console.log(self.scope.users_list);



                }

            }

        });
*/

    });

}
