var acquista_moments = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.ionicLoading = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.LanguageService = null;
    this.LazyLoader = null;
    this.myWindow = null;
    this.FileSystemService = null;
    this.scope = null;


    var coloreInterno = '#FFFFFF';
    var coloreBordo = '#9bbe3a';

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 


    '.fotoContainer {' +
    '   float:left;' + 
    '   border: solid 1px gray;' + 
    '   height:200px;' + 
    '   width:42%;' + 
    '   padding:1px;' + 
    '   margin:11px;' + 
    '}' + 

    '.foto {' +
    '   width:100%;' +
    '   height:100%;' +
    '   background-size:cover;' +
    '   background-position:center center;' +
    '   background-color: rgba(220, 222, 205, 0.41);' +
    '   padding:0px;' +
    '   margin:0px;' +
    '   pointer-events: none;' +
    '   text-decoration:none !important;' +
    '   color:inherit;' +
    '}' +

    '.fotoBig {' +
    '   width:100%;' +
    '   height:100%;' +
    '   background-size: contain;' +
    '   background-repeat: no-repeat;' +
    '   background-position:center center;' +
    '   background-color: rgba(255, 255, 255, 1.0);' +
    '   padding:0px;' +
    '   margin:0px;' +
    '   pointer-events: none;' +
    '   text-decoration:none !important;' +
    '   color:inherit;' +
    '   z-index:9999;' +
    '}' +

    '.chiudi {' +
    '   -webkit-filter: drop-shadow(0px 0px 3px white);' +
    '   filter: drop-shadow(0px 0px 3px white);'+
    '   opacity: 0.8 !important;' + 
    '}' 



    document.getElementsByTagName('head')[0].appendChild(style);



}


L.HtmlIcon = L.Icon.extend({
    options: {
        /*
        html: (String) (required)
        iconAnchor: (Point)
        popupAnchor: (Point)
        */
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    createIcon: function () {
        var div = document.createElement('div');
        div.innerHTML = this.options.html;
        return div;
    },

    createShadow: function () {
        return null;
    }
});


acquista_moments.Instance = null;

acquista_moments.Initialize = function(params) {

    if(acquista_moments.Instance != null) {
      return;
    }

    acquista_moments.Instance = new acquista_moments();
    acquista_moments.Instance.ionicModal = params.ionicModal;
    acquista_moments.Instance.ionicPopup = params.ionicPopup;
    acquista_moments.Instance.HelperService = params.HelperService;
    acquista_moments.Instance.http = params.http;
    acquista_moments.Instance.GlobalVariables = params.GlobalVariables;
    acquista_moments.Instance.LoggerService = params.LoggerService;
    acquista_moments.Instance.SocketService = params.SocketService;
    acquista_moments.Instance.LanguageService = params.LanguageService;
    acquista_moments.Instance.LazyLoader = params.LazyLoader;
    acquista_moments.Instance.myWindow = params.myWindow;
    acquista_moments.Instance.FileSystemService = params.FileSystemService;
    acquista_moments.Instance.ionicLoading = params.ionicLoading;


    // rimuovo la voce di menu dalla mappa di giallo_zafferano quando entro nella homepage //
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsAcquistaKey', function() {
        delete acquista_moments.Instance;
        acquista_moments.Instance = null;
        params.rootScope.removeItemFromMenu('moments_acquista_key');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeMomentsAcquistaKey', null);
    });

    params.rootScope.addItemToMenu('moments_acquista_key', params.LanguageService ? params.LanguageService.getLabel("ACQUISTA") : 'Acquista', 'ion-card', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = acquista_moments.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            acquista_moments.Instance.scope = params.rootScope.$new(true);
            acquista_moments.Instance.run();

        }


       
        

    });


}






acquista_moments.prototype.run = function() {

  	var self = this;

    self.scope.destroyView = function() {

        self.acquistaView.remove();
        self.scope.$destroy();
        delete self.scope;

     
    };

    self.scope.vaiSito = function() {

        console.log("Vado al sito");
        if(self.myWindow.cordova && self.myWindow.cordova.InAppBrowser) {
            self.myWindow.cordova.InAppBrowser.open("https://www.moments-ar.it", "_system");
        } else {
            self.myWindow.open("https://www.moments-ar.it",'_blank');
        }

     
    };


    //self.scope.nickname=self.LazyLoader.appConfiguration.commentsNickname;
    //console.log("Nickname: " + self.scope.nickname);

    //self.scope.images_list = [];
    //self.scope.users_list = [];
    //self.scope.lastImage = "";
    //self.scope.stato = 1; //mostra lista immagini
    //self.scope.bigImage = "";
    //self.scope.tmp_nickname = "";
    //self.scope.show_warn_nickname = false;

    
    self.acquistaView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" id="appContributiMapScreen" style="background-color:rgba(255,255,255,0.98);">' +

            '<div style="position: absolute;opacity: 0.2;width:100%;height: 100%;background-image: url(http://livengine.mediasoftonline.com/momentsStuff/slide01.jpg);background-size: cover;">' +

            '</div>' +

            '<a ng-click="destroyView()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated chiudi"></a>' +


            '<ion-content  id="messageScrollableArea" scroll="true" padding="false">' +

                '<div style="margin-top: 10%;">' +

                    '<p  style="text-align: center;font-size: large;margin-top: 5%;" class="">Acquista Moments</p>' +
                    

                    '<p  style="text-align: center;font-size: medium;margin-top: 10%;padding:5%;" class="">Sei un fotografo?<br>Acquista la suite Moments per offrire ai tuoi clienti la Realtà Aumentata sui loro Album!</p>' +


                    '<button ng-click="vaiSito()" class="button button-positive" style="background-color: #9acf16;margin-top:5%;width: 50%;margin-left: 25%;margin-bottom: 5%;">Visita il nostro Sito</button>' +

                    '<p  style="text-align: center;font-size: medium;margin-top: 10%;padding:5%;" class="">Siete una coppia di Sposi?<br>Fate conoscere al vostro fotografo la App di Moments!</p>' +

                '</div>' +

            '</ion-content>' +





        '</ion-modal-view>',

        {
            scope: self.scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false
        }

    );


    self.acquistaView.show().then(function() {



        

    });

}
