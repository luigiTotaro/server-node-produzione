var Gradimento_cosmo = function(container,dataObj, closeFn, externalFn) {
   
   
   this.container = container;
   this.baseUrl = dataObj.contenutiUrl;
   this.oggettoTarget = dataObj.oggetto;
   this.nome = "gradimento";
   this.voto =-1;

   console.log("Classe dinamica: "+this.nome);

   //this.AR = ar;
   this.ARclose=closeFn;
   this.externalFunction=externalFn;

   this.itemVuoto="img/stellaOff.png";
   this.itemPieno="img/stellaOn.png";
   this.itemQuarto="img/stellaOnQuarto.png";
   this.itemMezzo="img/stellaOnMezzo.png";
   this.itemTreQuarti="img/stellaOnTrequarti.png";
   //this.idPunto = idPunto;

   this.arrayImages=new Array();
   this.arrayImages.push(this.itemVuoto);
   this.arrayImages.push(this.itemPieno);
   this.arrayImages.push(this.itemQuarto);
   this.arrayImages.push(this.itemMezzo);
   this.arrayImages.push(this.itemTreQuarti);

    this.descrizione = "";

   //this.preloadImages();


}



Gradimento_cosmo.prototype.renderMain = function(immagine, punto, deviceId) {

    //console.log("RenderMain, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //finestra a tutto schermo
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,2555,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();

    //devo sapere se ha votato o no....
    var idPunto=this.oggettoTarget[immagine].punti[punto].id;
    var label=this.oggettoTarget[immagine].punti[punto].label;
    var url=this.baseUrl + "/checkVoto.php?idPunto="+idPunto+"&deviceId="+deviceId;
    //console.log("URL per checkVoto: " + url);

    //trovo quello che riguarda la votazione
    var config=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    for (var z=0;z<config.contenuto.length;z++)
    {
      if (config.contenuto[z].tipo=="Gradimento")
      {
        if (typeof config.contenuto[z].descrizione != 'undefined')
        {
          this.descrizione = config.contenuto[z].descrizione;
        }
        else
        {
          this.descrizione = "";
        }
      }
    }

    this.continueRenderMain(url, label, idPunto, deviceId);
}

Gradimento_cosmo.prototype.renderMainExt = function(immagine, punto, indice, deviceId) {

    console.log("renderMainExt, immagine: " + immagine + " - punto: " + punto + " - indice: " + indice + " - deviceId: " + deviceId);
    console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //finestra a tutto schermo
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,2555,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();

    //devo sapere se ha votato o no....
    var idPunto=this.oggettoTarget[immagine].punti[punto].id;
    var label=this.oggettoTarget[immagine].punti[punto].label;
    var url=this.baseUrl + "/checkVoto.php?idPunto="+idPunto+"&deviceId="+deviceId;
    //console.log("URL per checkVoto: " + url);

    //trovo quello che riguarda la votazione
    var oggettoContenuti=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    var oggettoPunto=oggettoContenuti[indice];
    var config=JSON.parse(oggettoPunto.config);
    this.descrizione = config.descrizione;

    this.continueRenderMain(url, label, idPunto, deviceId)
}


Gradimento_cosmo.prototype.continueRenderMain = function(url, label, idPunto, deviceId) {

    //console.log("continueRenderMain, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $.ajax({
      url: url,
      dataType: 'json'
      })
      .done(function( json )
      {
        //console.log("arrivata risposta dal checkVoto");
        //console.log(JSON.stringify(json));

        $("#loader").hide();

        if (json.myvoto.voto==-100)
        {
          //questo errore è solo se non ha passato i dati alla pagina che vede 
          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Attenzione, possibili problemi di connessione</p></div>";
          html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          $("#"+self.nome+"_chiudi").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();
        }
        else if (json.myvoto.voto==-1)
        {
          //alert("Non votato");
          //metto le stellette per votare
          var srcOn = self.baseUrl+"/"+self.itemPieno;
          var srcOff = self.baseUrl+"/"+self.itemVuoto;


          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Dai un voto a: "+label+"</p>";
          html+="<div id='"+self.nome+"_itemContainer' style='width:100%;border:0px solid red;'>";
          html+="<p class='dynText' style='color:#000000;border:0px solid #ffff00;'>"+self.descrizione+"</p>";

          for (var i=1;i<=5;i++) html+="<img id='"+self.nome+"_item"+i+"' src='"+srcOff+"' style='width:20%;float:left'>";
          html+="</div><div style='clear:both;'></div>";

          //metto i pulsanti di annulla e vota, ma vota si abilita solo se ha messo almeno una stelletta
          html+="<div id='"+self.nome+"_vota' class='dynButton' style='color:#000000;border:1px solid #555555;display:none;'>VOTA</div>";
          html+="<div id='"+self.nome+"_annulla' class='dynButton' style='color:#000000;border:1px solid #555555;'>ANNULLA</div>";

          html+="</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          
          $("#"+self.nome+"_item1").click(function(e) {
              self.itemClick(1);
          });
          $("#"+self.nome+"_item2").click(function(e) {
              self.itemClick(2);
          });
          $("#"+self.nome+"_item3").click(function(e) {
              self.itemClick(3);
          });
          $("#"+self.nome+"_item4").click(function(e) {
              self.itemClick(4);
          });
          $("#"+self.nome+"_item5").click(function(e) {
              self.itemClick(5);
          });
          $("#"+self.nome+"_vota").click(function(e) {
              self.vota(idPunto,deviceId);
          });
          $("#"+self.nome+"_annulla").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();

        }
        else
        {
          //alert("Già votato, voto: " + json.voto);
          var mioVoto=json.myvoto.voto;
          var numeroVoti=json.voti.length;
          var totale=0;
          for (var i=0;i<numeroVoti;i++)
          {
            totale+=parseInt(json.voti[i].voto);
          }
          var media=totale/numeroVoti;

          var srcOn = self.baseUrl+"/"+self.itemPieno;
          var srcOff = self.baseUrl+"/"+self.itemVuoto;
          var srcOnQuarto=self.baseUrl+"/"+self.itemQuarto;
          var srcOnMezzo=self.baseUrl+"/"+self.itemMezzo;
          var srcOnTrequarti=self.baseUrl+"/"+self.itemTreQuarti;


          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Hai già votato per "+label+", e il tuo voto è stato:</p>";
          html+="<div id='"+self.nome+"_itemContainer' style='width:100%;'>";
          html+="<p class='dynText' style='color:#000000;border:0px solid #ffff00;'>"+self.descrizione+"</p>";

          //console.log("miovoto: " + mioVoto);
          for (var i=1;i<=mioVoto;i++)
          {
            html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
          }
          //console.log("miovoto: " + mioVoto)
          for (var t=mioVoto;t<5;t++)
          {
            html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
          }
          //console.log("html: " + html);
          html+="</div><div style='clear:both;'></div><br>";
          html+="<div><p class='dynLabel' style='color:#000000;'>La community ha espresso "+numeroVoti+" voti, la cui media è:</p>";
          html+="<div id='"+self.nome+"_itemContainer2' style='width:100%;'>";

          var parteIntera=Math.floor(media);
          var disegnati=0;
          //fino a tutta la parte intera, disegno pieni
          for (var i=1;i<=parteIntera;i++)
          {
            html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
            disegnati++;
          }
          //metto l'eventuale parte decimale
          if (parteIntera<5)
          {
            var parteDecimale=media-parteIntera;
            if (parteDecimale<=0.125) html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
            else if(parteDecimale<=0.375) html+="<img src='"+srcOnQuarto+"' style='width:20%;float:left'>";
            else if(parteDecimale<=0.625) html+="<img src='"+srcOnMezzo+"' style='width:20%;float:left'>";
            else if(parteDecimale<=0.875) html+="<img src='"+srcOnTrequarti+"' style='width:20%;float:left'>";
            else html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
            disegnati++;
          }
          //arrivo alla fine con item vuoti
          for (var i=disegnati+1;i<=5;i++)
          {
            html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
          }
          html+="</div><div style='clear:both;'></div>";

          //metto il pulsante per chiudere
          html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

          html+="</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          $("#"+self.nome+"_chiudi").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();

        }

      })
      .fail(function( jqXHR, textStatus ) {
          //console.log( "Request failed: " + textStatus );
          //alert("Errore2");
          //probabile mancanza di rete
          html="";
          html+="<div><p class='dynLabel' style='color:#000000;'>Attenzione, probabili problemi di connessione</p></div>";
          html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

          var htmlObj = $(html);

          //htmlObj.appendTo($("#"+self.nome+"_container"));
          $("#"+self.nome+"_container").html(htmlObj);
          //$("#"+this.nome+"_container").append(html);

          $("#"+self.nome+"_chiudi").click(function(e) {
              self.container.html("");
              self.container.hide();
              self.ARclose();
              //self.AR.hardware.camera.enabled = true;
          });

          self.resizeBasedWindows();

          $("#loader").hide();
      });
}




Gradimento_cosmo.prototype.vota = function(idPunto,deviceId) {
  
  $("#loader").show();

  var self = this;

  var url=this.baseUrl + "/setVoto.php?idPunto="+idPunto+"&deviceId="+deviceId+"&voto="+this.voto;
  //console.log("URL per setVoto: " + url);

  $.ajax({
    url: url,
    dataType: 'json'
    })
    .done(function( json )
    {
      //console.log("arrivata risposta dal setVoto");
      //la risposta comprende altri dati, tipo il numero di voti fin qui espressi e la loro media

      $("#loader").hide();

      //console.log(JSON.stringify(json));

      var numeroVoti=json.length;
      var totale=0;
      for (var i=0;i<numeroVoti;i++)
      {
        totale+=parseInt(json[i].voto);
      }
      var media=totale/numeroVoti;

      var srcOn = self.baseUrl+"/"+self.itemPieno;
      var srcOff = self.baseUrl+"/"+self.itemVuoto;
      var srcOnQuarto=self.baseUrl+"/"+self.itemQuarto;
      var srcOnMezzo=self.baseUrl+"/"+self.itemMezzo;
      var srcOnTrequarti=self.baseUrl+"/"+self.itemTreQuarti;


      html="";
      html+="<div><p class='dynLabel' style='color:#000000;'>Grazie di aver votato! Il tuo voto è:</p>";
      html+="<div id='"+self.nome+"_itemContainer' style='width:100%;'>";
      for (var i=1;i<=self.voto;i++)
      {
        html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
      }
      for (var i=self.voto;i<5;i++)
      {
        html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
      }
      html+="</div><div style='clear:both;'></div><br>";
      html+="<div><p class='dynLabel' style='color:#000000;'>La community ha espresso "+numeroVoti+" voti, la cui media è:</p>";
      html+="<div id='"+self.nome+"_itemContainer2' style='width:100%;'>";

      var parteIntera=Math.floor(media);
      var disegnati=0;
      //fino a tutta la parte intera, disegno pieni
      for (var i=1;i<=parteIntera;i++)
      {
        html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
        disegnati++;
      }
      //metto l'eventuale parte decimale
      if (parteIntera<5)
      {
        var parteDecimale=media-parteIntera;
        if (parteDecimale<=0.125) html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
        else if(parteDecimale<=0.375) html+="<img src='"+srcOnQuarto+"' style='width:20%;float:left'>";
        else if(parteDecimale<=0.625) html+="<img src='"+srcOnMezzo+"' style='width:20%;float:left'>";
        else if(parteDecimale<=0.875) html+="<img src='"+srcOnTrequarti+"' style='width:20%;float:left'>";
        else html+="<img src='"+srcOn+"' style='width:20%;float:left'>";
        disegnati++;
      }
      //arrivo alla fine con item vuoti
      for (var i=disegnati+1;i<=5;i++)
      {
        html+="<img src='"+srcOff+"' style='width:20%;float:left'>";
      }
      html+="</div><div style='clear:both;'></div>";

      //metto il pulsante per chiudere
      html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

      html+="</div>";

      var htmlObj = $(html);

      //htmlObj.appendTo($("#"+self.nome+"_container"));
      $("#"+self.nome+"_container").html(htmlObj);
      //$("#"+this.nome+"_container").append(html);

      $("#"+self.nome+"_chiudi").click(function(e) {
          self.container.html("");
          self.container.hide();
          self.ARclose();
          //self.AR.hardware.camera.enabled = true;
     });

      self.resizeBasedWindows();

    })
    .fail(function( jqXHR, textStatus ) {
        //console.log( "Request failed: " + textStatus );
        //alert("Errore2");
        html="";
        html+="<div><p class='dynLabel' style='color:#000000;'>Attenzione, probabili problemi di connessione</p></div>";
        html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";

        var htmlObj = $(html);

        //htmlObj.appendTo($("#"+self.nome+"_container"));
        $("#"+self.nome+"_container").html(htmlObj);
        //$("#"+this.nome+"_container").append(html);

        $("#"+self.nome+"_chiudi").click(function(e) {
            self.container.html("");
            self.container.hide();
            self.ARclose();
            //self.AR.hardware.camera.enabled = true;
        });

        self.resizeBasedWindows();

        $("#loader").hide();
    });

}


Gradimento_cosmo.prototype.itemClick = function(item) 
{
  //console.log("Sono in itemClick, con item: " + item);

  var srcOn = this.baseUrl+"/"+this.itemPieno;
  var srcOff = this.baseUrl+"/"+this.itemVuoto;

  for (var i=1;i<=5;i++) $("#"+this.nome+"_item"+i).attr("src",srcOff);

  if ((item>0) && (item<=5))
  {
    for (var i=1;i<=item;i++) $("#"+this.nome+"_item"+i).attr("src",srcOn);
    $("#"+this.nome+"_vota").show();
    this.voto=item;
  }
}


Gradimento_cosmo.prototype.resizeBasedWindows = function() {

    var self = this;
    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    //console.log("Altezza container: " + altezzaContenitore);
    $("#"+this.nome+"_container").css("width",(larghezzaContenitore-(padding*2))+"px").css("left",(padding)+"px").css("height",(altezzaContenitore-(padding*2))+"px").css("top",(padding)+"px");

    var altezzaInner=$("#"+this.nome+"_container").height();
    var larghezzaInner=$("#"+this.nome+"_container").width();

    if (altezzaContenitore>larghezzaContenitore) //siamo in verticale
    {
      $("#"+this.nome+"_itemContainer").css("width","90%").css("margin-left","5%"); 
      $("#"+this.nome+"_itemContainer2").css("width","90%").css("margin-left","5%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.03).css("padding-top",altezzaContenitore*0.015).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.025).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.00).css("width","100%").css("margin-left","0px");
      $(".dynButton").css("font-size",altezzaContenitore*0.03).css("padding",altezzaContenitore*0.015).css("width","50%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.25)-(altezzaContenitore*0.015));
    }
    else //siamo in orizzontale
    {
      $("#"+this.nome+"_itemContainer").css("width","50%").css("margin-left","25%"); 
      $("#"+this.nome+"_itemContainer2").css("width","50%").css("margin-left","25%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.04).css("padding-top",altezzaContenitore*0.02).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.035).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.00).css("width","150%").css("margin-left","-25%");
      $(".dynButton").css("font-size",altezzaContenitore*0.05).css("padding",altezzaContenitore*0.015).css("width","30%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.35)-(altezzaContenitore*0.015));
   }

}

Gradimento_cosmo.prototype.test = function() {

    var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Gradimento_cosmo.prototype.chiSono = function() {
    return "Gradimento";
}

Gradimento_cosmo.prototype.getIcon = function() {

    var obj= new Object();
    obj.img="dynamicIcons/vota_fj.png";
    obj.label="Vota";
    return obj;
}

Gradimento_cosmo.prototype.preloadImages = function() {

    //console.log("sono in preloadImages");
    
    var imgs = new Array();
    for (var i=0;i<this.arrayImages.length;i++)
    {
      //console.log("Preload: " + this.baseUrl+"/"+ this.arrayImages[i]);
      imgs[i] = new Image();
      imgs[i].src = this.baseUrl+"/"+ this.arrayImages[i];
    }

    var iconaObj=this.getIcon();
    //console.log("Preload: " + this.baseUrl+"/"+ iconaObj.img);
    imgs[this.arrayImages.length] = new Image();
    imgs[this.arrayImages.length].src = this.baseUrl+"/"+ iconaObj.img;

}

