

var client_game_quiz_gz = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.FileSystemService = null;
    this.scope = null;
    this.gameCenter = null;
    this.rootScope = null;
    this.LanguageService = null;
    

    this.hasPendingRequest = false;

    this.currentGame = null;

    this.currentAlertBox = null;
    
    // scope dedicato alla vista che indica l'indizio
    this.clue_scope = null;

    // scope dedicato alla finestra di gestione delle immagini
    this.questions_scope = null;

    this.stats = null;

    this.config = {
        
        name: 'Rispondi a tutte le domande',
        description: 'Rispondi a tutte le domande. Vince chi risponde a tutte le domande.',
        key: 'mediasoft.quiz.gz',
        short_key: 'MQGZGZ0',
        time_to_live: 60000,

        icon: 'ion-person-stalker',
        second_icon: 'ion-person-stalker',
        color: 'green',
        index: 2,

        parameters: {
            parameters: {
                background: {
                    type: 'img',
                    description: 'Immagine di sfondo usata nella lista dei giochi'
                },
                
                advice: {
                    type: 'text',
                    description: 'Consiglio sugli articoli da conoscere per rispondere alle domande'
                },

                lista_domande: {
                    type: 'questions',
                    description: 'lista delle domande con relative risposte'
                }
            }
        },

        values: null

    };

}


client_game_quiz_gz.Instance = null;



client_game_quiz_gz.Initialize = function(params) {

    if(client_game_quiz_gz.Instance != null) {
        return;
    }    

    client_game_quiz_gz.Instance = new client_game_quiz_gz();
    client_game_quiz_gz.Instance.ionicModal = params.ionicModal;
    client_game_quiz_gz.Instance.ionicPopup = params.ionicPopup;
    client_game_quiz_gz.Instance.HelperService = params.HelperService;
    client_game_quiz_gz.Instance.http = params.http;
    client_game_quiz_gz.Instance.GlobalVariables = params.GlobalVariables;
    client_game_quiz_gz.Instance.LoggerService = params.LoggerService;
    client_game_quiz_gz.Instance.FileSystemService = params.FileSystemService;
    client_game_quiz_gz.Instance.SocketService = params.SocketService;
    client_game_quiz_gz.Instance.gameCenter = params.GameCenter;
    client_game_quiz_gz.Instance.rootScope = params.RootScope;
    client_game_quiz_gz.Instance.LanguageService = params.LanguageService;
    
    var SEED = client_game_quiz_gz.Instance.config.short_key;

    //traduzioni
    client_game_quiz_gz.Instance.config.name = client_game_quiz_gz.Instance.LanguageService ? client_game_quiz_gz.Instance.LanguageService.getLabel("RISPONDI_TUTTE_DOMANDE") : 'Rispondi a tutte le domande';    
    client_game_quiz_gz.Instance.config.description = client_game_quiz_gz.Instance.LanguageService ? client_game_quiz_gz.Instance.LanguageService.getLabel("VINCE_CHI_RISPONDE_TUTTE") : 'Rispondi a tutte le domande. Vince chi risponde a tutte le domande.';    


    client_game_quiz_gz.Instance.gameCenter.loadStats({
        key: client_game_quiz_gz.Instance.config.key,
        title: client_game_quiz_gz.Instance.config.name,
        onComplete: function(err, data) { 
        }
    });
    
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', function() {
        
        client_game_quiz_gz.Instance.gameCenter.unregisterGame({
            key: client_game_quiz_gz.Instance.config.key,
            title: client_game_quiz_gz.Instance.config.name,
            description: client_game_quiz_gz.Instance.description
        });
       
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite',                      SEED + 'onGameInvite', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite_ack',                  SEED + 'onGameInviteAck', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite_timeout',              SEED + 'onGameInviteTimeout', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_message',                     SEED + 'onGameMsg', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite_inviter_accept',       SEED + 'onInviterAccept', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_invite_invited_accept',       SEED + 'onInvitedAccept', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_win_by_forfait',              SEED + 'onGameWinByForfait', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_victory',                     SEED + 'onGameVictory', null);
        client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key,     'game_lost',                        SEED + 'onGameLost', null);

        client_game_quiz_gz.Instance.gameCenter.setDeviceEventCallback(client_game_quiz_gz.Instance.config.key, 'pause',                            SEED + 'onGamePaused', null);
        client_game_quiz_gz.Instance.gameCenter.setDeviceEventCallback(client_game_quiz_gz.Instance.config.key, 'resume',                           SEED + 'onGameResumed', null);
        
        
    });



    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', function() {        

        client_game_quiz_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', null);
        client_game_quiz_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', null);
        client_game_quiz_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', null);
        
        client_game_quiz_gz.Instance.gameCenter.removeGameStat(client_game_quiz_gz.Instance.config.key);
        client_game_quiz_gz.Instance = null;    

    });


    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', function() {

 
            client_game_quiz_gz.Instance.gameCenter.getConfiguration({
                key: client_game_quiz_gz.Instance.config.key,
                onComplete: function(err, configuration) {                     
                    
                    if(!err && configuration.values && configuration.visible == true) {                         

                        if(configuration.forDebugOnly == false || (configuration.forDebugOnly == true && client_game_quiz_gz.Instance.GlobalVariables.isDebugDevice == true)) {

                            client_game_quiz_gz.Instance.config.values = [];

                            /*
                            var image_list_parameter = _.find(configuration.values, function(item) { return item.name == 'lista_immagine_da_trovare' });                
                            client_game_quiz_gz.Instance.config.values['lista_immagine_da_trovare'] = JSON.parse(image_list_parameter.value);   

                            var totale_immagini = _.find(configuration.values, function(item) { return item.name == 'totale_immagini' });   
                            client_game_quiz_gz.Instance.config.values['totale_immagini'] =  totale_immagini.value;     
                            */

                            var background = _.find(configuration.values, function(item) { return item.name == 'background' });      
                            background = background.value ? (JSON.parse(background.value)).path : "";
                            client_game_quiz_gz.Instance.config.values['background'] = client_game_quiz_gz.Instance.GlobalVariables.baseUrl + "/" + background;

                            var advice = _.find(configuration.values, function(item) { return item.name == 'advice' });   
                            client_game_quiz_gz.Instance.config.values['advice'] =  advice.value;    

                            var questions = _.find(configuration.values, function(item) { return item.name == 'lista_domande' });  
                            questions = JSON.parse(questions.value);
 
                            client_game_quiz_gz.Instance.config.values['lista_domande'] = [];

                            _.each(questions, function(q) {

                                console.log(q);

                                client_game_quiz_gz.Instance.config.values['lista_domande'].push({

                                    text: q.text,
                                    background: q.background ? client_game_quiz_gz.Instance.GlobalVariables.baseUrl + "/" + q.background : './img/background_zoom.jpg',
                                    answers: q.answer
                                    
                                });


                            });

                             

                            try {

                                client_game_quiz_gz.Instance.gameCenter.registerGame({
                                    key: client_game_quiz_gz.Instance.config.key,
                                    icon: client_game_quiz_gz.Instance.config.icon,
                                    second_icon: client_game_quiz_gz.Instance.config.second_icon,
                                    color: client_game_quiz_gz.Instance.config.color,
                                    index: client_game_quiz_gz.Instance.config.index,
                                    title: client_game_quiz_gz.Instance.config.name,
                                    description: client_game_quiz_gz.Instance.config.description,
                                    classInstance: client_game_quiz_gz.Instance,
                                    background: client_game_quiz_gz.Instance.config.values['background'],
                                    onClickFunctionName: 'startMatchMaking'
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite', SEED + 'onGameInvite', function(message) {
                                    client_game_quiz_gz.Instance.onReceiveGameRequest(message);
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite_ack', SEED + 'onGameInviteAck', function(message) {
                                    client_game_quiz_gz.Instance.onReceiveGameRequestAck(message);
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite_timeout', SEED + 'onGameInviteTimeout', function(message) {            
                                    client_game_quiz_gz.Instance.onReceiveGameRequestTimeout(message);
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', function(message) {            
                                    client_game_quiz_gz.Instance.onReceiveGameRequestExpiredOrCancelled(message);
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_message', SEED + 'onGameMsg', function(message) {                                      
                                    
                                    if(message.sub_type && message.sub_type == 'opponent_has_answered_all_questions') {
                                        client_game_quiz_gz.Instance.onOpponentHasAnsweredAllQuestions(message);    
                                    }
                                    
                                });
                                
                                // ############################################################################ // 
                                // ricezione di un messaggio che indica che un utente ha accettato la MIA sfida //
                                // ############################################################################ //
                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite_inviter_accept', SEED + 'onInviterAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_quiz_gz.Instance.gameCenter.alterStat({
                                        key: client_game_quiz_gz.Instance.config.key,
                                        title: client_game_quiz_gz.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_quiz_gz.Instance.onReceiveGameRequestInviterAccept(message);
                                });

                                // ######################################################################################################## //
                                // ricezione di un messaggio che indica che ho correttamente accettato la sfida lanciata da un ALTRO utente //
                                // ######################################################################################################## //
                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_invite_invited_accept', SEED + 'onInvitedAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_quiz_gz.Instance.gameCenter.alterStat({
                                        key: client_game_quiz_gz.Instance.config.key,
                                        title: client_game_quiz_gz.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_quiz_gz.Instance.onReceiveGameRequestInvitedAccept(message);
                                });


                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_win_by_forfait', SEED + 'onGameWinByForfait', function(message) {     
                                    
                                    // incrementa le partite giocate  
                                    client_game_quiz_gz.Instance.gameCenter.alterStat({
                                        key: client_game_quiz_gz.Instance.config.key,
                                        title: client_game_quiz_gz.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    client_game_quiz_gz.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: true,
                                        victory_condition: null,
                                    });
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_victory', SEED + 'onGameVictory', function(message) {         
                                    
                                    // incrementa le partite giocate  
                                    client_game_quiz_gz.Instance.gameCenter.alterStat({
                                        key: client_game_quiz_gz.Instance.config.key,
                                        title: client_game_quiz_gz.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    console.log(message);
                                    
                                    client_game_quiz_gz.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: false,
                                        victory_condition: message.victory_condition
                                    });
                                });

                                client_game_quiz_gz.Instance.gameCenter.setMessageCallback(client_game_quiz_gz.Instance.config.key, 'game_lost', SEED + 'onGameLost', function(message) {            
                                    
                                    console.log(message);

                                    client_game_quiz_gz.Instance.onReceiveGameEnding({
                                        isWon: false,
                                        isForfait: false,
                                        victory_condition: message.victory_condition
                                    });
                                });


                                client_game_quiz_gz.Instance.gameCenter.setDeviceEventCallback(client_game_quiz_gz.Instance.config.key, 'pause', SEED + 'onGamePaused', function(message) {            
                                    
                                    client_game_quiz_gz.Instance.onGamePause();

                                });

                                client_game_quiz_gz.Instance.gameCenter.setDeviceEventCallback(client_game_quiz_gz.Instance.config.key, 'resume', SEED + 'onGameResumed', function(message) {    
                                    
                                    client_game_quiz_gz.Instance.onGameResume();
                                    
                                });
                                                                
                            } catch(err) {

                                alert(err);
                            
                            }

                        }
                        
                    }                 
                }                
            });

        
                    
    });
            


}




// ################################### //
// avvio ricerca giocatori per giocare //
// ################################### //
client_game_quiz_gz.prototype.startMatchMaking = function() {

    var self = this;
    self.isInArMode = false;

    if(!self.HelperService.isNetworkAvailable()) {

        self.ionicPopup.alert({
            title: (self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') +' <i class="ion-android-alert"></i>',
            template: self.LanguageService ? self.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
        });

        return;
    }

    self.gameCenter.getNickname({
        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            self.SocketService.emit('message', 'start_match_make', {}, {     
                key: self.config.key,   
                user: nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                game_data: null
            });

        }
    });

};



// ######################### //
// ricezione invito gi gioco //
// ######################### //
client_game_quiz_gz.prototype.onReceiveGameRequest = function(message) {

    var self = this;

    self.gameCenter.appendRequest({
        level: 'progetto',
        match_id: message.match_id,
        title: self.config.name,
        key: self.config.key,
        time_to_live: self.config.time_to_live,
        classInstance: self,
        onClickFunctionName: 'acceptInvite',
        from: {
            user: message.user,
            uid: message.uid
        }
        
    });

};


// ########################################## //
// accettazione di una sfida previo richiesta //
// ########################################## //
client_game_quiz_gz.prototype.acceptInvite = function(request) {

    var self = this;
    self.isInArMode = false;

    self.gameCenter.getNickname({

        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            self.askGameReq = self.ionicPopup.confirm({
                title: self.LanguageService ? self.LanguageService.getLabelParam("ACCETTI_LA_SFIDA_DA",[request.from.user]) : 'Accetti la sfida da <strong>' + request.from.user + '</strong> ?',
                template: self.LanguageService ? self.LanguageService.getLabelParam("PREMI_SI_PER_GIOCARE_CONTRO",[self.config.name,request.from.user]) : 'Premi "Si" per iniziare a giocare a ' + self.config.name + ' contro ' + request.from.user,
                cancelText: self.LanguageService ? self.LanguageService.getLabel("NO") : 'No', 
                cancelType: 'button-stable',
                okText: self.LanguageService ? self.LanguageService.getLabel("SI") : 'Si', 
                okType: 'button-balanced'
            });

            self.askGameReq.then(function(res) {

                if(res) {
                    
                    self.gameCenter.removeRequest(request);
                    
                    self.SocketService.emit('message', 'accept_match_make', {}, {    
                        match_id: request.match_id, 
                        key: self.config.key,   
                        user: nickname,
                        uid: self.GlobalVariables.deviceUUID,
                        from: request.from,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto
                    });                    

                }  

            });

        }

    });


};



// ############################################################################ //
// il server notifica che dopo un tot tempo la mia richiesta di gioco è scaduta //
// ############################################################################ //
client_game_quiz_gz.prototype.onReceiveGameRequestTimeout = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.ionicPopup.alert({
        title: self.LanguageService ? self.LanguageService.getLabel("NESSUN_UTENTE_ACCETTATO_SFIDA") : 'Nessun utente ha accettato la tua sfida',              
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};  




// ##################################################### //
// un utente ha raccolto la sfida che IO ho lanciato ... //
// ##################################################### //
client_game_quiz_gz.prototype.onReceiveGameRequestInviterAccept = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.gameCenter.closeView();

    self.showClue(message);

}; 




// ######################################################## //
// vengo notificato per aver accettato la sfida di un altro //
// ######################################################## //
client_game_quiz_gz.prototype.onReceiveGameRequestInvitedAccept = function(message) {

    var self = this;
    self.gameCenter.closeView();
    self.showClue(message);

}; 


// ###################################################### //
// abbandono il gioco esplicitamente vince l'altro utente //
// ###################################################### //
client_game_quiz_gz.prototype.doForfaitGame = function() {

    var self = this;
    
    self.SocketService.emit('message', 'forfait_game', {}, {  

        match_id: self.currentGame.match_id,  
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.GlobalVariables.application.currentProgetto.idProgetto

    });
    

    self.assertEnding({
        isWon: false,
        isForfait: true,
        victory_condition: null
    });

    
}; 


// ############################### //
// ricevo una notifica di vittoria //
// ############################### //
client_game_quiz_gz.prototype.onReceiveGameEnding = function(params) {

    var self = this;  

    try {

        // #################################################################################################### //
        // ricevo una notifica di vittoria mentre sono ancora nella schermata di presentazione dell'indizio.... //
        // #################################################################################################### //
        if(self.clue_scope && self.clue_scope.clueWnd) {
            self.hideClue(function() {
        
                self.assertEnding({
                    isWon: params.isWon, 
                    isForfait: params.isForfait,
                    victory_condition: params.victory_condition
                });
            
            });
        }


        // ###################################################### //
        // ricevo una notifica mentre sono nella realtà aumentata //
        // ###################################################### //
        if(self.questions_scope && self.questions_scope.questionsWnd) {

            self.hideQuestions(function() {

                self.assertEnding({
                    isWon: params.isWon, 
                    isForfait: params.isForfait,
                    victory_condition: params.victory_condition
                });

            });            

        }


        // ###################################################################### //
        // ricevo la notifica di vittoria DOPO aver trovato l'immagine e          //  
        // sono in attesa di sapere se ho impiegato meno tempo del mio avversario //
        // ###################################################################### //
        if(self.currentAlertBox) {
             
            self.currentAlertBox.close();
            self.currentAlertBox = null;

            self.assertEnding({
                isWon: params.isWon, 
                isForfait: params.isForfait,
                victory_condition: params.victory_condition
            });
            
        }


    } catch(err) {

        alert(err);
    
    }
    
};



client_game_quiz_gz.prototype.assertEnding = function( params ) {

    var self = this;

    var title = '';
    var template = '';
    var button = '';



    if(params.isWon) {

        title = self.LanguageService ? self.LanguageService.getLabel("COMPLIMENTI_HAI_VINTO") : 'Complimenti: hai vinto';

        if(params.isForfait) {
            
            template = self.LanguageService ? self.LanguageService.getLabelParam("SEMBRA_PROPRIO_CHE_ABBIA_ABBANDONATO_SFIDA",[self.currentGame.user]) : 'Sembra proprio che <strong>' + self.currentGame.user + '</strong> abbia abbandonato la sfida!';

        } else {

            template = (self.LanguageService ? self.LanguageService.getLabel("HAI_BATTUTO") : 'Hai battuto') + ' <strong>' + self.currentGame.user + '</strong>.';

            if(params.victory_condition) {

                template += '<br>';

                if(params.victory_condition.motivation == 'did_not_even_start') {

                    template += self.LanguageService ? self.LanguageService.getLabel("SEI_STATO_TALMENTE_VELOCE") : 'Sei stato talmente veloce che il tuo avversario non ha nemmeno iniziato a rispondere.';    

                } else if(params.victory_condition.motivation == 'more_right_answers') {

                    template += self.LanguageService ? self.LanguageService.getLabelParam("HAI_RISPOSTO_CORRETTAMENTE_A",[params.victory_condition.winner_answers,params.victory_condition.looser_answers]) : 'Hai risposto correttamente a ' + params.victory_condition.winner_answers + 
                        ' domande, il tuo avversario solo a ' + params.victory_condition.looser_answers + '.';    

                } else if(params.victory_condition.motivation == 'less_time') {

                    template += self.LanguageService ? self.LanguageService.getLabelParam("ENTRAMBI_RISPOSTE_SECONDI_IMPIEGATI",[self.currentGame.game_data.right_answers,params.victory_condition.winner_time,params.victory_condition.looser_time]) : 'Avete dato entrambi ' + self.currentGame.game_data.right_answers + ' risposte esatte, ma tu hai impiegato ' + params.victory_condition.winner_time + ' secondi mentre il tuo avversario ' + 
                        params.victory_condition.looser_time + '.';    

                }
                
            }
            
        }

        button = 'button-balanced' ;

    } else {

        title = self.LanguageService ? self.LanguageService.getLabel("PURTROPPO_HAI_PERSO") : 'Purtroppo hai perso';

        if(params.isForfait) {

            template = self.LanguageService ? self.LanguageService.getLabelParam("HAI_CEDUTO_LA_VITTORIA",[self.currentGame.user]) : 'Hai ceduto la vittoria a <strong>' +  self.currentGame.user + '</strong>. La prossima volta andrà meglio!';
        
        } else {

            template = self.LanguageService ? self.LanguageService.getLabelParam("SEI_STATO_SCONFITTO_DA",[self.currentGame.user]) : 'Sei stato sconfitto da <strong>' + self.currentGame.user + '</strong>.';

            if(params.victory_condition) {

                template += '<br>';

                if(params.victory_condition.motivation == 'did_not_even_start') {

                    template += self.LanguageService ? self.LanguageService.getLabel("SEI_STATO_TROPPO_LENTO") : 'Sei stato troppo lento! Il tuo avversario ha completato tutte le risposte.';    

                } else if(params.victory_condition.motivation == 'more_right_answers') {

                    template += self.LanguageService ? self.LanguageService.getLabelParam("IL_TUO_AVVERSARIO_HA_RISPOSTO_CORRETTAMENTE",[params.victory_condition.winner_answers,params.victory_condition.looser_answers]) : 'Il tuo avversario ha risposto correttamente a ' + params.victory_condition.winner_answers + 
                        ' domande, tu solo a ' + params.victory_condition.looser_answers + '.';    

                } else if(params.victory_condition.motivation == 'less_time') {

                    template += self.LanguageService ? self.LanguageService.getLabelParam("ENTRAMBI_RISPOSTE_SECONDI_IMPIEGATI",[self.currentGame.game_data.right_answers,params.victory_condition.looser_time,params.victory_condition.winner_time]) : 'Avete dato entrambi ' + self.currentGame.game_data.right_answers + ' risposte esatte,' +
                        ' ma tu hai impiegato ' + params.victory_condition.looser_time + ' secondi mentre il tuo avversario ' + 
                        params.victory_condition.winner_time + '.';    

                }
                
            }

        }

        button = 'button-assertive' ;

    }

       
    self.ionicPopup.alert({
        title: title,              
        template: template,
        okText: 'Ok', 
        okType: button        
    });

    self.currentGame = null;

}



client_game_quiz_gz.prototype.hideClue = function(onClose) {

    var self = this;

    if(self.clue_scope.clueWnd) {

        clearInterval(self.clue_scope.timer);

        self.clue_scope.clueWnd.remove().then(function() {
            onClose();
        });        

        self.clue_scope.clueWnd = null;
        self.clue_scope.$destroy();
        delete self.clue_scope;

    }
    
}



client_game_quiz_gz.prototype.hideQuestions = function(onClose) {

    var self = this;

    if(self.questions_scope.questionsWnd) {        

        if(self.questions_scope.rollout) {
            clearInterval(self.questions_scope.rollout.timeout);    
            self.questions_scope.rollout.timeout = null;
        }
        

        self.questions_scope.questionsWnd.remove().then(function() {
            onClose();
        });        

        self.questions_scope.questionsWnd = null;
        self.questions_scope.$destroy();
        delete self.questions_scope;

    }

};


client_game_quiz_gz.prototype.startGame = function() {

    var self = this;
    
    self.SocketService.emit('message', 'start_game', {}, {     
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.currentGame.identifier,
        match_id: self.currentGame.match_id
    });


    self.questions_scope = self.rootScope.$new(true);
    self.questions_scope.clueWnd = null;  
    self.questions_scope.timeout_to_close = null;

    self.currentGame.game_data = {
        right_answers: 0
    };


    self.questions_scope.instance = {

        message: null,

        buttons_enabled: true,

        current_question_index: 0,

        buttons_state: {
            A1: 'button-stable',
            A2: 'button-stable',
            A3: 'button-stable',
            A4: 'button-stable'
        },

        question: self.config.values['lista_domande'][ 0 ]

    };
 
    self.questions_scope.forfaitGame = function() {
        self.hideQuestions(function() {
            self.doForfaitGame();
        });
    };

    self.questions_scope.clickOnAnswer = function(selected_btn_code) {

        self.questions_scope.instance.buttons_enabled = false;
        

        if(self.questions_scope.instance.question.answers[selected_btn_code].is_right == true) {
            self.questions_scope.instance.buttons_state[selected_btn_code] = 'button-balanced';
            self.currentGame.game_data.right_answers++;
        } else {
            self.questions_scope.instance.buttons_state[selected_btn_code] = 'button-assertive';
        }

        self.questions_scope.instance.buttons_state[selected_btn_code] = 
            self.questions_scope.instance.question.answers[selected_btn_code].is_right ? 'button-balanced' : 'button-assertive';


        setTimeout(function () {

            self.questions_scope.$apply(function () {
                
                self.questions_scope.instance.buttons_enabled = true;
                
                for(var btnCode in self.questions_scope.instance.buttons_state) {
                    self.questions_scope.instance.buttons_state[btnCode] = 'button-light'
                }


                if((self.questions_scope.instance.current_question_index + 1) == self.config.values['lista_domande'].length) {

                    

                    self.hideQuestions(function() {

                        self.currentAlertBox = self.ionicPopup.show({
                            title: self.LanguageService ? self.LanguageService.getLabel("COMPLIMENTI_HAI_RISPOSTO_TUTTE_DOMANDE") : 'Complimenti hai risposto a tutte le domande!',
                            template:
                                '<div>' + (self.LanguageService ? self.LanguageService.getLabelParam("ATTENDI_PER_SAPERE_SE_HAI_BATTUTO",[self.currentGame.user]) : 'Attendi un istante per sapere se hai battuto <strong>' + self.currentGame.user + '</strong>') + '</div>' +
                                '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'
                        });            

                        // ho trovato tutte le immagini. concludo la partita //
                        self.SocketService.emit('message', 'all_questions_answered', {}, {     
                            key: self.config.key,                   
                            uid: self.GlobalVariables.deviceUUID,
                            identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                            match_id: self.currentGame.match_id,
                            game_data: {
                                right_answers: self.currentGame.game_data.right_answers
                            }
                        });

                    });            
                
                } else {

                    self.questions_scope.instance.current_question_index++;

                    self.questions_scope.instance.question = 
                        self.config.values['lista_domande'][ self.questions_scope.instance.current_question_index ];    
                
                }
                

            });

        }, 500);

    };



    self.questions_scope.questionsWnd = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" class="game-generic-modal" cache-view="false">' +

            '<ion-content scroll="true" padding="false" style="bottom:74px;">' +

                '<div style="position: absolute; top:0px; left:0px; right:0px; height:15em; background-size:cover; background-image:url({{instance.question.background}});">' + 
                    '<div style="position: absolute; top:0px; left:0px; right: 0px; bottom: 0px; background-color: rgba(147,147,147,0.55);">' +
                        '<div ng-if="instance.message" ng-bind-html="instance.message" style="position: absolute; color:white; padding:3px; top:0px; left:0px; right:0px; min-height:2em; height:auto; text-align:center; background: linear-gradient(to bottom, rgba(63,63,63,0.6) 0%,rgba(224,224,224,0) 100%);"></div>' +
                        '<div style="position: absolute; color:white; padding:3px; bottom:0px; left:0px; right:0px; min-height:2em; height:auto; background: linear-gradient(to bottom, rgba(224,224,224,0) 0%,rgba(63,63,63,0.65) 100%);">{{instance.question.text}}</div>' +
                    '</div>' +
                '</div>' +
                
                '<div style="position: absolute; top:15em; left:0px; right:0px; height:auto; padding: 0.2em 0.2em;">' +

                    '<button ng-class="instance.buttons_state.A1" ng-disabled="!instance.buttons_enabled" ng-click="clickOnAnswer(\'A1\');" style="border-top:solid 1px #cdcdcd; border-bottom:solid 1px #cdcdcd;" class="button button-full">{{instance.question.answers.A1.text}}</button>' +
                    '<button ng-class="instance.buttons_state.A2" ng-disabled="!instance.buttons_enabled" ng-click="clickOnAnswer(\'A2\');" style="border-top:solid 1px #cdcdcd; border-bottom:solid 1px #cdcdcd;" class="button button-full">{{instance.question.answers.A2.text}}</button>' +
                    '<button ng-class="instance.buttons_state.A3" ng-disabled="!instance.buttons_enabled" ng-click="clickOnAnswer(\'A3\');" style="border-top:solid 1px #cdcdcd; border-bottom:solid 1px #cdcdcd;" class="button button-full">{{instance.question.answers.A3.text}}</button>' +
                    '<button ng-class="instance.buttons_state.A4" ng-disabled="!instance.buttons_enabled" ng-click="clickOnAnswer(\'A4\');" style="border-top:solid 1px #cdcdcd; border-bottom:solid 1px #cdcdcd;" class="button button-full">{{instance.question.answers.A4.text}}</button>' +

                '</div>' +

            '</ion-content>' +

            '<ion-footer-bar align-title="left" class="game-generic-footer row">' +

                '<div class="col">' +
                    '<button ng-click="forfaitGame()" class="button button-full button-assertive cst-button">' + (self.LanguageService ? self.LanguageService.getLabel("MI_ARRENDO") : 'Mi Arrendo') + '</button>' +
                '</div>' +
                
            '</ion-footer-bar>' +

        '</ion-modal-view>',

        {
            scope: self.questions_scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false,
            backdropClickToClose: false
        }

    );


    self.questions_scope.questionsWnd.show();
  
}


// il momento in cui mostro l'indizio è anche il momento in cui ho instanziato una sfida
// a questo punto il contatore delle partite giocate aumenta
client_game_quiz_gz.prototype.showClue = function(message) {

    var self = this;        
    self.clue_scope = self.rootScope.$new(true);
    self.clue_scope.clueWnd = null;    
    self.currentGame = message;

   

    self.clue_scope.time = 10;
    self.clue_scope.timer = null;

    self.clue_scope.startGame = function() {


        self.hideClue(function() {
            self.startGame();    
        });
        
    };

    self.clue_scope.forfaitGame = function() {
        
        self.hideClue(function() {
            self.doForfaitGame();    
        });
        
    };

    self.clue_scope.clueWnd = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" class="game-generic-modal" cache-view="false">' +

            '<ion-content scroll="true" padding="false" style="bottom:70px;">' +

                '<div class="game-generic-inner">' +
                    
                    '<div><h2>' + self.config.name + '</h2></div>' +
                    '<div><h3>' + self.config.description + '</h3></div>' +

                    '<div><h2>'+(self.LanguageService ? self.LanguageService.getLabel("IL_TUO_AVVERSARIO_E") : 'Il tuo avversario è')+'</h2></div>' +
                    '<div><h3>' + self.currentGame.user + '</h3></div>' +
                    
                    '<div><h2>'+(self.LanguageService ? self.LanguageService.getLabel("UN_PICCOLO_INDIZIO_SULLE_DOMANDE") : 'Un piccolo indizio sulle domande che troverai:')+'</h2></div>' +
                    '<div>' + self.config.values['advice'] + '</div>' +                        

                '</div>' +

            '</ion-content>' +

            '<ion-footer-bar align-title="left" class="game-generic-footer row">' +
                '<div class="col">' +
                    '<button ng-click="forfaitGame()" class="button button-full button-assertive cst-button">' + (self.LanguageService ? self.LanguageService.getLabel("MI_ARRENDO") : 'Mi Arrendo') + '...</button>' +
                '</div>' +
                '<div class="col">' +
                    '<button ng-click="startGame()" class="button button-full button-balanced cst-button">' + (self.LanguageService ? self.LanguageService.getLabel("INIZIO_FRA") : 'Inizio fra') + ' {{time}}</button>' +
                '</div>' +
            '</ion-footer-bar>' +

            '</ion-modal-view>',

        {
            scope: self.clue_scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false,
            backdropClickToClose: false
        }

    );


    self.clue_scope.clueWnd.show().then(function() {

        self.clue_scope.timer = setInterval(function() {
             
            self.clue_scope.$apply(function() {

                self.clue_scope.time--;

                if(self.clue_scope.time == 0) {
                    
                    self.clue_scope.startGame();
                }

            });

        }, 1000);

    });

}



// ################################################################################## //
// messaggio ricevuto quando provo ad accettare una sfida che nel frattempo è scaduta //
// ################################################################################## //
client_game_quiz_gz.prototype.onReceiveGameRequestExpiredOrCancelled = function(message) {

    var self = this;

    self.ionicPopup.alert({
        title: self.LanguageService ? self.LanguageService.getLabel("TROPPO_TARDI") : 'Troppo Tardi',              
        template: self.LanguageService ? self.LanguageService.getLabelParam("LA_SFIDA_NON_E_PIU_DISPONIBILE",[message.from.user,self.config.name]) : 'La sfida lanciata da "' + message.from.user + '" al gioco "' + self.config.name + '" non è più disponibile...' ,
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};


client_game_quiz_gz.prototype.onReceiveGameRequestAck = function(message) {

    var self = this;

    if(message.status == 'OK') {



        self.waitBox = self.ionicPopup.alert({
          title: self.LanguageService ? self.LanguageService.getLabel("ATTENDI_UTENTE_ACCETTI_SFIDA") : 'Attendi che un utente accetti la sfida',
          template: 
            '<div>'+(self.LanguageService ? self.LanguageService.getLabel("PREMERE_ANNULLA_PER_INTERROMPERE") : 'Puoi premere su \'Annulla\' per interrompere la tua richiesta di gioco')+'<div>' + 
            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>',
          okText: self.LanguageService ? self.LanguageService.getLabel("ANNULLA") : 'Annulla', 
          okType: 'button-assertive'            
        });

        self.hasPendingRequest = true;

        self.waitBox.then(function() {

            self.waitBox = null;
          
            if(self.hasPendingRequest == true) {

                self.hasPendingRequest = false;

                self.SocketService.emit('message', 'cancel_match_make', {}, {     
                    key: message.key,   
                    user: self.rootScope.game_center.nickname,
                    uid: self.GlobalVariables.deviceUUID,
                    identifier: message.identifier
                });
          
            }
          
        });

    } else if(message.status == 'KO') {

        //traduzione messaggio
        var messaggio=message.reason;
        if (message.reason=="Non ci sono utenti disponibili in questo momento")
        {
            messaggio = self.LanguageService ? self.LanguageService.getLabel("NO_USERS_NOW") : message.reason;
        }
        

        self.waitBox = self.ionicPopup.alert({
            title: self.LanguageService ? self.LanguageService.getLabel("RICHIESTA_GIOCO_RESPINTA") : 'La tua richiesta di gioco è stata respinta',
            template: messaggio,
            okText: self.LanguageService ? self.LanguageService.getLabel("CHIUDI") : 'Chiudi'          
        });

    }

}



client_game_quiz_gz.prototype.onGamePause = function(message) {
 
    var self = this;

    try {

        if(self.waitBox) {

            self.waitBox.close();

            self.SocketService.emit('message', 'cancel_match_make', {}, {     
                key: self.config.key,   
                user: self.rootScope.game_center.nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto
            });

        }

        if((self.clue_scope && self.clue_scope.clueWnd) || (self.questions_scope && self.questions_scope.questionsWnd)) {

            self.SocketService.emit('message', 'forfait_game', {}, {  

                match_id: self.currentGame.match_id,  
                key: self.config.key,   
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto

            });


        }


    } catch(err) {

        alert(err);

    }   
    


}



client_game_quiz_gz.prototype.onGameResume = function(message) {
    
    var self = this;

    try {

        if(self.clue_scope && self.clue_scope.clueWnd) {
            
            self.hideClue(function() { 
                self.assertEnding({
                    isWon: false, 
                    isForfait: true,
                    victory_condition: null
                });
            });

        }


        if(self.questions_scope && self.questions_scope.questionsWnd) {
            
            self.hideQuestions(function() { 
                self.assertEnding({
                    isWon: false, 
                    isForfait: true,
                    victory_condition: null
                });
            });

        }


    } catch(err) {

        alert(err);
    
    }
 
}



client_game_quiz_gz.prototype.onOpponentHasAnsweredAllQuestions = function(message) {

    console.log("onOpponentHasAnsweredAllQuestions");

    var self = this;

    if(self.questions_scope && self.questions_scope.questionsWnd) {
        
        self.questions_scope.rollout = {
            timeout: null,
            seconds: 10
        };

        self.questions_scope.$apply(function () {            
            self.questions_scope.instance.message = "<strong>" + self.currentGame.user + "</strong> " + (self.LanguageService ? self.LanguageService.getLabel("HA_COMPLETATO_TUTTE_LE_DOMANDE") : "ha completato tutte le domande...");                        
        });

        self.questions_scope.rollout.timeout = setInterval(function() {

            if(self.questions_scope.rollout.seconds == 0) {
                            
                self.hideQuestions(function() {

                    self.currentAlertBox = self.ionicPopup.show({
                        title: self.LanguageService ? self.LanguageService.getLabel("TEMPO_SCADUTO") : 'Tempo Scaduto!',
                        template:
                            '<div>' + (self.LanguageService ? self.LanguageService.getLabelParam("ATTENDI_PER_SAPERE_SE_HAI_BATTUTO",[self.currentGame.user]) : 'Attendi un istante per sapere se hai battuto <strong>' + self.currentGame.user + '</strong>') + '</div>' +
                            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'
                    });            

                    self.SocketService.emit('message', 'all_questions_answered', {}, {     
                        key: self.config.key,                   
                        uid: self.GlobalVariables.deviceUUID,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                        match_id: self.currentGame.match_id,
                        game_data: {
                            right_answers: self.currentGame.game_data.right_answers
                        }
                    });

                });  


            } else {

                self.questions_scope.$apply(function () {            
                    self.questions_scope.instance.message = (self.LanguageService ? self.LanguageService.getLabelParam("TI_RIMANGONO_SECONDI_PER_IL_QUIZ",[self.questions_scope.rollout.seconds]) : "Ti Rimangono " + self.questions_scope.rollout.seconds + " secondi per completare il quiz");                        
                    self.questions_scope.rollout.seconds--;
                });
            }


        }, 1000);


    }

    
};






