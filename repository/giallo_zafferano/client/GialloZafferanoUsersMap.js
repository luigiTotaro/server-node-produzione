var GialloZafferanoUsersMap = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.LanguageService = null;
    this.scope = null;


    var viola = '#7c6da4';
    var arancio = '#ff9513';

    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 

    '.div-users-now {' +
    '   position: absolute;' + 
    '   top: 5px;' +
    '   left: 5px;' +
    '   height: auto !important;' +
    '   z-index: 99999;' +
    '   padding: 6px 8px !important;'+    
    '   background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAA1CAYAAAADOrgJAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABgBJREFUeNrcmntQVFUcx3/nnl1gYdcFlocuILj4YGF5NxXgjJZkOk2jk698pOmgxZhl1kz/1DTmH5XNaNM0SUVSYs5oVGaUf0RpjczQJA+XDeQRoAuusDxkl8fusuec/tgV2GGBfbFyPf+de/eecz/ne37nfH9nLzIVpYK/C0567ks20puDgkLVpPnSDl+0KfA3BKfIryQ3f8i1V1O4uLwsqq1M8rpdvyqRdSCRtlXkTr5GtZUrcMbeNbwCAUYDnV6nVOJt08jfMcLF5d2k2soV4/XoDD3troua14rgrAOJUwZfW5nERafrAQCQdLHZFxBzBoIzCzJQhHKAqEtbkURuwSnb3nH4QUhkCwAAEi/S+UzpuQChXVW/s97GULCOAjPeEZJ/z7+HMwtUk6PFRhww4tbLLl1fjlU7XvUbCOttDJ9y0WLcNBGZ2Gxb/IP6XVI4ectHSBJjpq2Xn2F9TW/5DQSFLxt0qItkBAIkP06QMgEAABIE6WaZoo9xi7K6SMN3bzJjVwAAANVr5Di7MNQvIFxszjokXWweBwldcpfUFqsnacbZp9bdaSGUm0+Q+tIqqquRO9ywmgGGdCe8AsGZBUk4/cV1OLtwxudIXUkVG7wdxCnyK+2jGIOzDiybpAi29S7scjoQCU9cJ41lr4PV7DwGe+q3eQXCjHeOkxtfX6aNZRYUperlFPnXsGrH4WmDvq1iJQqOJGA1AbvXVjJ1ZAJvO1SzC8UoMnmAdlzJnvE9Bv4Lxml7Nno+tahVAgDARvSY9WhktK0ij2jOnUQSuYVLWF2N0/ZsnzJS8as+AQCgnVUT1gQLhmw3ufZJEBG09Rc90zeEujSoA63vew5CzGHTKCWkHVeziPqbc1x0mh4nbz02/khj2REIlDKwDCGcvOUDuyUJsK9e7XaIcNpS3skGbwe5PKa66iScXSjwDMRqXjBrB93qCNJw4W0UperFqbv2AQDgxKeP2e7dOGhHxwAApLa4GwCAtlfcYgZtoHvvYgIwDbzhIciIy+aO9WhkpP7sV1zCk3+ThgvvoihVH+trFttXqz4IEDN7YP/D+lvEHu1X/a2HPAt2qznQ3c5oxx+PcouydFyo4jfb5rb1KOKEg1z4sh6cunM/7bjyiMcOQlcdg7P2x3sSIwLPOqxZSJovPW9bOtWvASNBbLgnjDSXf+6lhwBm6PzM/QzRahJ6b1+apEwYvPL+Tu11e7qatR7EiMkHToAB1dXIAfnGVLDhbgFOe2GvgFu6vhzMRgVYjBHMMhQMVpMQqMXWCxIwEIqsSBgyDELREL1b5ztLw6jvmjJoX0GmolQGPC8oLHGE46IzevgOwoZ7RBySxl3jOwhQC+JAJDvNexCEGTIVpQISyaxstA/zFkQYbEtwUHhiF99FsYGI5VcfChAQhZ16KEBIXUkVksjH+Lv+0gmfgMISW3i8bLEJEPHCX3mLESwbm/BOAZKfeQsiie3iJo5wTv+FRDLKSxDp4vMObhZFJLXyjkIgYiCSHXUEkcbzbhnmYnOuk+pTjokS0Zz7mG/TC4Up9jnNELm43Iu8USMur5nUFmucp7qS2O33j2rmvRoRymcdLcrk6VV9yoKX5J+Z92osya8itcXN41DT/RmKZMuN4wdq802JBbEWZugMnGoanRHHr04DHDg/1VCsXePc/ToppOaLdpyydd98g8ArNp4hdaevuQwCAEDUpSVYuenTeaNEzOPtpOninunzkZlgGr8/hJWbTz74FSppkHZVKWZOrGaFKTuCU3ftRsGR5MF4qXgTF/9E+OwZoisw9WdLOeVmOZewus7PO/cop3gqgVSfmtFxePQtClbtfJnqrh9nfU2SOZ5OBtZ7U+p6zu5mIZpvi1hf0wKcvOVDFKG8NyeBHZ2udxXCY0WmKJS68yU2qD3I9JoUNtrv9UE3F79KTW/9me6Wer78zAlnF4rBNHCYDXdvYAatkt3rCAFqdXOf2FBKmn7a7fY0nMvvtXBmgQrGhteB2ZjLzIblYDZEMYsxBKwmAVhNmNExDsiYPV2Vj3CxOYdJ/dliT/r6fwAvjFqI0Uz3hAAAAABJRU5ErkJggg==);' +
    '   background-repeat: no-repeat;'+
    '   background-position: 102% 5%;'+
    '   background-size: 30%;'+
    '   max-width: 85%;'+
    '}' +

    '.gz-popup {' +
    '   width: 120px;' + 
    '   margin-left:-60px;' +
    '   height: 51px;' +
    '   margin-top: -50px;' + 
    '   white-space: nowrap;' +    
    '}' +    

    '.gz-popup-body {' +
    '   margin: 0 auto;' + 
    '   display: table;' + 
    '}' +

    '.gz-popup-inner {' +
    '   color: white;' + 
    '   border:solid 2px ' + arancio + ';' +
    '   font-size: 12px;' +
    '   font-family: sans-serif;' +
    '   height: 35px;' +
    '   background-color:' + viola + ';' +
    '   text-align:center;' +
    '   border-radius: 5px;' + 
    '   padding: 2px 4px;' + 
    '   line-height: 13px;' + 
    '}'  +

    '.gz-arrow-ctr {' +
    '   height: 15px;' + 
    '}'  +

    '.gz-arrow-ctr-1 {' +
    '   width: 0;' + 
    '   height: 0;' + 
    '   border-style: solid;' + 
    '   border-width: 13.0px 7.5px 0 7.5px;' + 
    '   border-color: ' + arancio + ' transparent transparent transparent;'+
    '   margin:0 auto;' + 
    '}' +

    '.gz-arrow-ctr-2 {' +
    '   width: 0;' + 
    '   height: 0;' + 
    '   border-style: solid;' + 
    '   border-width: 12px 6px 0 6.5px;' + 
    '   border-color: ' + viola + ' transparent transparent transparent;' + 
    '   margin:0 auto;' + 
    '   margin-top: -15px;' + 
    '   margin-left: -6px;' + 
    '}'
    ;


    document.getElementsByTagName('head')[0].appendChild(style);

 

}


L.HtmlIcon = L.Icon.extend({
    options: {
        /*
        html: (String) (required)
        iconAnchor: (Point)
        popupAnchor: (Point)
        */
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    createIcon: function () {
        var div = document.createElement('div');
        div.innerHTML = this.options.html;
        return div;
    },

    createShadow: function () {
        return null;
    }
});


GialloZafferanoUsersMap.Instance = null;

GialloZafferanoUsersMap.Initialize = function(params) {

    if(GialloZafferanoUsersMap.Instance != null) {
      return;
    }

    GialloZafferanoUsersMap.Instance = new GialloZafferanoUsersMap();
    GialloZafferanoUsersMap.Instance.ionicModal = params.ionicModal;
    GialloZafferanoUsersMap.Instance.ionicPopup = params.ionicPopup;
    GialloZafferanoUsersMap.Instance.HelperService = params.HelperService;
    GialloZafferanoUsersMap.Instance.http = params.http;
    GialloZafferanoUsersMap.Instance.GlobalVariables = params.GlobalVariables;
    GialloZafferanoUsersMap.Instance.LoggerService = params.LoggerService;
    GialloZafferanoUsersMap.Instance.SocketService = params.SocketService;
    GialloZafferanoUsersMap.Instance.LanguageService = params.LanguageService;


    // rimuovo la voce di menu dalla mappa di giallo_zafferano quando entro nella homepage //
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeGialloZafferanoKey', function() {
        delete GialloZafferanoUsersMap.Instance;
        GialloZafferanoUsersMap.Instance = null;
        params.rootScope.removeItemFromMenu('giallo_zafferano_map_key');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'removeGialloZafferanoKey', null);
    });

    params.rootScope.addItemToMenu('giallo_zafferano_map_key', params.LanguageService ? params.LanguageService.getLabel("MAPPA_DEGLI_UTENTI") : 'Mappa degli utenti', 'ion-ios-location', function() {

        if(!params.HelperService.isNetworkAvailable()) {

            var alert = GialloZafferanoUsersMap.Instance.ionicPopup.alert({
             title: (params.LanguageService ? params.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') + ' <i class="ion-android-alert"></i>',
             template: params.LanguageService ? params.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Non sei attualmente connesso ad Internet'
            });


        } else {

            params.rootScope.closeMainMenu();
            // creazione di uno scope solo per questa classe.
            // distruggere questo scope quando si esce
            GialloZafferanoUsersMap.Instance.scope = params.rootScope.$new(true);
            GialloZafferanoUsersMap.Instance.run();

        }


       
        

    });


}



GialloZafferanoUsersMap.prototype.getData = function(params) {

    var self = this;


    self.http({
        url: self.GlobalVariables.application.applicationUrl + '/getUsersByRegion',
        responseType: 'json',
        method: 'GET',
        timeout: 10000
    }).then(function(json) {
        params.onComplete(null, json.data);
    }, function(error, xhr) {
        params.onComplete(self.LanguageService ? self.LanguageService.getLabel("VERIFICATO_ERRORE_CONNESSIONE") : 'Si è verificato un errore di connessione Internet', null);
    });

}


GialloZafferanoUsersMap.prototype.run = function() {

  	var self = this;

    self.scope.destroyView = function() {
        self.scope.mappa.remove();
        self.mapView.remove();
        self.scope.$destroy();
        delete self.scope;
        
    };

    self.SocketService.emit('message','has_opened_map', {}, {
        uid: self.GlobalVariables.deviceUUID
    });



    self.mapView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" id="appFirstWelcomeScreen" style="background-color:rgba(0,0,0,0.5);">' +
            '<ion-content id="messageScrollableArea" scroll="false" padding="false">' +
                '<a ng-click="destroyView()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +
                '<div id="mapDiv" style="position:absolute; top:6px; left:5px; right:5px; bottom:6px;"></div>' +
            '</ion-content>' +
        '</ion-modal-view>',

        {
            scope: self.scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false
        }

    );


    self.mapView.show().then(function() {

        self.scope.mappa = L.map('mapDiv',{
          zoomControl:false
        }).setView([42.508899, 12.830778], 5);

        L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYWtpcmFwaXgiLCJhIjoiRjRaNDFNNCJ9.w19L-nOBP5GGemvdlb0n7w', {
            attribution: '',
            maxZoom: 7,
            minZoom: 4
        }).addTo( self.scope.mappa );

        self.getData({
            onComplete: function(err, json) {

                if(err) {

                    var alertErr = self.ionicPopup.alert({
                      title: self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione',
                      subTitle: self.LanguageService ? self.LanguageService.getLabel("RIPROVARE_PIU_TARDI") : 'Riprovare più tardi',
                      template: err
                    });

                    alertErr.then(function(res) {
                    	self.scope.destroyView();
                    });

                    return;
                    

                } else {

                	var viola = '#7c6da4';
                	var arancio = '#ff9513';
                	var markerArray = [];

                    var utenteConTeLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTE_CON_TE_SU") : 'utente è ora con te su';
                    var utentiConTeLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTI_CON_TE_SU") : 'utenti sono ora con te su';
                    var utenteLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTE") : 'Utente';
                    var utentiLabel= self.LanguageService ? self.LanguageService.getLabel("UTENTI") : 'Utenti';

                    var divUsersNow = document.createElement('div');
                    divUsersNow.className = 'gz-popup-inner div-users-now';
                    divUsersNow.appendChild(document.createTextNode( json.total + " " + (json.total == 1 ? utenteConTeLabel : utentiConTeLabel) + " Giallo Zafferano" ));
                    document.getElementById('mapDiv').appendChild(divUsersNow);

                    setTimeout(function() {

    
                        _.each(json.region_list, function(item) {

                             var helloLondonHtmlIcon = new L.HtmlIcon({
                                html: 
                                	'<div class="gz-popup">' + 
                                		'<div class="gz-popup-body">' +
                                			'<div class="gz-popup-inner">' +
                                				item.totale + " " + (item.totale == 1 ? utenteLabel : utentiLabel) + " in<br>" + item.name + 
                                			'</div>' +
                                			'<div class="gz-arrow-ctr">' +
                                				'<div class="gz-arrow-ctr-1">'+
                                					'<div class="gz-arrow-ctr-2"></div>' +
                                				'</div>' +
                                			'</div>' +
                                			
                                		'</div>' +
                                	'</div>'
                            });

                             var marker = new L.Marker(new L.LatLng(item.lat, item.lon), {icon: helloLondonHtmlIcon});

                             self.scope.mappa.addLayer(marker);

                             markerArray.push(marker);
                
                        });

                        if(markerArray.length > 0) {
                            var group = L.featureGroup(markerArray);
                            self.scope.mappa.fitBounds(group.getBounds());    
                        }
                        

                    }, 500);

                    


                }

            }

        });

    });

}
