var Config = require('./config');
var RegionsDatabase = require('./regionCoords');
var DatabaseManager = require('./databaseManager');
var fs = require('fs');
var Path = require('path');
var _ = require('underscore');
var winston = require('winston');

var gestore_gps_chi = function(parameters) {

    var self = this;

    self.regions = {
        creation: 0,
        data: {}
    };

    // ############################################################### //
    // sezione dedicata alla validazione dei parametri del costruttore //
    // ############################################################### //
    if(!parameters.socket_namespace) {
      throw "Parametro 'parameters.socket_namespace' obbligatorio.";
    }

    if(!parameters.application_name) {
      throw "Parametro 'parameters.application_name' obbligatorio.";
    }

    if(!parameters.express_instance) {
      throw "Parametro 'parameters.express_instance' obbligatorio.";
    }


    self.socket_namespace = parameters.socket_namespace;
    self.application_name = parameters.application_name;
    self.express_instance = parameters.express_instance;


    if(!self.socket_namespace.additional_callbacks) {
      self.socket_namespace.additional_callbacks = {};
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT] = [];
    }

    // ############################################################################ //
    // una classe può aggiungere arbitrariamente degli endpoint all'istanza express //
    // ############################################################################ //
    self.express_instance.get('/' + self.application_name + '/setposition/', function(req, res) {

      DatabaseManager.getData('testDatabase', function(err, data) {
          res.send(data);
      });

    });

    // persisto su db il risultato dei dati estratti. in caso di caduta del server
    // posso usare questi dati
    self.persistUsersByRegion = function(onComplete) {

        DatabaseManager.getTable(self.application_name, 'commons', function(err, table) {

            if(err) {

                onComplete(err);

            } else {

              table.findOneAndUpdate({
                  // find section
                  common_key: 'users_by_region'
                }
                , {
                  // modify section
                  $set: self.regions

                },

                // options section
                {
                  upsert: true
                },

                // callback
                function(err, result) {

                  onComplete( err ? err : null );

              });

            }

        });

    }


    self.getCachedUsersByRegion = function(onComplete) {

        DatabaseManager.getTable(self.application_name, 'commons', function(err, table) {

            if(err) {

                onComplete(err);

            } else {

                table.findOne({
                  // find section
                  common_key: 'users_by_region'
                },{

                }, function(err, result) {

                    if(err) {
                        onComplete(err,null);
                    } else {
                        onComplete(null, result);
                    }


                });

            }

        });
    }

    // ############################################################## //
    // restituisce un json contenente il numero di utenti per regione //
    // ############################################################## //
    self.express_instance.get('/' + self.application_name + '/getUsersByRegion', function(req, res) {

        var NOW = (new Date()).getTime();

        if((NOW - self.regions.creation) > 5000) {

            self.regions.creation = NOW;

            self.regions.data = {
                total: Object.keys(self.socket_namespace.sockets).length,
                region_list: []
            };

            var tmp = [];

            _.each(self.socket_namespace.sockets, function(currentClientConnection) {

                if(currentClientConnection.applicationData.position && currentClientConnection.applicationData.position.regione) {

                    if(!tmp[ currentClientConnection.applicationData.position.regione.name ]) {

                        tmp[ currentClientConnection.applicationData.position.regione.name ] = {
                            totale: 0,
                            name: currentClientConnection.applicationData.position.regione.name,
                            lat: currentClientConnection.applicationData.position.regione.lat,
                            lon: currentClientConnection.applicationData.position.regione.lon
                        }

                    }

                    tmp[ currentClientConnection.applicationData.position.regione.name ].totale++;
                }

            });


            for(var regionName in tmp) {
              self.regions.data.region_list.push(tmp[regionName]);
            }


            self.persistUsersByRegion(function(err) {
                if(err) {
                    winston.error('errore persistenza dati regione per ' + self.application_name + ": " + err);
                }

            });

        }

        res.send( self.regions.data );

    });



    // ######################################################################### //
    // una classe espone ed aggiunge callback che possono agire su eventi socket //
    // ######################################################################### //
    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE].push(function(clientConnection, message) {

        if(message.type == 'has_opened_map') {

            DatabaseManager.getTable(application_name, 'event', function(err, table) {

                if(!err) {

                    var _data = message.msg_body;
                    _data.type = 'has_opened_map';
                    _data.time = (new Date()).getTime();

                    table.insertOne(_data);

                }

            });

        }

    });


    self.getCachedUsersByRegion(function(err, regionData) {

        var NOW = (new Date()).getTime();

        if(err) {

            winston.error('errore recupero dati regione per ' + self.application_name + ": " + err);

        } else {

            if(regionData) {

                // età dei dati espressa in secondi
                var data_age = (NOW - regionData.creation) / 1000;

                if(data_age <= 60) {
                  // se i dati salvati nel db sono più vecchi di 60 secondi allora
                  // non posso ritenerli affidabili. se sono sotto questo range allora
                  // vengono usati e lasciati validi per 10 secondi + 5 secondi

                  self.regions.creation = NOW + (1000 * 10);
                  self.regions.data = regionData.data;

                }

            }

        }

    });

};
