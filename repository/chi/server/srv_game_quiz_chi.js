var Config = require('./config');
var DatabaseManager = require('./databaseManager');
var fs = require('fs');
var Path = require('path');
var _ = require('underscore');
var winston = require('winston');
var mongo = require('mongodb');
var Bot = require('./bot.js');

var srv_game_quiz_chi = function(parameters) {

    var self = this;
 
    self.this_game_request_queue = [];
    self.this_game_running_games = [];

    self.config = {
        
        name: 'Rispondi a tutte le domande',
        description: 'Rispondi a tutte le domande. Vince chi risponde a tutte le domande.',
        key: 'mediasoft.quiz.chi',
        time_to_live: 60000,

        parameters: {
            background: {
                type: 'img',
                description: 'Immagine di sfondo usata nella lista dei giochi'
            },

            advice: {
                type: 'text',
                description: 'Consiglio sugli articoli da conoscere per rispondere alle domande'
            },
            
            lista_domande: {
                type: 'questions',
                description: 'lista delle domande con relative risposte'
            }
        }

    };


    // ############################################################### //
    // sezione dedicata alla validazione dei parametri del costruttore //
    // ############################################################### //
    if(!parameters.socket_namespace) {
      throw "Parametro 'parameters.socket_namespace' obbligatorio here.";
    }

    if(!parameters.application_name) {
      throw "Parametro 'parameters.application_name' obbligatorio.";
    }

    if(!parameters.express_instance) {
      throw "Parametro 'parameters.express_instance' obbligatorio.";
    }


    self.socket_namespace = parameters.socket_namespace;
    self.application_name = parameters.application_name;
    self.express_instance = parameters.express_instance;

    

    if(!self.socket_namespace.additional_callbacks) {
      self.socket_namespace.additional_callbacks = {};
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT] = [];
    }
    

    self.express_instance.get('/' + self.application_name + '/quiz/request_queue', function(req, res) {
        res.status(200).send({
            count: Object.keys(self.this_game_request_queue).length
        });

    });

    self.express_instance.get('/' + self.application_name + '/quiz/running_queue', function(req, res) {
        res.status(200).send({ 
            count:Object.keys(self.this_game_running_games).length 
        });
    });

    // ########################################### //
    // auto plug del descrittore di configurazione //
    // ########################################### //
    if(!global.configurables[parameters.application_name]) {
        global.configurables[parameters.application_name] = {};
    }
    
    if(!global.configurables[parameters.application_name]['progetto']) {
        global.configurables[parameters.application_name]['progetto'] = [];
    }
    
    global.configurables[parameters.application_name]['progetto'].push(self.config);


    // ######################################################################### //
    // una classe espone ed aggiunge callback che possono agire su eventi socket //
    // ######################################################################### //
    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE].push(function(clientConnection, message) {

        if(message.msg_body.key && message.msg_body.key == self.config.key) {

            if(message.type == 'start_match_make') {

                // ############################################################################################################### //
                // ricezione di un messaggio che indica la richiesta di avvio gioco e conseguente ricerca di giocatori interessati //
                // ############################################################################################################### //
                self.processMatchMakingRequest(clientConnection, message.msg_body);
                
            } else if(message.type == 'cancel_match_make') {

                // ########################################################################### //
                // ricezione di un messaggio che indica la cancellazione di richiesta di gioco //
                // ########################################################################### //
                self.cancelMatchMakingRequest(message.msg_body.uid, false);
                
            } else if(message.type == 'accept_match_make') {

                // ###################################################################################### //
                // ricezione di un messaggio che indica che un utente ha accettato una richiesta di gioco //
                // ###################################################################################### //
                self.acceptMatchMake(clientConnection, message);

                
            } else if(message.type == 'forfait_game') {

                // ############################################################################################ //
                // ricezione di un messaggio che indica che un giocatore ha abbandonato una sfida già accettata //
                // ############################################################################################ //
                self.manageGameForfait(clientConnection, message);
                
            } else if(message.type == 'start_game') {

                // ######################################################################################################################## //
                // ricezione di un messaggio che indica che un utente che ha accettato una sfida ha avviato la realtà aumentata per giocare //
                // ######################################################################################################################## //
                self.userStartedGame(clientConnection, message);
                
            } else if(message.type == 'all_questions_answered') {

                self.allQuestionsAnswered(clientConnection, message);
                
            } else if(message.type == 'user_update_game_progress') {

                self.userHasMadeGameProgress(clientConnection, message);
                
            }
        
        }

    });

 



    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT].push(function(clientConnection) {

        //self.cancelMatchMakingRequest(clientConnection.applicationData.device_info.uid, false);
        try {
            self.cancelMatchMakingRequest(clientConnection.applicationData.device_info.uid, false);

        } catch(err) {

            console.log("cancelMatchMakingRequest catch error");

        }
    });


    

    setInterval(function() {
        
        var NOW = (new Date()).getTime();
        
        for(var uid in self.this_game_request_queue) {

            var time_to_live = NOW - self.this_game_request_queue[uid].start_time;

            if(time_to_live > self.config.time_to_live) {
                self.cancelMatchMakingRequest(uid, true);
            }

        };

        for(var match_id in self.this_game_running_games) {

            var time_to_live = NOW - self.this_game_running_games[match_id].start_time;

            if(time_to_live > (self.config.time_to_live * 5)) {                
                delete self.this_game_running_games[match_id];
            }

        };

    }, 30000);




    // #################################################################### //
    // metodo eseguito quando il server riceve una richiesta di avvio gioco //
    // #################################################################### //
    self.processMatchMakingRequest = function(clientConnection, msg) {
        
        var gameMatchResponse = {
            status: 'OK',             
            uid: msg.uid,
            key: self.config.key,
            user: msg.user,
            identifier: msg.identifier                 
        }

        try {

            console.log("Matchmaking request di QUIZ");

            var ROOM_NAME = 'room_progetto_' + msg.identifier;
            var clients = self.socket_namespace.adapter.rooms[ROOM_NAME].sockets;
            var users_in_room = Object.keys(clients).length;
            var startBotTime=15; //il bot deve partire dopo 15 secondi, in modo da accettare una richiesta che altri non hanno accettato

            if(users_in_room > 5000) {
                gameMatchResponse.status = 'KO';
                gameMatchResponse.reason = 'Servers Too Busy :(';
            }

            if(users_in_room == 1) {
                //instanzio il bot
                startBotTime=5; //parte dopo 5 secondi

                /*
                gameMatchResponse.status = 'KO';
                gameMatchResponse.reason = 'Non ci sono utenti disponibili in questo momento';
                */
                
            }

            clientConnection.emit('server_message', {
                type: 'game_invite_ack',
                msg_body: gameMatchResponse
            });

            if(gameMatchResponse.status == 'KO') {
                return;
            } 

            // identificativo della partita
            var MATCH_ID = (new Date()).getTime() + "_" + msg.uid + "_" + msg.identifier;
            msg.match_id = MATCH_ID;

            self.this_game_request_queue[msg.uid] = {
                match_id: MATCH_ID,
                socket_id: clientConnection.id,
                user: msg.user,
                uid: msg.uid,
                identifier: msg.identifier,
                start_time: (new Date()).getTime(),
                game_data: msg.game_data
            }

                
            DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

                if(!err) {

                    table.insertOne({
                        identifier: msg.identifier,
                        game_title: self.config.name,
                        key: self.config.key,
                        match_id: MATCH_ID,
                        type: 'play_game',
                        status: 'request',
                        request_time: (new Date()).getTime(),
                        accept_time: 0,
                        duration: 0
                    }, function(err, record) {

                        self.this_game_request_queue[msg.uid].recordId = record.insertedId;

                    });

                }

            });
           

            /*
            console.log("Users in Room: " + users_in_room);
            
            for(var connectionId in clients) {
               console.log( self.socket_namespace.sockets[connectionId].applicationData    );
            }
            */

            if (startBotTime!=0)
            {
                var parametro = new Object();
                parametro.uid = msg.uid;
                parametro.match_id = msg.match_id;
                parametro.socket_id = clientConnection.id;
                parametro.user = msg.user;
                parametro.identifier = msg.identifier;
                parametro.game_data = msg.game_data;
                parametro.startIn = startBotTime;
                parametro.classGame = self;
                parametro.application_name = self.application_name;
                parametro.key=self.config.key;
                var mybot = new Bot(parametro);
                mybot.startBot();

            }
                    
            // invio a tutti i presenti in una room
            self.socket_namespace.to(ROOM_NAME).emit('server_message', {
                type: 'game_invite',
                msg_body: msg
            });

        } catch(err) {

            gameMatchResponse.status = 'KO';
            gameMatchResponse.reason = 'Errore inatteso...';

            clientConnection.emit('server_message', {
                type: 'game_invite_ack',
                msg_body: gameMatchResponse
            });

        }

    };




    self.cancelMatchMakingRequest = function(uid, notify) {

        try {

            if(notify == true) {

                var socket_id = self.this_game_request_queue[uid].socket_id;

                self.socket_namespace.to(socket_id).emit('server_message', {
                    type: 'game_invite_timeout',
                    msg_body: {
                        key: self.config.key,
                        uid: uid
                    }
                });

            }

            delete self.this_game_request_queue[uid];

        } catch(err) {

            winston.error("cancelMatchMakingRequest/" + self.config.key + ": " , err);

        }

    }




    // un utente ha accettato una notifica di sfida.
    // questo metodo deve produrre uno di due possibili esiti
    // 1) la richiesta di match_id è ancora valida -> il gioco comincia
    // 2) la richiesta è scaduta -> notifico l'utente che il match non può avvenire
    self.acceptMatchMake = function(clientConnection, msg) {
        
        //console.log("acceptMatchMake");
        var isBot = clientConnection instanceof Bot;

        try {

            if( self.this_game_request_queue[ msg.msg_body.from.uid ] && self.this_game_request_queue[ msg.msg_body.from.uid ].match_id == msg.msg_body.match_id) {
                
                var match = self.this_game_request_queue[ msg.msg_body.from.uid ];

                // messaggio al client che ha LANCIATO la sfida un messaggio che indica
                // che un utente ha accettato la sua sfida. allego i dati di chi ha accettato la sfida
                self.socket_namespace.to(match.socket_id).emit('server_message', {
                    type: 'game_invite_inviter_accept',
                    msg_body: {
                        key: self.config.key,
                        match_id: match.match_id,
                        identifier: match.identifier,
                        uid: msg.msg_body.uid,
                        user: msg.msg_body.user,
                        game_data: match.game_data
                    }
                });

                 
                // messaggio al client che ha ACCETTATO la sfida lanciata da un altro utente
                // allego i dati dell'utente che ha CREATO la sfida             
                if (!isBot)
                {
                    clientConnection.emit('server_message', {
                        type: 'game_invite_invited_accept',
                        msg_body: {
                            key: self.config.key,
                            match_id: match.match_id,
                            identifier: match.identifier,
                            uid: match.uid,
                            user: match.user,
                            game_data: match.game_data                    
                        }
                    });
                }
                else
                {
                    //console.log("match:" + JSON.stringify(match));
                    clientConnection.inviteResult(true);
                }

                // ################################################# //
                // la richiesta di gioco diventa una istanza attiva; //
                // ################################################# //            
                self.this_game_running_games[match.match_id] = {

                    start_time: (new Date()).getTime(),
                    identifier: match.identifier,
                    game_data: match.game_data,
                    match_id: match.match_id,
                    recordId: match.recordId,

                    // utente che ha creato 
                    inviter: {
                        uid: match.uid,
                        user: match.user, 
                        socket_id: match.socket_id,
                        status: 'waiting_to_start',
                        game_start: null,
                        game_end: null
                    },

                    invited: {
                        uid: msg.msg_body.uid,
                        user: msg.msg_body.user,
                        socket_id: isBot ? clientConnection : clientConnection.id,
                        status: 'waiting_to_start',
                        game_start: null,
                        game_end: null
                    }

                };

                // la richiesta di matchmaking non serve più..
                self.cancelMatchMakingRequest(match.uid, false);

                DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

                    if(!err) {

                        table.findOneAndUpdate({

                            _id: new mongo.ObjectID(match.recordId)

                          }, {

                              // modify section
                              $set: {
                                status: 'accepted',
                                accept_time: (new Date()).getTime(),
                              }

                          }, {

                            upsert: false

                          }, function(err, result) { });                    

                    }

                });
                
            
            } else {

                // messaggio al client che ha accettato la sfida
                if (!isBot)
                {
                    clientConnection.emit('server_message', {
                        type: 'game_invite_expired_or_cancelled',
                        msg_body: msg.msg_body
                    });
                }
                else
                {
                    //match non disponibile, uccido il bot
                    clientConnection = null;
                }

            }

        } catch(err) {

            winston.error("acceptMatchMake/" + self.config.key + ": " , err);

        }
        

    }




    self.manageGameForfait = function(clientConnection, msg) {

        try {

            var match = self.this_game_running_games[msg.msg_body.match_id];
            var winner = null;
            var looser = null;

            if(match.inviter.uid == msg.msg_body.uid) {
                winner = match.invited;
                looser = match.inviter;
            } else {
                winner = match.inviter;
                looser = match.invited;
            }

            var isBot = winner.socket_id instanceof Bot;
            if (!isBot)
            {
                self.socket_namespace.to(winner.socket_id).emit('server_message', {
                    type: 'game_win_by_forfait',
                    msg_body: {
                        key: self.config.key,
                        match_id: match.match_id,
                        identifier: match.identifier,
                        winner: winner,
                        looser: looser
                    }
                });
            }
            else
            {
                //bonifico dei parametri in modo che il salvataggio sul db non fallisca
                winner.socket_id="bot";
                winner.status = 'ended';
            }

            delete self.this_game_running_games[msg.msg_body.match_id];

            DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

                console.log('err', err);

                if(!err) {

                    table.findOneAndUpdate({

                        _id: new mongo.ObjectID(match.recordId)

                      }, {

                          // modify section
                          $set: {
                            status: 'completed',
                            duration: (new Date()).getTime() - match.start_time,
                            winner: winner
                          }

                      }, {

                        upsert: false

                      }, function(err, result) { 
                        console.log("err",err);
                    });                    

                }

            });

        } catch(err) {

            winston.error("manageGameForfait/" + self.config.key + ": " , err);

        }
        

    }
 



    self.userStartedGame = function(clientConnection, msg) {

        try {

            var match = self.this_game_running_games[msg.msg_body.match_id];

            if(!match) {
                return;
            }

            var user_role = (msg.msg_body.uid == match.inviter.uid) ? 'inviter' : 'invited';

            match[ user_role ].status = 'playing';
            match[ user_role ].game_start = (new Date()).getTime();
            match[ user_role ].game_end = null;

        } catch(err) {

            winston.error("userStartedGame/" + self.config.key + ": " , err);
            
        }
       
    }

    
    
    
    

    self.notifyGameEnd = function(params) {
        
        try {
            
            var isBot = params.winner.socket_id instanceof Bot;
            if (!isBot)
            {
                self.socket_namespace.to(params.winner.socket_id).emit('server_message', {
                    type: 'game_victory',
                    msg_body: {
                        key: self.config.key,
                        match_id: params.match.match_id,
                        identifier: params.match.identifier,
                        victory_condition: params.victory_condition
                    }
                });
            }
            else
            {
                //bonifico dei parametri in modo che il salvataggio sul db non fallisca
                params.winner.socket_id="bot";
                params.winner.status = 'ended';
            }

            var isBot = params.looser.socket_id instanceof Bot;
            if (!isBot)
            {
                self.socket_namespace.to(params.looser.socket_id).emit('server_message', {
                    type: 'game_lost',
                    msg_body: {
                        key: self.config.key,
                        match_id: params.match.match_id,
                        identifier: params.match.identifier,
                        victory_condition: params.victory_condition
                    }
                });
            }
            else
            {
                //bonifico dei parametri in modo che il salvataggio sul db non fallisca
                params.looser.socket_id="bot";
                params.looser.status = 'ended';
            }

            delete self.this_game_running_games[ params.match.match_id ];

            DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

                if(!err) {

                    table.findOneAndUpdate({

                        _id: new mongo.ObjectID(params.match.recordId)

                      }, {

                          // modify section
                          $set: {
                            status: 'completed',
                            duration: (new Date()).getTime() - params.match.start_time,
                            winner: params.winner
                          }

                      }, {

                        upsert: false

                      }, function(err, result) {});                    

                }

            });

        } catch(err) {

            winston.error("notifyGameEnd/" + self.config.key + ": " , err);

        }


    }




    self.allQuestionsAnswered = function(clientConnection, msg) {

        try {

            var match = self.this_game_running_games[msg.msg_body.match_id];

            var user_who_completed_answers = (msg.msg_body.uid == match.inviter.uid) ? match['inviter'] : match['invited'];
            var other_user = (msg.msg_body.uid == match.inviter.uid) ? match['invited'] : match['inviter'];
            
            user_who_completed_answers.status = 'ended';
            user_who_completed_answers.game_end = (new Date()).getTime() - user_who_completed_answers.game_start;
            user_who_completed_answers.right_answers = msg.msg_body.game_data.right_answers;

            var isBot = user_who_completed_answers.socket_id instanceof Bot;
            if (isBot) user_who_completed_answers.game_end=other_user.game_end + msg.msg_body.delta_time;

            if(other_user.status == 'ended') {

                // ########################################################### //
                // entrambi gli utenti sono giunti alla fine del questionario. //
                // confrontiamo i tempi ed il numero di risposte esatte        //
                // ########################################################### //
                console.log("ENTRAMBI GLI UTENTI HANNO TERMINATO IL QUESTIONARIO");
                
                if(user_who_completed_answers.right_answers == other_user.right_answers) {

                    var _winner = user_who_completed_answers.game_end <= other_user.game_end ? user_who_completed_answers : other_user; 
                    var _looser = user_who_completed_answers.game_end <= other_user.game_end ? other_user : user_who_completed_answers;

                    self.notifyGameEnd({
                        match: match,
                        winner: _winner, 
                        looser: _looser,
                        victory_condition: {
                            motivation: 'less_time',
                            winner_time: Math.ceil(_winner.game_end / 1000),
                            looser_time: Math.floor(_looser.game_end / 1000) 
                        }
                    });

                } else {

                    var _winner = user_who_completed_answers.right_answers > other_user.right_answers ? user_who_completed_answers : other_user;
                    var _looser = user_who_completed_answers.right_answers > other_user.right_answers ? other_user : user_who_completed_answers;

                    self.notifyGameEnd({
                        match: match,
                        winner: _winner, 
                        looser: _looser,
                        victory_condition: {
                            motivation: 'more_right_answers',
                            winner_answers: _winner.right_answers,
                            looser_answers: _looser.right_answers, 
                        }
                    });

                }

            } else {

                // il primo dei due utenti ha terminato il questionario
                console.log("UTENTE HA TERMINATO LE DEOMANDE. ASPETTO L'altro");

                if(other_user.status == 'waiting_to_start') {

                    // ########################################################################## //
                    // un utente completa tutte le risposte mentre l'altro ancora non ha iniziato //
                    // il primo vince ed il secondo perde                                         //
                    // ########################################################################## //
                    self.notifyGameEnd({
                        match: match,
                        winner: user_who_completed_answers, 
                        looser: other_user,
                        victory_condition: {
                            motivation: 'did_not_even_start' 
                        }
                    });

                } else if(other_user.status == 'playing') {

                    // ####################################################################################### //
                    // un utente completa tuttle le risposte mentre l'altro ha iniziato il questionario        //
                    // a questo punto l'utente con il questionario in corso ha ancora X secondi per continuare //
                    // ####################################################################################### //
                    var isBot = other_user.socket_id instanceof Bot;

                    if (!isBot)
                    {
                        self.socket_namespace.to(other_user.socket_id).emit('server_message', {
                            type: 'game_message',                    
                            msg_body: {
                                sub_type: 'opponent_has_answered_all_questions',
                                key: self.config.key,
                                match_id: match.match_id,
                                identifier: match.identifier,
                                uid: user_who_completed_answers.uid,
                                user: user_who_completed_answers.user
                            }
                        });
                    }
                    else
                    {
                        // è un bot...
                        other_user.socket_id.userCompletedQuestions(user_who_completed_answers.right_answers,user_who_completed_answers.game_end);//gli passo il numero di risposte giuste e il tempo
                    }
                }


            }
            
        } catch(err) {

            winston.error("allQuestionsAnswered/" + self.config.key + ": " , err);

        }
        

    }


    // il server viene notificato del fatto che un utente ha fatto un progresso (nello specifico
    // ha trovato una immagine). notifico l'avversario del progresso avvenuto
    self.userHasMadeGameProgress = function(clientConnection, msg) {

        try {

            var match = self.this_game_running_games[msg.msg_body.match_id];
            var other_user = (msg.msg_body.uid == match.inviter.uid) ? match['invited'] : match['inviter'];


            self.socket_namespace.to(other_user.socket_id).emit('server_message', {
                type: 'game_message',
                sub_type: 'user_made_progress',
                msg_body: {
                    key: self.config.key,
                    match_id: match.match_id,
                    identifier: match.identifier,
                    uid: other_user.uid,
                    user: other_user.user,
                    game_progress: msg.msg_body.game_progress
                }
            });

        } catch(err) {

            winston.error("userHasMadeGameProgress/" + self.config.key + ": " , err);

        }

    }




};







