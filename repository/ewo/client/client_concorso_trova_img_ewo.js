

var client_concorso_trova_img_ewo = function(  ) {

    // componenti standard di ionic
    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.FileSystemService = null;
    this.scope = null;
    this.gameCenter = null;
    this.rootScope = null;
    this.WikiService = null;
    // componenti standard di ionic

    // variabili ad uso del gioco    
    this.isInArMode = false;
    this.currentGame = null;
    this.advice_scope = null;
    this.currentAlertBox = null;
    this.username = null;
    this.email = null;
    // variabili ad uso del gioco
     

    this.config = {
        
        name: 'Gioco: trova le immmagini nel minor tempo.',
        description: 'Trova tutte le immagini nel minor tempo possibile',
        key: 'mediasoft.concorso.trova.img.ewo',
        time_to_live: 60000,
        short_key: 'MCTEWO0',
        icon: 'ion-chatboxes',
        second_icon: 'ion-trophy',
        color: 'pink',
        index: 0,

        parameters: {

            background: {
                type: 'img',
                description: 'Immagine di sfondo usata nella lista dei giochi'
            },

            advice: {
                type: 'text',
                description: 'Consiglio generico su dove cercare le immagini'
            },

            tempo_limite: {
                type: 'number',
                description: 'Tempo Massimo per trovare tutte le immagini'
            },

            data_chiusura_gioco: {
                type: 'timestamp',
                description: 'Data entro cui il gioco si conclude'
            },

            lista_immagine_da_trovare: {
                type: 'wtc_image_list',
                description: 'lista di immagini da usare per il riconoscimento'
            }
                    
        },

        values: null

    };

}



// singleton della nostra classe di gioco
client_concorso_trova_img_ewo.Instance = null;



client_concorso_trova_img_ewo.Initialize = function(params) {

    if(client_concorso_trova_img_ewo.Instance != null) {
        return;
    }    

    client_concorso_trova_img_ewo.Instance = new client_concorso_trova_img_ewo();
    client_concorso_trova_img_ewo.Instance.ionicModal = params.ionicModal;
    client_concorso_trova_img_ewo.Instance.ionicPopup = params.ionicPopup;
    client_concorso_trova_img_ewo.Instance.HelperService = params.HelperService;
    client_concorso_trova_img_ewo.Instance.http = params.http;
    client_concorso_trova_img_ewo.Instance.GlobalVariables = params.GlobalVariables;
    client_concorso_trova_img_ewo.Instance.LoggerService = params.LoggerService;
    client_concorso_trova_img_ewo.Instance.FileSystemService = params.FileSystemService;
    client_concorso_trova_img_ewo.Instance.SocketService = params.SocketService;
    client_concorso_trova_img_ewo.Instance.gameCenter = params.GameCenter;
    client_concorso_trova_img_ewo.Instance.rootScope = params.RootScope;
    client_concorso_trova_img_ewo.Instance.WikiService = params.WikiService;
    
 


    var SEED = client_concorso_trova_img_ewo.Instance.config.short_key;

 

     
    // evento scatenato nel momento in cui entro nella pagina 'lista dei numeri del progetto'
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', function() {
        
        client_concorso_trova_img_ewo.Instance.gameCenter.unregisterGame({
            key: client_concorso_trova_img_ewo.Instance.config.key,
            title: client_concorso_trova_img_ewo.Instance.config.name,
            description: client_concorso_trova_img_ewo.Instance.description
        });
        

        client_concorso_trova_img_ewo.Instance.WikiService.registerCallback(client_concorso_trova_img_ewo.Instance.config.key, SEED + 'respondToMessageFromWiki', null);
        
    });


    // evento scatenato nel momento in cui entro nella pagina 'selezione della testata'
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', function() {        

        client_concorso_trova_img_ewo.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', null);
        client_concorso_trova_img_ewo.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', null);
        client_concorso_trova_img_ewo.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', null);                
        client_concorso_trova_img_ewo.Instance = null;    

    });

    // evento scatenato nel momento in cui entro nella pagina 'dettaglio del numero'
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', function() {

 
            client_concorso_trova_img_ewo.Instance.gameCenter.getGameConfig({
                key: client_concorso_trova_img_ewo.Instance.config.key,
                livello: 'progetto',
                identifier: client_concorso_trova_img_ewo.Instance.GlobalVariables.application.currentProgetto.idProgetto,
                onComplete: function(err, configuration) {                     
            
                    if(!err && configuration.values && configuration.visible == true) {                         

                        if(configuration.forDebugOnly == false || (configuration.forDebugOnly == true && client_concorso_trova_img_ewo.Instance.GlobalVariables.isDebugDevice == true)) {


                            client_concorso_trova_img_ewo.Instance.config.values = [];
                                                                          
                            var background = _.find(configuration.values, function(item) { return item.name == 'background' });      
                            background = background.value ? (JSON.parse(background.value)).path : "";
                            client_concorso_trova_img_ewo.Instance.config.values['background'] = client_concorso_trova_img_ewo.Instance.GlobalVariables.baseUrl + "/" + background;

                            var advice = _.find(configuration.values, function(item) { return item.name == 'advice' });   
                            client_concorso_trova_img_ewo.Instance.config.values['advice'] =  advice.value;    

                            var tempo_limite = _.find(configuration.values, function(item) { return item.name == 'tempo_limite' });   
                            client_concorso_trova_img_ewo.Instance.config.values['tempo_limite'] =  tempo_limite.value;    
                            
                            var image_list_parameter = _.find(configuration.values, function(item) { return item.name == 'lista_immagine_da_trovare' });                
                            client_concorso_trova_img_ewo.Instance.config.values['lista_immagine_da_trovare'] = JSON.parse(image_list_parameter.value); 

                            var data_chiusura_gioco = _.find(configuration.values, function(item) { return item.name == 'data_chiusura_gioco' });                
                            client_concorso_trova_img_ewo.Instance.config.values['data_chiusura_gioco'] = data_chiusura_gioco.value; 
                            

                            try {

                                client_concorso_trova_img_ewo.Instance.gameCenter.registerGame({
                                    key: client_concorso_trova_img_ewo.Instance.config.key,
                                    icon: client_concorso_trova_img_ewo.Instance.config.icon,
                                    second_icon: client_concorso_trova_img_ewo.Instance.config.second_icon,
                                    color: client_concorso_trova_img_ewo.Instance.config.color,
                                    index: client_concorso_trova_img_ewo.Instance.config.index,
                                    title: client_concorso_trova_img_ewo.Instance.config.name,
                                    description: client_concorso_trova_img_ewo.Instance.config.description,
                                    classInstance: client_concorso_trova_img_ewo.Instance,
                                    background: client_concorso_trova_img_ewo.Instance.config.values['background'],
                                    onClickFunctionName: 'partecipaAlConcorso'
                                });


                                // LISTA EVENTI DA REALTA' AUMENTATA
                                
                                
                                // ricezione eventi dall'ambiente wikitude dedicato ai giochi collaborativi
                                client_concorso_trova_img_ewo.Instance.WikiService.registerCallback(client_concorso_trova_img_ewo.Instance.config.key, SEED + 'respondToMessageFromWiki', function(message) {
                                    
                                    if(!(message.type && message.key)) {
                                        return;
                                    }

                                    if(message.type == 'startRA') {

                                        client_concorso_trova_img_ewo.Instance.userHasStartedAR(message);

                                    } else if(message.type == 'AR_Forfait') {
                                        
                                        client_concorso_trova_img_ewo.Instance.WikiService.sendMessageToAR({
                                            type: 'closeAR'
                                        });

                                        client_concorso_trova_img_ewo.Instance.doForfaitGame();

                                    
                                    } else if(message.type == 'AR_Image_Tracked') {
                                    
                                        client_concorso_trova_img_ewo.Instance.onImageTracked(message.msg_body);
                                    
                                    } else if(message.type == 'AR_Countdown_finish') {
                                        
                                        client_concorso_trova_img_ewo.Instance.WikiService.sendMessageToAR({
                                            type: 'closeAR'
                                        });

                                        client_concorso_trova_img_ewo.Instance.onCountdownFinish(message.msg_body);
                                    
                                    } 

                                });
                                
                                
                                
                            
                            } catch(err) {

                                alert(err);
                            
                            }

                        }
                        
                    }                 
                }                
            });

        
                    
    });
            


}



client_concorso_trova_img_ewo.prototype.partecipaAlConcorso = function() {

    var self = this;
    self.isInArMode = false;

    if(!self.HelperService.isNetworkAvailable()) {

        self.ionicPopup.alert({
            title: 'Attenzione <i class="ion-android-alert"></i>',
            template: 'Non sei attualmente connesso ad Internet'
        });

        return;
    }

    self.gameCenter.getNicknameAndUserData({
        sub_title: 'Attenzione la mail è obbligatoria',
        onComplete: function(nickname, email) {

            if(!(nickname && email)) {

                self.ionicPopup.alert({
                    title: 'Attenzione <i class="ion-android-alert"></i>',
                    template: 'Poichè questo è un gioco a premi è necessario inserire uno username e un indirizzo email corretto.'
                });

                return;
            }

            self.nickname = nickname;
            self.email = email;
            
            var NOW = (new Date()).getTime();

            if(self.config.values['data_chiusura_gioco'] && NOW >= self.config.values['data_chiusura_gioco']) {

                self.http({
                    url: self.GlobalVariables.application.applicationUrl + "/" + self.config.short_key + "/get_classifica_persistita",
                    responseType: 'json',
                    method: 'POST',
                    timeout: 10000,
                    data: {
                      uid: self.GlobalVariables.deviceUUID,
                      email: self.email,
                      identifier: self.GlobalVariables.application.currentProgetto.idProgetto
                    }
                }).then(function(json_result) {

                    var posizione_classifica = 
                        json_result.data.score == 0 
                        ? 'Al momento in classifica risulti oltre la centesima posizione'
                        : 'Al momento in classifica sei in posizione n° ' +  json_result.data.score;

                    self.ionicPopup.alert({
                        title: 'Attenzione <i class="ion-android-alert"></i>',
                        template: '<strong>Purtroppo il gioco per questo numero è concluso</strong><br>' + posizione_classifica
                    });

                }, function(err) {
console.log(err);
                    self.ionicPopup.alert({
                        title: 'Attenzione <i class="ion-android-alert"></i>',
                        template: 'Si è verificato un errore sconosciuto. Provare più tardi'
                    });

                });
                

                return;

            }



            self.http({
                url: self.GlobalVariables.application.applicationUrl + "/" + self.config.short_key + "/check_play",
                responseType: 'json',
                method: 'POST',
                timeout: 10000,
                data: {
                  uid: self.GlobalVariables.deviceUUID,
                  email: self.email,
                  identifier: self.GlobalVariables.application.currentProgetto.idProgetto
                }
            }).then(function(json_result) {

                if(!json_result.data.result) {                    

                    var posizione_classifica = 
                        json_result.data.score == 0 
                        ? 'Al momento in classifica risulti oltre la centesima posizione'
                        : 'Al momento in classifica sei in posizione n° ' +  json_result.data.score;

                    self.ionicPopup.alert({
                        title: 'Attenzione <i class="ion-android-alert"></i>',
                        template: '<strong>Hai già giocato</strong><br>' + posizione_classifica 
                    });

                    return;
                }

                

                // da qui in poi posso giocare
                self.gameCenter.closeView(function() {
                    self.mostraIndizi();    
                });
                

            }, function(err) {

                self.ionicPopup.alert({
                    title: 'Attenzione <i class="ion-android-alert"></i>',
                    template: 'Si è verificato un errore sconosciuto. Provare più tardi'
                });

                return;

            });
            
        }

    });

}



client_concorso_trova_img_ewo.prototype.chiudiFinestraIndizio = function(onClose) {

    var self = this;

    if(self.advice_scope && self.advice_scope.adviceWnd) {

        self.advice_scope.adviceWnd.remove().then(function() {
            onClose();
        });        

        self.advice_scope.adviceWnd = null;
        self.advice_scope.$destroy();
        delete self.advice_scope;

    }
    
}

// il momento in cui mostro l'indizio è anche il momento in cui ho instanziato una sfida
// a questo punto il contatore delle partite giocate aumenta
client_concorso_trova_img_ewo.prototype.mostraIndizi = function() {

    var self = this;        
    self.advice_scope = self.rootScope.$new(true);
    self.advice_scope.adviceWnd = null;

    self.currentGame = {
        game_start_time: 0,
        game_total_time: 0,
        found_images_target: []
    };

    
    self.advice_scope.startGame = function() {

        self.chiudiFinestraIndizio(function() {
            self.startGame();    
        });
        
    };


    self.advice_scope.abortGame = function() {
        
        self.chiudiFinestraIndizio(function() {
            
            self.ionicPopup.alert({
                title: 'Peccato',
                template: 'Provaci più tardi'
            });
        });
        
    };
    

    self.advice_scope.adviceWnd = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" class="game-generic-modal" cache-view="false">' +

            '<ion-content scroll="true" padding="false" >' +

                '<div class="game-generic-inner">' +
                    
                    '<div><h2>' + self.config.name + '</h2></div>' +
                    '<div><h3>' + self.config.description + '</h3></div>' +
                    '<div><h3>Devi inquadrare con la fotocamera ' + self.config.values['lista_immagine_da_trovare'].listaImmagini.length + ' immagini prima della scadenza del tempo massimo. Hai a disposizione ' + self.config.values['tempo_limite'] + ' secondi</h3></div>' +

                                    
                    '<div><h2>L\'indizio è:</h2></div>' +
                    '<div><h3>' + self.config.values['advice'] + '</h3></div>' +                        

                '</div>' +

            '</ion-content>' +

            '<ion-footer-bar align-title="left" class="game-generic-footer row">' +
                '<div class="col">' +
                    '<button ng-click="abortGame()" class="button button-full button-assertive cst-button">Annulla</button>' +
                '</div>' +
                '<div class="col">' +
                    '<button ng-click="startGame()" class="button button-full button-balanced cst-button">Ok Iniziamo!</button>' +
                '</div>' +
            '</ion-footer-bar>' +

        '</ion-modal-view>',

        {
            scope: self.advice_scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false,
            backdropClickToClose: false
        }

    );

    self.advice_scope.adviceWnd.show();

}


client_concorso_trova_img_ewo.prototype.startGame = function() {

    var self = this; 
    
    var WTC_PATH = self.config.values['lista_immagine_da_trovare'].wtcPath;
 
    var json = {
        wtc: WTC_PATH,   
        labels: {
            abbandona: 'Basta così!',
            conferma_abbandona: 'Sei sicuro di voler terminare?',
            conferma_abbandona_si: 'Si',
            conferma_abbandona_no: 'No',
        }             
    }

    self.currentAlertBox = self.ionicPopup.show({
        title: '<div>Avvio ambiente realtà aumentata</div>',
        template: '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'        
    });

    self.WikiService.openArForGame(self.config.key, json);
    
};


client_concorso_trova_img_ewo.prototype.userHasStartedAR = function() {

    var self = this;     
    
    if(self.currentAlertBox) {
        self.currentAlertBox.close();
        self.currentAlertBox = null;    
    }
        
    self.isInArMode = true;

    self.currentGame.game_start_time = (new Date()).getTime();

    self.WikiService.sendMessageToAR({
        type: 'showCountdown',
        msg_body: {
            time: self.config.values['tempo_limite']
        }
    });


    setTimeout(function() {
        
        self.WikiService.sendMessageToAR({
            type: 'showAlert',
            msg_body: {
                messaggio: "Trova tutte le immagini",
                duration: 4000
            }
        });

    }, 500);
    
    
};


// ###################################################### //
// abbandono il gioco esplicitamente vince l'altro utente //
// ###################################################### //
client_concorso_trova_img_ewo.prototype.doForfaitGame = function() {

    var self = this;
    
    self.currentGame.game_total_time = Math.round(((new Date()).getTime() - self.currentGame.game_start_time) / 1000);

    self.assertEnding({
        msg: '<strong>ti sei già stancato!</strong> Hai comunque trovato ' + 
            self.currentGame.found_images_target.length + ' immagine/i su ' + 
            self.config.values['lista_immagine_da_trovare'].listaImmagini.length + ' in ' + self.currentGame.game_total_time + ' secondi.'
    });

    
}; 


client_concorso_trova_img_ewo.prototype.assertEnding = function( params ) {

    var self = this;

    var title = '';
    var template = '';
    var button = '';

    

    self.ionicPopup.alert({
        title: 'Fine del gioco',              
        template: params.msg,
        okText: 'Chiudi', 
        okType: 'button-balanced'        
    });

    self.http({
        url: self.GlobalVariables.application.applicationUrl + "/" + self.config.short_key + "/save_play_result",
        responseType: 'json',
        method: 'POST',
        timeout: 10000,
        data: {
              uid: self.GlobalVariables.deviceUUID,
              user: self.nickname,
              email: self.email,
              identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
              total_time: self.currentGame.game_total_time,
              punteggio: self.currentGame.found_images_target.length
        }

    }).then(function(json_result) {

        self.currentGame = null;

    }, function(err) {

        self.currentGame = null;
    });


}




client_concorso_trova_img_ewo.prototype.onImageTracked = function(message) {

    var self = this;

    var matched_image = _.find(self.config.values['lista_immagine_da_trovare'].listaImmagini, function(image) {
        return (message.target == image.targetName);
    });

    if(matched_image) {
        
        if(self.currentGame.found_images_target.indexOf(message.target) == -1) {

            self.currentGame.found_images_target.push(message.target);

            if(self.currentGame.found_images_target.length == self.config.values['lista_immagine_da_trovare'].listaImmagini.length) {

                self.WikiService.sendMessageToAR({
                    type: 'closeAR'
                });

                self.isInArMode = false;   

                self.currentGame.game_total_time = Math.round(((new Date()).getTime() - self.currentGame.game_start_time) / 1000);

                self.assertEnding({
                    msg: '<strong>Complimenti!</strong> Hai comunque trovato tutte le ' + 
                        self.config.values['lista_immagine_da_trovare'].listaImmagini.length + 
                        ' immagini in ' + self.currentGame.game_total_time + ' secondi'
                });

            } else {

                self.WikiService.sendMessageToAR({
                    type: 'showAlert',
                    msg_body: {
                        messaggio: 'Hai trovato ' + self.currentGame.found_images_target.length + ' immagine/i su ' + self.config.values['lista_immagine_da_trovare'].listaImmagini.length,
                        duration: 3000
                    }
                });

            }


        } else {

            self.WikiService.sendMessageToAR({
                type: 'showAlert',
                msg_body: {
                    messaggio: 'Hai già trovato quest\'immagine',
                    duration: 4000
                }
            });

        }
                
    } 

}


client_concorso_trova_img_ewo.prototype.onCountdownFinish = function() {

    var self = this;

    self.currentGame.game_total_time = self.config.values['tempo_limite'];

    self.assertEnding({
        msg: '<strong>Tempo Scaduto!</strong> Hai trovato ' + self.currentGame.found_images_target.length + ' immagine/i su ' + self.config.values['lista_immagine_da_trovare'].listaImmagini.length
    });

}







