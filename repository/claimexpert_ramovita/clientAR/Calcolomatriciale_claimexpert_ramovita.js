var Calcolomatriciale_claimexpert_ramovita = function(container,dataObj, closeFn, externalFn) {
   
   
   this.container = container;
   this.baseUrl = dataObj.contenutiUrl;
   this.oggettoTarget = dataObj.oggetto;
   this.nome = "Calcolomatriciale";
   this.indiceComboColonne =-1;
   this.indiceComboRighe =-1;
   this.indiceComboParametro =-1;
   this.result =0;
   this.idHS=-1;
   this.oggettoMatriciale=null;
   this.idImmagine=-1;
   this.idPunto=-1;

   console.log("Classe dinamica: "+this.nome);

   //this.AR = ar;
   this.ARclose=closeFn;
   this.externalFunction=externalFn;

   this.itemVuoto="img/stellaOff.png";
   this.itemPieno="img/stellaOn.png";
   this.itemQuarto="img/stellaOnQuarto.png";
   this.itemMezzo="img/stellaOnMezzo.png";
   this.itemTreQuarti="img/stellaOnTrequarti.png";
   //this.idPunto = idPunto;

   this.arrayImages=new Array();
   this.arrayImages.push(this.itemVuoto);
   this.arrayImages.push(this.itemPieno);
   this.arrayImages.push(this.itemQuarto);
   this.arrayImages.push(this.itemMezzo);
   this.arrayImages.push(this.itemTreQuarti);

    this.descrizione = "";

   //this.preloadImages();


}



Calcolomatriciale_claimexpert_ramovita.prototype.renderMain = function(immagine, punto, deviceId) {


}

Calcolomatriciale_claimexpert_ramovita.prototype.renderMainExt = function(immagine, punto, indice, deviceId) {

    console.log("renderMainExt, immagine: " + immagine + " - punto: " + punto + " - indice: " + indice + " - deviceId: " + deviceId);
    console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    $("#loader").show();

    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //finestra a tutto schermo
    html+="<div id='"+this.nome+"_container' style='width:"+(larghezzaContenitore-(padding*2))+"px;position:relative;left:"+(padding)+"px;top:"+(padding)+"px;background-color:rgba(255,255,2555,1.0);height:"+(altezzaContenitore-(padding*2))+"px;'></div>"
    this.container.html(html);
    this.container.show();

    this.idHS=this.oggettoTarget[immagine].punti[punto].id;
    this.idImmagine=immagine;
    this.idPunto=punto;

    //var label=this.oggettoTarget[immagine].punti[punto].label;

    //trovo quello che riguarda la votazione
    var oggettoContenuti=JSON.parse(this.oggettoTarget[immagine].punti[punto].config);
    var oggettoPunto=oggettoContenuti[indice];
    this.oggettoMatriciale=JSON.parse(oggettoPunto.config);
    //this.descrizione = config.descrizione;

    this.continueRenderMain()
}


Calcolomatriciale_claimexpert_ramovita.prototype.continueRenderMain = function() {

    //console.log("continueRenderMain, immagine: " + immagine + " - punto: " + punto + " - deviceId: " + deviceId);
    //console.log(JSON.stringify(this.oggettoTarget));
    var self = this;

    html="";
    html+="<div><p class='dynLabel' style='color:#000000;'>Configuratore</p></div>";
    
    //COLONNE
    html+="<div><p class='dynText' style='color:#000000;'>"+this.oggettoMatriciale.labelColonne+"</p></div>";
    //metto il combo box
    html+="<select id='"+this.nome+"_comboColonne' style='width:80%;margin-left: 10px;'>";
    //metto una opzione nulla di base
    html+="<option value=-1>- Seleziona -</option>";    
    for (var i=0;i<this.oggettoMatriciale.numColonne;i++)
    {
      html+="<option value='"+i+"'>"+this.oggettoMatriciale.colonne[i].nome+"</option>";    
    }
    html+="</select><div id='"+self.nome+"_conferma1' class='dynButtonMini' style='color:#000000;border:1px solid #555555;display:none;'>CONFERMA</div><br><br>";    

    //RIGHE
    html+="<div><p class='dynText' style='color:#000000;'>"+this.oggettoMatriciale.labelRighe+"</p></div>";
    //metto il combo box
    html+="<select id='"+this.nome+"_comboRighe' style='width:80%;margin-left: 10px;'>";
    //metto una opzione nulla di base
    html+="<option value=-1>- Seleziona -</option>";    
    for (var i=0;i<this.oggettoMatriciale.numRighe;i++)
    {
      html+="<option value='"+i+"'>"+this.oggettoMatriciale.righe[i].nome+"</option>";    
    }
    html+="</select><div id='"+self.nome+"_conferma2' class='dynButtonMini' style='color:#000000;border:1px solid #555555;display:none;'>CONFERMA</div><br><br>";    

    //TERZOPARAMETRO, ma invisibile di base
    html+="<div><p id='"+this.nome+"_labelParametro' class='dynText' style='color:#000000;display:none;'></p></div>";
    //metto il combo box
    html+="<select id='"+this.nome+"_comboParametro' style='width:80%;margin-left: 10px;display:none;'>";
    html+="</select><div id='"+self.nome+"_conferma3' class='dynButtonMini' style='color:#000000;border:1px solid #555555;display:none;'>CONFERMA</div><br><br>";    

    html+="<div><p id='"+this.nome+"_labelRisultato' class='dynText' style='color:#000000;display:none;'>Valore Opzione:</p></div>";
    html+="<div><p id='"+this.nome+"_risultato' class='dynLabel' style='color:#FF0000;display:none;'></p></div>";

    html+="<div id='"+self.nome+"_chiudi' class='dynButton' style='color:#000000;border:1px solid #555555;'>CHIUDI</div>";


    var htmlObj = $(html);

    //htmlObj.appendTo($("#"+self.nome+"_container"));
    $("#"+self.nome+"_container").html(htmlObj);
    //$("#"+this.nome+"_container").append(html);

    $("#"+self.nome+"_comboColonne").change(function(e) {
      self.selezioneComboColonneChange();
    });

    $("#"+self.nome+"_comboColonne").focus(function(e) {
      $("#"+self.nome+"_conferma1").show();
    });

    $("#"+self.nome+"_comboRighe").change(function(e) {
      self.selezioneComboRigheChange();
    });

    $("#"+self.nome+"_comboRighe").focus(function(e) {
      $("#"+self.nome+"_conferma2").show();
    });

    $("#"+self.nome+"_comboParametro").change(function(e) {
      self.selezioneParametroChange();
    });

    $("#"+self.nome+"_comboParametro").focus(function(e) {
      $("#"+self.nome+"_conferma3").show();
    });


    $("#"+self.nome+"_chiudi").click(function(e) {
      self.container.html("");
      self.container.hide();
      //creo il parametro di uscita
      var param= new Object();
      param.code="Calcolomatriciale";
      var oggetto= new Object();
      oggetto.idHS=self.idHS;
      oggetto.idImmagine=self.idImmagine;
      oggetto.idPunto=self.idPunto;
      oggetto.result="€ " + self.result;
      oggetto.colonna=self.indiceComboColonne;
      oggetto.riga=self.indiceComboRighe;
      oggetto.parametro=self.indiceComboParametro;
      param.oggetto=oggetto;
      self.ARclose(param);
      //self.AR.hardware.camera.enabled = true;
    });

    $("#"+self.nome+"_conferma1").click(function(e) {
      $("#"+self.nome+"_comboColonne").blur();
      $("#"+self.nome+"_conferma1").hide();
    });

    $("#"+self.nome+"_conferma2").click(function(e) {
      $("#"+self.nome+"_comboRighe").blur();
      $("#"+self.nome+"_conferma2").hide();
    });

    $("#"+self.nome+"_conferma3").click(function(e) {
      $("#"+self.nome+"_comboParametro").blur();
      $("#"+self.nome+"_conferma3").hide();
    });

    self.resizeBasedWindows();

    $("#loader").hide();


}


Calcolomatriciale_claimexpert_ramovita.prototype.sceltaCombo = function(indiceColonne, indiceRighe) {
  
  console.log("sceltaCombo - colonna: " + indiceColonne + " - riga: "+ indiceRighe);
  var self = this;

  $("#"+self.nome+"_risultato").hide();
  $("#"+self.nome+"_labelRisultato").hide();


  if ((indiceColonne==-1) || (indiceRighe==-1))
  {
    $("#"+self.nome+"_labelParametro").hide();  
    $("#"+self.nome+"_comboParametro").hide();    
    return;
  }

  //cerco, tra le celle, la configurazione
  for (var i=0;i<this.oggettoMatriciale.celle.length;i++)
  {
    if ((this.oggettoMatriciale.celle[i].colonna==indiceColonne) && (this.oggettoMatriciale.celle[i].riga==indiceRighe))
    {
      console.log("trovata cella");
      console.log(this.oggettoMatriciale.celle[i]);
      $("#"+self.nome+"_labelParametro").html(this.oggettoMatriciale.celle[i].descrizione);
      //aggiungo le opzioni, prima lo svuoto
      $("#"+self.nome+"_comboParametro").find('option').remove().end();
      //metto una opzione nulla di base
      $("#"+self.nome+"_comboParametro").append($('<option>', {
          value: -1,
          text: "- Seleziona -"
      }));
      for (var t=0;t<this.oggettoMatriciale.celle[i].oggetto.length;t++)
      {
        $("#"+self.nome+"_comboParametro").append($('<option>', {
            value: t,
            text: this.oggettoMatriciale.celle[i].oggetto[t].chiave
        }));
      }
    }
  }

  $("#"+self.nome+"_labelParametro").show();  
  $("#"+self.nome+"_comboParametro").show();    

}


Calcolomatriciale_claimexpert_ramovita.prototype.selezioneComboColonneChange = function() 
{
  //prendo l'indice che ha scelto
  this.indiceComboColonne = parseInt($("#"+this.nome+"_comboColonne").val());
  console.log("Scelto indice colonna: " + this.indiceComboColonne);
  this.sceltaCombo(this.indiceComboColonne, this.indiceComboRighe);
}

Calcolomatriciale_claimexpert_ramovita.prototype.selezioneComboRigheChange = function() 
{
  //prendo l'indice che ha scelto
  this.indiceComboRighe = parseInt($("#"+this.nome+"_comboRighe").val());
  console.log("Scelto indice riga: " + this.indiceComboRighe);
  this.sceltaCombo(this.indiceComboColonne, this.indiceComboRighe);
}

Calcolomatriciale_claimexpert_ramovita.prototype.selezioneParametroChange = function() 
{
  //prendo l'indice che ha scelto
  this.indiceComboParametro = parseInt($("#"+this.nome+"_comboParametro").val());
  console.log("Scelto indice parametro: " + this.indiceComboParametro);
  this.calcolaValore(this.indiceComboColonne, this.indiceComboRighe, this.indiceComboParametro);
}

Calcolomatriciale_claimexpert_ramovita.prototype.calcolaValore = function(indiceColonne, indiceRighe, indiceParametro) {
  
  console.log("calcolaValore - colonna: " + indiceColonne + " - riga: "+ indiceRighe + " - parametro: " + indiceParametro);

  //primo, prendo il valore di base della colonna
  var valoreBase=Number(this.oggettoMatriciale.colonne[indiceColonne].valore);

  //prendo la l'oggetto cella
  for (var i=0;i<this.oggettoMatriciale.celle.length;i++)
  {
    if ((this.oggettoMatriciale.celle[i].colonna==indiceColonne) && (this.oggettoMatriciale.celle[i].riga==indiceRighe))
    {
      console.log("trovata cella");
      var operando = parseInt(this.oggettoMatriciale.celle[i].oggetto[indiceParametro].operando);
      var valore = Number(this.oggettoMatriciale.celle[i].oggetto[indiceParametro].valore);

      console.log("valoreBase: " + valoreBase + " - operando: " + operando + " - valore: " + valoreBase);

      this.result=0;
      if (operando==0) this.result = valoreBase*valore;
      else if (operando==1) this.result = valoreBase+valore;

      $("#"+this.nome+"_risultato").html("&euro; "+this.result);
      $("#"+this.nome+"_risultato").show();
      $("#"+this.nome+"_labelRisultato").show();

    }
  }




}

Calcolomatriciale_claimexpert_ramovita.prototype.resizeBasedWindows = function() {

    var self = this;
    var html="";
    //quanto é alto il contenitore?
    var altezzaContenitore=this.container.height();
    var larghezzaContenitore=this.container.width();
    var padding=larghezzaContenitore*0.05;
    padding=0; //fullscreen
    //console.log("Altezza container: " + altezzaContenitore);
    $("#"+this.nome+"_container").css("width",(larghezzaContenitore-(padding*2))+"px").css("left",(padding)+"px").css("height",(altezzaContenitore-(padding*2))+"px").css("top",(padding)+"px");

    var altezzaInner=$("#"+this.nome+"_container").height();
    var larghezzaInner=$("#"+this.nome+"_container").width();

    if (altezzaContenitore>larghezzaContenitore) //siamo in verticale
    {
      $("#"+this.nome+"_itemContainer").css("width","90%").css("margin-left","5%"); 
      $("#"+this.nome+"_itemContainer2").css("width","90%").css("margin-left","5%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.03).css("padding-top",altezzaContenitore*0.015).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.025).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.00).css("width","100%").css("margin-left","0px");
      $(".dynButton").css("font-size",altezzaContenitore*0.03).css("padding",altezzaContenitore*0.015).css("width","50%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.25)-(altezzaContenitore*0.015));
      $(".dynButtonMini").css("font-size",altezzaContenitore*0.03).css("padding",altezzaContenitore*0.005).css("width","40%").css("margin-top",altezzaContenitore*0.01).css("margin-left",(larghezzaInner*0.30)-(altezzaContenitore*0.015));
    }
    else //siamo in orizzontale
    {
      $("#"+this.nome+"_itemContainer").css("width","50%").css("margin-left","25%"); 
      $("#"+this.nome+"_itemContainer2").css("width","50%").css("margin-left","25%"); 
      $(".dynLabel").css("font-size",altezzaContenitore*0.04).css("padding-top",altezzaContenitore*0.02).css("margin-top",0);
      $(".dynText").css("font-size",altezzaContenitore*0.035).css("padding-top",altezzaContenitore*0.015).css("margin-top",altezzaContenitore*0.00).css("width","150%").css("margin-left","-25%");
      $(".dynButton").css("font-size",altezzaContenitore*0.05).css("padding",altezzaContenitore*0.015).css("width","30%").css("margin-top",altezzaContenitore*0.09).css("margin-left",(larghezzaInner*0.35)-(altezzaContenitore*0.015));
      $(".dynButtonMini").css("font-size",altezzaContenitore*0.05).css("padding",altezzaContenitore*0.005).css("width","20%").css("margin-top",altezzaContenitore*0.01).css("margin-left",(larghezzaInner*0.40)-(altezzaContenitore*0.015));
   }

}

Calcolomatriciale_claimexpert_ramovita.prototype.test = function() {

    var self = this;
    //alert("Classe Gradimento: " + this.idTarget + " - " + this.idPunto);

}

Calcolomatriciale_claimexpert_ramovita.prototype.chiSono = function() {
    return "Calcolomatriciale";
}



Calcolomatriciale_claimexpert_ramovita.prototype.preloadImages = function() {

    //console.log("sono in preloadImages");
    
    var imgs = new Array();
    for (var i=0;i<this.arrayImages.length;i++)
    {
      //console.log("Preload: " + this.baseUrl+"/"+ this.arrayImages[i]);
      imgs[i] = new Image();
      imgs[i].src = this.baseUrl+"/"+ this.arrayImages[i];
    }

    var iconaObj=this.getIcon();
    //console.log("Preload: " + this.baseUrl+"/"+ iconaObj.img);
    imgs[this.arrayImages.length] = new Image();
    imgs[this.arrayImages.length].src = this.baseUrl+"/"+ iconaObj.img;

}

