var Config = require('./config');
var RegionsDatabase = require('./regionCoords');
var DatabaseManager = require('./databaseManager');
var fs = require('fs');
var Path = require('path');
var _ = require('underscore');
var winston = require('winston');

var gestore_salvataggio_claimexpert_ramovita = function(parameters) {

    var self = this;


    // ############################################################### //
    // sezione dedicata alla validazione dei parametri del costruttore //
    // ############################################################### //
    if(!parameters.socket_namespace) {
      throw "Parametro 'parameters.socket_namespace' obbligatorio.";
    }

    if(!parameters.application_name) {
      throw "Parametro 'parameters.application_name' obbligatorio.";
    }

    if(!parameters.express_instance) {
      throw "Parametro 'parameters.express_instance' obbligatorio.";
    }


    self.socket_namespace = parameters.socket_namespace;
    self.application_name = parameters.application_name;
    self.express_instance = parameters.express_instance;


    if(!self.socket_namespace.additional_callbacks) {
      self.socket_namespace.additional_callbacks = {};
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.CONNECT] = [];
    }

    if(!self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT]) {
      self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT] = [];
    }




    // ######################################################################### //
    // una classe espone ed aggiunge callback che possono agire su eventi socket //
    // ######################################################################### //
    self.socket_namespace.additional_callbacks[Config.SOCKET_EVENTS.MESSAGE].push(function(clientConnection, message) {

        if(message.type == 'has_saved_item') {

            DatabaseManager.getTable(application_name, 'event', function(err, table) {

                if(!err) {

                    var _data = message.msg_body;
                    _data.type = 'has_saved_item';
                    _data.time = (new Date()).getTime();

                    table.insertOne(_data);

                }

            });

        }

    });

};
