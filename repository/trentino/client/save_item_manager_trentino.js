var save_item_manager_trentino = function( params ) {

    var self = this;
    self.SAVED_ITEMS_DIRECTORY = 'trentino_saved_items';
    self.SAVED_ITEMS_DESCRIPTOR_NAME = 'trentino_saved_items.json';
    self.KEY = 'TRENTINO_NO_';


    self.ionicModal = params.ionicModal;
    self.ionicPopup = params.ionicPopup;
    self.HelperService = params.HelperService;
    self.http = params.http;
    self.GlobalVariables = params.GlobalVariables;
    self.LoggerService = params.LoggerService;
    self.FileSystemService = params.FileSystemService;  
    self.rootScope = params.rootScope;
    self.WikiService = params.WikiService;
    self.ionicLoading = params.ionicLoading;
    self.SocketService = params.SocketService;
    self.LanguageService = params.LanguageService;

    self.globalSavedRecipesDescriptor = {};
    self.currentRivistaDescriptor = [];

    self.scope = null;


    var style = document.createElement('style');
    style.type = 'text/css';

    style.innerHTML = 
    '.zoom-pane {' +
    '   width: 100% !important;' +
    '   height: 100% !important;' +
    '}' +

    '.zoom-pane .scroll {' +
    '   min-height: 100% !important;' +
    '   display: -webkit-box;' +
    '   display: -moz-box;' +
    '   display: -ms-flexbox;' +
    '   display: -webkit-flex;' +
    '   display: flex;' +
    '   -webkit-box-direction: normal;' +
    '   -moz-box-direction: normal;' +
    '   -webkit-box-orient: horizontal;' +
    '   -moz-box-orient: horizontal;' +
    '   -webkit-flex-direction: row;' +
    '   -ms-flex-direction: row;' +
    '   flex-direction: row;' +
    '   -webkit-flex-wrap: nowrap;' +
    '   -ms-flex-wrap: nowrap;' +
    '   flex-wrap: nowrap;' +
    '   -webkit-box-pack: center;' +
    '   -moz-box-pack: center;' +
    '   -webkit-justify-content: center;' +
    '   -ms-flex-pack: center;' +
    '   justify-content: center;' +
    '   -webkit-align-content: stretch;' +
    '   -ms-flex-line-pack: stretch;' +
    '   align-content: stretch;' +
    '   -webkit-box-align: center;' +
    '   -moz-box-align: center;' +
    '   -webkit-align-items: center;' +
    '   -ms-flex-align: center;' +
    '   align-items: center;' +
    '}' +

    '.zoom-pane img {' +
    '   width: 100% !important;' +
    '    vertical-align: middle !important;' +
    '}' +

    '.image-modal {' +
    '    width: 100% !important;' +
    '    height: 100% !important;' +
    '    top: 0 !important;' +
    '    left: 0 !important;' +
    '    background: black !important;' +
    '}';


    document.getElementsByTagName('head')[0].appendChild(style);

 

}

save_item_manager_trentino.Instance = null;

save_item_manager_trentino.hashCode = function(str) {

    var hash = 0;

    for (var i = 0; i < str.length; i++) {
        hash = ~~(((hash << 5) - hash) + str.charCodeAt(i));
    }

    return hash;
}


save_item_manager_trentino.addBadge = function(noRivista, noSavedItems) {

    try {

        var badge = document.createElement('div');
        badge.id = 'badge_of_' + noRivista;
        badge.style.background = 'red';
        badge.style.color = 'white';
        badge.style.width = '20px';
        badge.style.textAlign = 'center';
        badge.style.position = 'absolute';
        badge.style.right = '3px';
        badge.style.top = '3px';
        badge.style.padding = '0px 4px';
        badge.style.borderRadius = '10px';
        badge.style.zIndex = '999999';
        badge.style.display = 'block';
        badge.style.boxShadow = '-2px 3px 5px 0px rgba(0,0,0,0.75)';


        badge.appendChild(document.createTextNode( noSavedItems ));
        document.getElementById('div_progetto_' + noRivista).appendChild( badge );
 

    } catch(err) {}
	

}


save_item_manager_trentino.removeBadge = function(noRivista) {

	try {

      var elem = document.getElementById('badge_of_' + noRivista);
      elem.parentNode.removeChild(elem);

    } catch(err) { }

}




save_item_manager_trentino.Initialize = function(params) {

    if(save_item_manager_trentino.Instance != null) {
      return;
    }

    save_item_manager_trentino.Instance = new save_item_manager_trentino({
    	ionicModal: params.ionicModal,
    	ionicPopup: params.ionicPopup,
    	HelperService: params.HelperService,
    	http: params.http,
    	GlobalVariables: params.GlobalVariables,
    	LoggerService: params.LoggerService,
    	FileSystemService : params.FileSystemService,
    	rootScope: params.rootScope,
    	WikiService: params.WikiService,
        ionicLoading: params.ionicLoading,
        SocketService: params.SocketService,
        LanguageService: params.LanguageService

    });

    
    save_item_manager_trentino.Instance.loadGlobalDescriptor();

    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, 'TrentinoSavedItemDeallocate', function() {
        
        if(save_item_manager_trentino.Instance) {
            params.rootScope.removeItemFromMenu('map_read_saved_items_trentino');
            params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_TESTATA, params.LoggerService.ACTION.ENTER, 'removeSavedItemKey_Trentino', null);
            params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, 'removeSavedItemKey_Trentino', null);
            params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.DATA_LOAD, 'savedItemsShow_Trentino', null);
            params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, 'addSavedItemKey_Trentino', null);
            params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.WIKI_STATE, params.LoggerService.ACTION.TRIGGER_WIKI_EVT, 'saveItemFromWiki_Trentino', null);
            save_item_manager_trentino.Instance = null;

        }

    });

    // mi assicuro di rimuovere la vode dal menu se entro nella vista di dettaglio della testata (entry point della rivista)
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_TESTATA, params.LoggerService.ACTION.ENTER, 'removeSavedItemKey_Trentino', function() {
        params.rootScope.removeItemFromMenu('map_read_saved_items_trentino');
        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.WIKI_STATE, params.LoggerService.ACTION.TRIGGER_WIKI_EVT, 'saveItemFromWiki_Trentino', null);
    });



    // mi assicuro di rimuovere la vode dal menu se entro nella vista dell'elenco dei numeri della rivista
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, 'removeSavedItemKey_Trentino', function() {
      params.rootScope.removeItemFromMenu('map_read_saved_items_trentino');
      params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.WIKI_STATE, params.LoggerService.ACTION.TRIGGER_WIKI_EVT, 'saveItemFromWiki_Trentino', null);
    });



    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.DATA_LOAD, 'savedItemsShow_Trentino', function() {

        setTimeout(function() {
 

            for(var j in save_item_manager_trentino.Instance.globalSavedRecipesDescriptor) {
            
                var noSavedReceipts = save_item_manager_trentino.Instance.globalSavedRecipesDescriptor[j];
                var noRivista = j.substring( save_item_manager_trentino.Instance.KEY.length );

                save_item_manager_trentino.removeBadge(noRivista);

                if(noSavedReceipts > 0) {                                       
                    save_item_manager_trentino.addBadge(noRivista, noSavedReceipts);
                }

            }

        }, 100);


    });


    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, 'addSavedItemKey_Trentino', function() {

        // ################################################################# //
        // in questo caso posso caricare il dettaglio della rivista corrente //
        // ################################################################# //
        save_item_manager_trentino.Instance.loadCurrentRivistaDescriptor(function(err) {

            // ##################################################################################################################### //            
            // uso il descrittore generale. per la rivista con indice GZ_NO_<id rivista> verifico che il contatore sia maggiore di 0 //
            // ##################################################################################################################### //
            var numElementiSalvati=save_item_manager_trentino.Instance.hasSavedRecipesToShow();
            if(numElementiSalvati>0)
            {
                params.rootScope.addItemToMenu('map_read_saved_items_trentino', numElementiSalvati + " " + (params.LanguageService ? params.LanguageService.getLabel("ELEMENTI_SALVATI") : 'Elementi Salvati'), 'ion-social-buffer', function() {

                    save_item_manager_trentino.Instance.showPersistedRecipes();

                });
       
            }

        });


        


        params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.WIKI_STATE, params.LoggerService.ACTION.TRIGGER_WIKI_EVT, 'saveItemFromWiki_Trentino', function(data) {

            try {

                save_item_manager_trentino.Instance.saveRecipe({
                    url: data.url,
                    fileName: "recipe_" + (new Date()).getTime() + ".jpg",
                    descr: data.descr,
                    onComplete: function( err ) {

                        save_item_manager_trentino.Instance.GlobalVariables.wikitudePlugin
                            .callJavaScript("downloadElementStatus('" +  (err ? err : 'ok')  + "');");

                    }
                });

            } catch(err) { }
            

        });


    });


}




save_item_manager_trentino.prototype.updateGlobalDescriptor = function(onComplete) {

    var self = this;

    self.FileSystemService.writeFile({
        directory: self.SAVED_ITEMS_DIRECTORY,
        fileName: self.SAVED_ITEMS_DESCRIPTOR_NAME,
        bufferData: JSON.stringify(self.globalSavedRecipesDescriptor),
        onComplete: function(err, operationCompleted) {
            onComplete(err);
        }
    });

}


save_item_manager_trentino.prototype.updateCurrentRivistaDescriptor = function(onComplete) {

    var self = this;
    var noRivista = self.GlobalVariables.application.currentProgetto.idProgetto;
    var gKey = self.KEY + noRivista;
    var currentRivistaFileName = gKey + '.json';

    self.FileSystemService.writeFile({
        directory: self.SAVED_ITEMS_DIRECTORY,
        fileName: currentRivistaFileName,
        bufferData: JSON.stringify(self.currentRivistaDescriptor),
        onComplete: function(err, operationCompleted) {
            onComplete(err);
        }
    });

}





save_item_manager_trentino.prototype.saveRecipe = function(params) {


        var self = this;
        var noRivista = self.GlobalVariables.application.currentProgetto.idProgetto;
        var gKey = self.KEY + noRivista;
        var uKey = save_item_manager_trentino.hashCode(  params.url );
        var isYetPresent = false;

        for(var j=0; j<self.currentRivistaDescriptor.length; j++) {
          if(self.currentRivistaDescriptor[j].key == uKey) {
              isYetPresent = true;
          }
        }

        if(isYetPresent == true) {
          params.onComplete(self.LanguageService ? self.LanguageService.getLabel("ELEMENTO_GIA_REGISTRATO") : 'Questo Elemento è stato già registrato su questo dispositivo');
          return;
        }

        
        try {
			
            self.SocketService.emit('message', 'has_saved_item', {}, {
                uid: self.GlobalVariables.deviceUUID,
                id_progetto: noRivista,
                descr: params.descr ? params.descr : noRivista
            });

        } catch(err) {
            
        }


      

        self.FileSystemService.downloadFile({
          url: params.url,
          fileName: params.fileName ,
          directory: self.SAVED_ITEMS_DIRECTORY,
          onComplete: function(err, localFilePath) {

                if(err) {
                    params.onComplete(err);
                    return;
                }

                save_item_manager_trentino.Instance.rootScope.addItemToMenu('map_read_saved_items_trentino', self.LanguageService ? self.LanguageService.getLabel("ELEMENTI_SALVATI") : 'Elementi Salvati', 'ion-social-buffer', function() {
                     save_item_manager_trentino.Instance.showPersistedRecipes();
                });

                if(!self.globalSavedRecipesDescriptor[gKey]) {
                    self.globalSavedRecipesDescriptor[gKey] = 0;
                }

                self.globalSavedRecipesDescriptor[gKey]++;

                self.updateGlobalDescriptor(function(err) {

                    if(err) {

                        params.onComplete(err);
                        return;
                    }

                    self.currentRivistaDescriptor.push({
                        key: uKey,
                        localPath: params.fileName,
                        descr: params.descr
                    });

                    self.updateCurrentRivistaDescriptor(function(err) {

                        if(err) {
                          params.onComplete(err);
                          return;
                        }

                        params.onComplete(null);

                    });

                });


          }

        });



}



save_item_manager_trentino.prototype.loadGlobalDescriptor = function() {

    var self = this;

    

    self.FileSystemService.fileExists({

        directory: self.SAVED_ITEMS_DIRECTORY,
        fileName: self.SAVED_ITEMS_DESCRIPTOR_NAME,
        onComplete: function(err, isAvailable) {

            if(!err && isAvailable) {

                self.FileSystemService.fileRead({
                    directory: self.SAVED_ITEMS_DIRECTORY,
                    fileName: self.SAVED_ITEMS_DESCRIPTOR_NAME,
                    onComplete: function(err, buffereData) {

                        try {
            
                            self.globalSavedRecipesDescriptor = JSON.parse(buffereData);

                        } catch(err) {

                        }

                    }
                });

            }

        }

    });

};






save_item_manager_trentino.prototype.loadCurrentRivistaDescriptor = function(onComplete) {

    var self = this;
    var noRivista = self.GlobalVariables.application.currentProgetto.idProgetto;
    var gKey = self.KEY + noRivista;


    var currentRivistaFileName = gKey + '.json';

    self.FileSystemService.fileExists({

        directory: self.SAVED_ITEMS_DIRECTORY,
        fileName: currentRivistaFileName,
        onComplete: function(err, isAvailable) {

            if(!err && isAvailable) {

                self.FileSystemService.fileRead({
                    directory: self.SAVED_ITEMS_DIRECTORY,
                    fileName: currentRivistaFileName,
                    onComplete: function(err, buffereData) {

                        if(err) {
                            onComplete(err);
                        } else {
                            self.currentRivistaDescriptor = JSON.parse(buffereData);
                            onComplete(null);
                        }

                    }

                });

            }

        }

    });

};







save_item_manager_trentino.prototype.hasSavedRecipesToShow = function( ) {

    var self = this;
    var noRivista = self.GlobalVariables.application.currentProgetto.idProgetto;
    var gKey = self.KEY + noRivista;

    if(!self.globalSavedRecipesDescriptor[gKey]) {
        self.globalSavedRecipesDescriptor[gKey] = 0;
    }
    if (self.globalSavedRecipesDescriptor[gKey])
    {
        return self.globalSavedRecipesDescriptor[gKey];
    }
    else return 0;

    //return (self.globalSavedRecipesDescriptor[gKey] && self.globalSavedRecipesDescriptor[gKey] > 0);

}




save_item_manager_trentino.prototype.startProcessLocalFileQueue = function(params) {

    var self = this;
    self.images_to_process = [];

    for(var j in self.currentRivistaDescriptor) {

        self.images_to_process.push({
            index: j,
            fileName: self.currentRivistaDescriptor[j].localPath
        })
    }

    self.resolveLocalFile(params);

};


save_item_manager_trentino.prototype.resolveLocalFile = function(params) {

    var self = this;

    if(self.images_to_process.length == 0) {

        params.onComplete();

    } else {

        var item = self.images_to_process.pop();

        self.FileSystemService.getFileContentAsBase64({
            directory: self.SAVED_ITEMS_DIRECTORY,
            fileName: item.fileName,
            onComplete: function(err, base64Data) {

                if(!err) {

                    document.getElementById('recipe_' + item.index).style.backgroundImage = 'url(' + base64Data + ')';
                    //document.getElementById('recipe_' + item.index).src = 'url(' + base64Data + ')';

                }

                self.resolveLocalFile(params);

            }
        });

    }

}


save_item_manager_trentino.prototype.showPersistedRecipes = function() {

  	var self = this;
    var slides = "";

    self.ionicLoading.show({
        template: self.LanguageService ? self.LanguageService.getLabel("ATTENDERE") : 'Attendere'
    });

    self.rootScope.closeMainMenu();
    self.scope = self.rootScope.$new(true);
    self.scope.currentIndex = 0;

    self.scope.destroyView = function() {
        self.savedRecipesView.remove();
        self.scope.$destroy();
        delete self.scope;
    };


    

    self.scope.hideZoom = function() {
    	if(self.hideZoomEnabled == true) {
    		self.zoomed.remove();        
    	}        
    };


    self.scope.showWithZoom = function() {

		self.hideZoomEnabled = false;         


        self.ionicLoading.show({
            template: self.LanguageService ? self.LanguageService.getLabel("ATTENDERE") : 'Attendere'
        });

        var localPath = self.currentRivistaDescriptor[ self.scope.currentIndex ].localPath;       


        self.FileSystemService.getFileContentAsBase64({
            directory: self.SAVED_ITEMS_DIRECTORY,
            fileName: localPath,
            onComplete: function(err, base64Data) {

                self.ionicLoading.hide();

                if(!err) {

                    self.zoomed = self.ionicModal.fromTemplate(

                        '<ion-modal-view ng-click="hideZoom()" padding="true" cache-view="false" style="background-color:black; width:100%; height:100%; left:0px; right:0px; top:0px;">' +
                                                                                     
                            '<ion-scroll style="position: absolute; top:0px; right:0px; left:0px; bottom:0px;" zooming="true" direction="xy" delegate-handle="zoom-pane"  class="zoom-pane"  min-zoom="1"  scrollbar-x="false"  scrollbar-y="false"  overflow-scroll="false">' +
                                '<img id="zoomed_image" src="' + base64Data + '" >' +
                            '</ion-scroll>   ' +
                             
                        '</ion-modal-view>', {

                        scope: self.scope,
                        animation :'none',
                        hardwareBackButtonClose: false

                    });

                    // disabilito il botton di chiusura per 500ms a partire dallo show dell'immagine
                    self.zoomed.show().then(function() {
                    	setTimeout(function() {
                    		self.hideZoomEnabled = true;
                    	}, 500);
                    });

                } 

            }

        });


    }





    self.scope.deleteRecipe = function() {

        var confirmPopup = self.ionicPopup.confirm({
           title: (self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attenzione') +'<i class="ion-android-alert"></i>',
           subTitle: self.LanguageService ? self.LanguageService.getLabel("ELIMINARE_ELEMENTO") : 'Eliminare questo elemento?',
           template: self.LanguageService ? self.LanguageService.getLabel("AGGIUNGERE_NUOVAMENTE_ELEMENTO") : 'Potrai sempre aggiungerlo nuovamente usando la telecamera',
           okText: self.LanguageService ? self.LanguageService.getLabel("RIMUOVI") : 'Rimuovi',
           okType: 'button-assertive',
           cancelText: self.LanguageService ? self.LanguageService.getLabel("ANNULLA") : 'Annulla',
           cancelType: 'button-stable'
       });

       confirmPopup.then(function(res) {

         if(res) {

             var noRivista = self.GlobalVariables.application.currentProgetto.idProgetto;
             var gKey = self.KEY + noRivista;
             var fileName = self.scope.recipes[self.scope.currentIndex].localPath;

             self.FileSystemService.fileDelete({
                directory: self.SAVED_ITEMS_DIRECTORY,
                fileName: fileName,
                onComplete: function(err) {

                    // ignoro un eventuale errore
                    self.currentRivistaDescriptor.splice( self.scope.currentIndex, 1 );
                    self.scope.recipes.splice( self.scope.currentIndex, 1 );
                    self.globalSavedRecipesDescriptor[gKey] = self.currentRivistaDescriptor.length;

                    self.updateCurrentRivistaDescriptor(function(err) {

                        if(!err) {

                            self.updateGlobalDescriptor(function(err) {

                               self.scope.$apply(true);

                                //è cambiato il numero di elementi, sicuramente... rimuovo l'item e lo rimetto col numero giusto
                                if(self.scope.recipes.length > 0)
                                {
                                    self.rootScope.removeItemFromMenu('map_read_saved_items_trentino');
                                    self.rootScope.addItemToMenu('map_read_saved_items_trentino', self.scope.recipes.length + " " + (self.LanguageService ? self.LanguageService.getLabel("ELEMENTI_SALVATI") : 'Elementi Salvati'), 'ion-social-buffer', function() {
                                         self.showPersistedRecipes();
                                    });

                                }
                                else
                                //if(self.scope.recipes.length == 0)
                                {
                                   self.scope.destroyView();
                                   self.rootScope.removeItemFromMenu('map_read_saved_items_trentino');
                                }

                            });
                        }

                    });
                }
             });
           }
       });

    };


    self.scope.options = {
        loop: false,
        effect: 'slide',
        speed: 200,
    }

    self.scope.recipes = [];

    for(var j in self.currentRivistaDescriptor) {
    
        self.scope.recipes.push({
          descr: self.currentRivistaDescriptor[j].descr,
          localPath: self.currentRivistaDescriptor[j].localPath          
        });

    }



    self.savedRecipesView = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" style="background-color:rgba(0,0,0,0.5); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
            '<ion-content scroll="false" padding="false" style="background-image: url(img/background_grey.jpg);">' +

                '<a ng-click="deleteRecipe()" style="position:absolute; top: 4px; left: 5px; z-index:99999;" class="button button-icon icon ion-trash-a activated"></a>' +

                '<a ng-click="destroyView()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +

                '<ion-slides  options="options" slider="data.slider">' +

                    //slides +
                    '<ion-slide-page ng-repeat="recipe in recipes">' +

                        '<ion-slide-page>'+
                                                        
                            '<div ng-click="showWithZoom()" id="recipe_{{$index}}" class="box" style="background-repeat: no-repeat; background-position: center center; background-size:contain; background-image:url(img/imgLoader.gif); height:100%; text-align:center;">' +                                
                                '<i style="margin:0 auto; color:rgba(255,255,255,0.8) !important; position: relative; top: 45%; transform: translateY(-45%);font-size:6em; color:white; z-index:9999;" class="ion-search"></i>' + 
                                '<div style="position: absolute; bottom:0px; left:0px; right:0px; font-size:1.0em; background: linear-gradient(to bottom, rgba(229,229,229,0) 0%,rgba(255,255,255,1) 100%); overflow:hidden; text-overflow: ellipsis; padding: 0px 5px 30px 5px; letter-spacing: -1px;">{{recipe.descr}}</div>' +
                            '</div>' +
                            
                        '</ion-slide-page>' +

                    '</ion-slide-page>' +

                '</ion-slides>' +

            '</ion-content>' +
        '</ion-modal-view>', {

        scope: self.scope,
        focusFirstInput: true,
        animation :'none',
        hardwareBackButtonClose: false

    });

    self.savedRecipesView.show();

    
    self.startProcessLocalFileQueue({
      onComplete: function() {
        self.ionicLoading.hide();
      }
    });
    


    self.scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
        self.scope.currentIndex = data.slider.activeIndex;
    });



};
