

var client_game_find_multiple_images_mondadori_fr = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.FileSystemService = null;
    this.scope = null;
    this.gameCenter = null;
    this.rootScope = null;
    this.WikiService = null;
    this.LanguageService = null;

    this.hasPendingRequest = false;

    this.isInArMode = false;

    this.currentGame = null;

    this.currentAlertBox = null;
    
    // scope dedicato alla vista che indica l'indizio
    this.clue_scope = null;

    this.stats = null;

    this.config = {
        
        name: 'Trouver toutes les images',
        description: 'Retrouvez toutes les images avant votre adversaire',
        key: 'mediasoft.multiple.image.find.mondadorifr',
        short_key: 'MMIFMONF0',
        time_to_live: 60000,

        icon: 'ion-wineglass',
        second_icon: 'ion-person-stalker',
        color: 'red',
        index: 1,

        parameters: {
            background: {
                type: 'img',
                description: 'Immagine di sfondo usata nella lista dei giochi'
            },
            advice: {
                type: 'text',
                description: 'Descrizione comune'
            },
            totale_immagini: {
                type: 'number',
                description: 'Totale delle immagini da trovare'
            },
            countdown_before_start: {
                type: 'number',
                description: 'Tempo prima di far partire il gioco'
            },
            max_time_game: {
                type: 'number',
                description: 'Tempo limite per trovare le immagini'
            },
            lista_immagine_da_trovare: {
                type: 'wtc_image_list',
                description: 'lista di immagini da usare per il riconoscimento'
            }
        },

        values: null

    };

}


client_game_find_multiple_images_mondadori_fr.Instance = null;



client_game_find_multiple_images_mondadori_fr.Initialize = function(params) {

    if(client_game_find_multiple_images_mondadori_fr.Instance != null) {
        return;
    }    

    client_game_find_multiple_images_mondadori_fr.Instance = new client_game_find_multiple_images_mondadori_fr();
    client_game_find_multiple_images_mondadori_fr.Instance.ionicModal = params.ionicModal;
    client_game_find_multiple_images_mondadori_fr.Instance.ionicPopup = params.ionicPopup;
    client_game_find_multiple_images_mondadori_fr.Instance.HelperService = params.HelperService;
    client_game_find_multiple_images_mondadori_fr.Instance.http = params.http;
    client_game_find_multiple_images_mondadori_fr.Instance.GlobalVariables = params.GlobalVariables;
    client_game_find_multiple_images_mondadori_fr.Instance.LoggerService = params.LoggerService;
    client_game_find_multiple_images_mondadori_fr.Instance.FileSystemService = params.FileSystemService;
    client_game_find_multiple_images_mondadori_fr.Instance.SocketService = params.SocketService;
    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter = params.GameCenter;
    client_game_find_multiple_images_mondadori_fr.Instance.rootScope = params.RootScope;
    client_game_find_multiple_images_mondadori_fr.Instance.WikiService = params.WikiService;
    client_game_find_multiple_images_mondadori_fr.Instance.LanguageService = params.LanguageService;
    
    var SEED = client_game_find_multiple_images_mondadori_fr.Instance.config.short_key;

    //traduzioni
    client_game_find_multiple_images_mondadori_fr.Instance.config.name = client_game_find_multiple_images_mondadori_fr.Instance.LanguageService ? client_game_find_multiple_images_mondadori_fr.Instance.LanguageService.getLabel("TROVA_TUTTE_IMMAGINI") : 'Trouver toutes les images';    
    client_game_find_multiple_images_mondadori_fr.Instance.config.description = client_game_find_multiple_images_mondadori_fr.Instance.LanguageService ? client_game_find_multiple_images_mondadori_fr.Instance.LanguageService.getLabel("TROVA_TUTTE_IMMAGINI_PRIMA_AVVERSARIO") : 'Retrouvez toutes les images avant votre adversaire';    


    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.loadStats({
        key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
        title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
        onComplete: function(err, data) { 
        }
    });
    
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', function() {
        
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.unregisterGame({
            key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
            title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
            description: client_game_find_multiple_images_mondadori_fr.Instance.description
        });
       
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite',                      SEED + 'onGameInvite', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite_ack',                  SEED + 'onGameInviteAck', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite_timeout',              SEED + 'onGameInviteTimeout', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_message',                     SEED + 'onGameMsg', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite_inviter_accept',       SEED + 'onInviterAccept', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_invite_invited_accept',       SEED + 'onInvitedAccept', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_win_by_forfait',              SEED + 'onGameWinByForfait', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_victory',                     SEED + 'onGameVictory', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key,     'game_lost',                        SEED + 'onGameLost', null);

        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setDeviceEventCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'pause',                            SEED + 'onGamePaused', null);
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setDeviceEventCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'resume',                           SEED + 'onGameResumed', null);

        client_game_find_multiple_images_mondadori_fr.Instance.WikiService.registerCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, SEED + 'respondToMessageFromWiki', null);
        
    });



    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', function() {        

        client_game_find_multiple_images_mondadori_fr.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', null);
        client_game_find_multiple_images_mondadori_fr.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', null);
        client_game_find_multiple_images_mondadori_fr.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', null);
        
        client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.removeGameStat(client_game_find_multiple_images_mondadori_fr.Instance.config.key);
        client_game_find_multiple_images_mondadori_fr.Instance = null;    

    });


    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', function() {

 
            client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.getConfiguration({
                key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                onComplete: function(err, configuration) {                     
                    
                    if(!err && configuration.values && configuration.visible == true) {                         

                        if(configuration.forDebugOnly == false || (configuration.forDebugOnly == true && client_game_find_multiple_images_mondadori_fr.Instance.GlobalVariables.isDebugDevice == true)) {

                            client_game_find_multiple_images_mondadori_fr.Instance.config.values = [];

                            var image_list_parameter = _.find(configuration.values, function(item) { return item.name == 'lista_immagine_da_trovare' });                
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['lista_immagine_da_trovare'] = JSON.parse(image_list_parameter.value);   

                            var totale_immagini = _.find(configuration.values, function(item) { return item.name == 'totale_immagini' });   
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['totale_immagini'] =  totale_immagini.value;     

                            var countdown_before_start = _.find(configuration.values, function(item) { return item.name == 'countdown_before_start' });   
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['countdown_before_start'] =  countdown_before_start.value;     

                            var max_time_game = _.find(configuration.values, function(item) { return item.name == 'max_time_game' });   
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['max_time_game'] =  max_time_game.value;     

                            var advice = _.find(configuration.values, function(item) { return item.name == 'advice' });   
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['advice'] =  advice.value;    

                            var background = _.find(configuration.values, function(item) { return item.name == 'background' });      
                            background = background.value ? (JSON.parse(background.value)).path : "";
                            client_game_find_multiple_images_mondadori_fr.Instance.config.values['background'] = client_game_find_multiple_images_mondadori_fr.Instance.GlobalVariables.baseUrl + "/" + background;

                            try {

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.registerGame({
                                    key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                                    icon: client_game_find_multiple_images_mondadori_fr.Instance.config.icon,
                                    second_icon: client_game_find_multiple_images_mondadori_fr.Instance.config.second_icon,
                                    color: client_game_find_multiple_images_mondadori_fr.Instance.config.color,
                                    index: client_game_find_multiple_images_mondadori_fr.Instance.config.index,
                                    title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
                                    description: client_game_find_multiple_images_mondadori_fr.Instance.config.description,
                                    classInstance: client_game_find_multiple_images_mondadori_fr.Instance,
                                    background: client_game_find_multiple_images_mondadori_fr.Instance.config.values['background'],
                                    onClickFunctionName: 'startMatchMaking'
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite', SEED + 'onGameInvite', function(message) {
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequest(message);
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite_ack', SEED + 'onGameInviteAck', function(message) {
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequestAck(message);
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite_timeout', SEED + 'onGameInviteTimeout', function(message) {            
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequestTimeout(message);
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', function(message) {            
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequestExpiredOrCancelled(message);
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_message', SEED + 'onGameMsg', function(message) {                                      
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameMessage(message);
                                });
                                
                                // ############################################################################ // 
                                // ricezione di un messaggio che indica che un utente ha accettato la MIA sfida //
                                // ############################################################################ //
                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite_inviter_accept', SEED + 'onInviterAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.alterStat({
                                        key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                                        title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequestInviterAccept(message);
                                });

                                // ######################################################################################################## //
                                // ricezione di un messaggio che indica che ho correttamente accettato la sfida lanciata da un ALTRO utente //
                                // ######################################################################################################## //
                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_invite_invited_accept', SEED + 'onInvitedAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.alterStat({
                                        key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                                        title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameRequestInvitedAccept(message);
                                });


                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_win_by_forfait', SEED + 'onGameWinByForfait', function(message) {     
                                    
                                    // incrementa le partite giocate  
                                    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.alterStat({
                                        key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                                        title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: true
                                    });
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_victory', SEED + 'onGameVictory', function(message) {         
                                    
                                    // incrementa le partite giocate  
                                    client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.alterStat({
                                        key: client_game_find_multiple_images_mondadori_fr.Instance.config.key,
                                        title: client_game_find_multiple_images_mondadori_fr.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: false
                                    });
                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setMessageCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'game_lost', SEED + 'onGameLost', function(message) {            
                                    client_game_find_multiple_images_mondadori_fr.Instance.onReceiveGameEnding({
                                        isWon: false,
                                        isForfait: false
                                    });
                                });


                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setDeviceEventCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'pause', SEED + 'onGamePaused', function(message) {            
                                    
                                    client_game_find_multiple_images_mondadori_fr.Instance.onGamePause();

                                });

                                client_game_find_multiple_images_mondadori_fr.Instance.gameCenter.setDeviceEventCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, 'resume', SEED + 'onGameResumed', function(message) {    
                                    
                                    client_game_find_multiple_images_mondadori_fr.Instance.onGameResume();
                                    
                                });
                                
                                // ricezione eventi dall'ambiente wikitude dedicato ai giochi collaborativi
                                client_game_find_multiple_images_mondadori_fr.Instance.WikiService.registerCallback(client_game_find_multiple_images_mondadori_fr.Instance.config.key, SEED + 'respondToMessageFromWiki', function(message) {
                                    
                                    if(!(message.type && message.key)) {
                                        return;
                                    }

                                    if(message.type == 'startRA') {

                                        client_game_find_multiple_images_mondadori_fr.Instance.userHasStartedAR(message);

                                    } else if(message.type == 'AR_Forfait') {

                                        client_game_find_multiple_images_mondadori_fr.Instance.WikiService.sendMessageToAR({
                                            type: 'closeAR'
                                        });

                                        client_game_find_multiple_images_mondadori_fr.Instance.doForfaitGame();
                                    
                                    } else if(message.type == 'AR_Image_Tracked') {
                                    
                                        client_game_find_multiple_images_mondadori_fr.Instance.onImageTracked(message.msg_body);
                                    
                                    } else if(message.type == 'AR_Countdown_finish') {

                                        //devo uscire
                                        client_game_find_multiple_images_mondadori_fr.Instance.onCountdownFinish(message.msg_body);
                                    }

                                });
                                
                            
                            } catch(err) {

                                alert(err);
                            
                            }

                        }
                        
                    }                 
                }                
            });

        
                    
    });
            


}





client_game_find_multiple_images_mondadori_fr.prototype.onImageTracked = function(message) {

    var self = this;

    var matched_image = _.find(client_game_find_multiple_images_mondadori_fr.Instance.config.values['lista_immagine_da_trovare'].listaImmagini, function(image) {
        return (message.target == image.targetName);
    });
  
	if (!matched_image)
	{
		matched_image = _.find(client_game_find_multiple_images_mondadori_fr.Instance.config.values['lista_immagine_da_trovare'].listaImmagini, function(image) {
			return (message.target.name == image.targetName);
		});
	}

    if(matched_image) {



        // ################################################ //
        // ho trovato una delle immagini previste dal gioco //
        // ################################################ //
        if(self.currentGame.game_data.choosed_image_indexes.indexOf(matched_image.indice) >= 0) {

            if(self.currentGame.game_data.found_image_targets.indexOf(matched_image.targetName) == -1) {

                // ########################################################################### //
                // ho riconosciuto una delle immagini sorteggiate dal gioco per la prima volta //
                // ########################################################################### //
                self.currentGame.game_data.found_image_targets.push(matched_image.targetName);

                if(self.currentGame.game_data.found_image_targets.length == self.config.values['totale_immagini']) {

                    // ############################# //
                    // ho trovato tutte le immagini! //
                    // ############################# //

                    self.WikiService.sendMessageToAR({
                        type: 'closeAR'
                    });

                    self.isInArMode = false;    
                     
                    self.currentAlertBox = self.ionicPopup.show({
                        title: self.LanguageService ? self.LanguageService.getLabel("COMPLIMENTI_TROVATO_TUTTE_IMMAGINI") : 'Félicitations, vous avez trouvé toutes les images!',
                        template:
                            '<div>' + (self.LanguageService ? self.LanguageService.getLabelParam("ATTENDI_PER_SAPERE_SE_HAI_BATTUTO",[self.currentGame.user]) : 'Attendez un moment pour savoir si vous avez battu <strong>' + self.currentGame.user + '</strong>') + '</div>' +
                            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'
                    });

                    // ho trovato tutte le immagini. concludo la partita //
                    self.SocketService.emit('message', 'all_images_found', {}, {     
                        key: self.config.key,                   
                        uid: self.GlobalVariables.deviceUUID,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                        match_id: self.currentGame.match_id
                    });
                
                } else {

                    self.WikiService.sendMessageToAR({
                        type: 'showAlert',
                        msg_body: {
                            messaggio: self.LanguageService ? self.LanguageService.getLabelParam("HAI_TROVATO_IMMAGINI_SU",[self.currentGame.game_data.found_image_targets.length,self.config.values['totale_immagini']]) : 'Vous avez trouvé ' + self.currentGame.game_data.found_image_targets.length + ' images sur ' + self.config.values['totale_immagini'],
                            duration: 4000
                        }
                    });

                    self.SocketService.emit('message', 'user_update_game_progress', {}, {     
                        key: self.config.key,                   
                        uid: self.GlobalVariables.deviceUUID,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                        match_id: self.currentGame.match_id,
                        game_progress: {
                            found_images_count: self.currentGame.game_data.found_image_targets.length
                        }
                    });
                }


            } else {

                // ######################################################################################################### //
                // ho riconosciuto una delle immagini sorteggiate dal gioco, ma questa era stata già precedentemente trovata //
                // ######################################################################################################### //
                self.WikiService.sendMessageToAR({
                    type: 'showAlert',
                    msg_body: {
                        messaggio: self.LanguageService ? self.LanguageService.getLabel("HAI_GIA_TROVATO_IMMAGINE") : 'Vous avez déjà trouvé cette image',
                        duration: 4000
                    }
                });

            }            
            
        } else {

            self.WikiService.sendMessageToAR({
                type: 'showAlert',
                msg_body: {
                    messaggio: self.LanguageService ? self.LanguageService.getLabel("NON_E_IMMAGINE_GIUSTA") : 'Ce n\'est pas la bonne image',
                    duration: 4000
                }
            });

        }

    }


}

client_game_find_multiple_images_mondadori_fr.prototype.onCountdownFinish = function(message) {

    var self = this;

    // ############################# //
    // Tempo finito                  //
    // ############################# //

    self.WikiService.sendMessageToAR({
        type: 'closeAR'
    });

    self.isInArMode = false;    
     
    self.currentAlertBox = self.ionicPopup.show({
        title: self.LanguageService ? self.LanguageService.getLabel("TEMPO_SCADUTO") : 'Tempo Concluso',
        template:
            '<div>' + (self.LanguageService ? self.LanguageService.getLabelParam("ATTENDI_PER_SAPERE_SE_HAI_BATTUTO",[self.currentGame.user]) : 'Attendi un istante per sapere se hai battuto <strong>' + self.currentGame.user + '</strong>') + '</div>' +
            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'
    });

    // ho trovato tutte le immagini. concludo la partita //
    self.SocketService.emit('message', 'time_finish', {}, {     
        key: self.config.key,                   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
        match_id: self.currentGame.match_id
    });




}


client_game_find_multiple_images_mondadori_fr.prototype.userHasStartedAR = function() {

    var self = this;     

    if(self.currentAlertBox) {
        self.currentAlertBox.close();
        self.currentAlertBox = null;    
    }

    if(self.currentGame) {    
        
        self.currentGame.game_data.game_start_time = (new Date()).getTime();

        self.isInArMode = true;

        
        /*
        self.WikiService.sendMessageToAR({
            type: 'showTimer'
        });
        */
        
        setTimeout(function() {
            
            self.WikiService.sendMessageToAR({
                type: 'showAlert',
                msg_body: {
                    messaggio: self.LanguageService ? self.LanguageService.getLabel("TROVA_LE_IMMAGINI_PER_PRIMO") : "Soyez le premier à trouver les images",
                    duration: 4000
                }
            });

        }, 500);     
        
        
        setTimeout(function() {
            
            self.WikiService.sendMessageToAR({
                type: 'showCountdown',
                msg_body: {
                    time: self.config.values['max_time_game']
                }
            });

        }, 1000);     
       
    
    } else {  

        // al termine dell'avvio della realtà aumentata potrei aver già vinto per abbandono
        // in tal caso self.currentGame è nullo.
        client_game_find_multiple_images_mondadori_fr.Instance.WikiService.sendMessageToAR({
            type: 'closeAR'
        });

        self.isInArMode = false;

    }

    
};




client_game_find_multiple_images_mondadori_fr.prototype.generateShuffle = function(SIZE) {

    var self = this;
    var indexes = [];
    var sorted = [];

    for(var j=0; j<self.config.values['lista_immagine_da_trovare'].listaImmagini.length; j++) {
        indexes.push(self.config.values['lista_immagine_da_trovare'].listaImmagini[j].indice);
    }

    for (var a = indexes, i = a.length; i--; ) {
        sorted.push(a.splice(Math.floor(Math.random() * (i + 1)), 1)[0]);
    }

    return sorted.slice(0, SIZE);

};




// ################################### //
// avvio ricerca giocatori per giocare //
// ################################### //
client_game_find_multiple_images_mondadori_fr.prototype.startMatchMaking = function() {

    var self = this;
    self.isInArMode = false;

    if(self.clue_scope && self.clue_scope.timer) clearInterval(self.clue_scope.timer); //per sicurezza

    if(!self.HelperService.isNetworkAvailable()) {

        self.ionicPopup.alert({
            title: (self.LanguageService ? self.LanguageService.getLabel("ATTENZIONE") : 'Attention') +' <i class="ion-android-alert"></i>',
            template: self.LanguageService ? self.LanguageService.getLabel("NON_SEI_CONNESSO") : 'Vous n\'êtes actuellement connecté à Internet'
        });

        return;
    }

    self.gameCenter.getNickname({
        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            var choosed_image_indexes = self.generateShuffle(parseInt(self.config.values['totale_immagini']));

            self.SocketService.emit('message', 'start_match_make', {}, {     
                key: self.config.key,   
                user: nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                game_data: {
                    choosed_image_indexes: choosed_image_indexes
                }
            });

        }
    });

};



// ######################### //
// ricezione invito gi gioco //
// ######################### //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequest = function(message) {

    var self = this;

    self.gameCenter.appendRequest({
        level: 'progetto',
        match_id: message.match_id,
        title: self.config.name,
        key: self.config.key,
        time_to_live: self.config.time_to_live,
        classInstance: self,
        onClickFunctionName: 'acceptInvite',
        from: {
            user: message.user,
            uid: message.uid
        }
        
    });

};


// ########################################## //
// accettazione di una sfida previo richiesta //
// ########################################## //
client_game_find_multiple_images_mondadori_fr.prototype.acceptInvite = function(request) {

    var self = this;
    self.isInArMode = false;

    self.gameCenter.getNickname({

        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            self.askGameReq = self.ionicPopup.confirm({
                title: self.LanguageService ? self.LanguageService.getLabelParam("ACCETTI_LA_SFIDA_DA",[request.from.user]) : 'Acceptez-vous le défi de <strong>' + request.from.user + '</strong> ?',
                template: self.LanguageService ? self.LanguageService.getLabelParam("PREMI_SI_PER_GIOCARE_CONTRO",[self.config.name,request.from.user]) : 'Appuyez sur "Oui" pour commencer à jouer ' + self.config.name + ' avec ' + request.from.user,
                cancelText: self.LanguageService ? self.LanguageService.getLabel("NO") : 'Non', 
                cancelType: 'button-stable',
                okText: self.LanguageService ? self.LanguageService.getLabel("SI") : 'Oui', 
                okType: 'button-balanced'
            });

            self.askGameReq.then(function(res) {

                if(res) {
                    
                    self.gameCenter.removeRequest(request);
                    
                    self.SocketService.emit('message', 'accept_match_make', {}, {    
                        match_id: request.match_id, 
                        key: self.config.key,   
                        user: nickname,
                        uid: self.GlobalVariables.deviceUUID,
                        from: request.from,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto
                    });                    

                }  

            });

        }

    });


};



// ############################################################################ //
// il server notifica che dopo un tot tempo la mia richiesta di gioco è scaduta //
// ############################################################################ //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequestTimeout = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.ionicPopup.alert({
        title: self.LanguageService ? self.LanguageService.getLabel("NESSUN_UTENTE_ACCETTATO_SFIDA") : 'Aucun utilisateur n\'a accepté votre défi',              
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};  




// ##################################################### //
// un utente ha raccolto la sfida che IO ho lanciato ... //
// ##################################################### //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequestInviterAccept = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.gameCenter.closeView();

    //alert('sfida raccolta da: ' + JSON.stringify(message));

    self.showClue(message);

}; 




// ######################################################## //
// vengo notificato per aver accettato la sfida di un altro //
// ######################################################## //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequestInvitedAccept = function(message) {

    var self = this;
    self.gameCenter.closeView();
    self.showClue(message);

}; 


// ###################################################### //
// abbandono il gioco esplicitamente vince l'altro utente //
// ###################################################### //
client_game_find_multiple_images_mondadori_fr.prototype.doForfaitGame = function() {

    var self = this;
    
    self.SocketService.emit('message', 'forfait_game', {}, {  

        match_id: self.currentGame.match_id,  
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.GlobalVariables.application.currentProgetto.idProgetto

    });
    

    self.assertEnding({
        isWon: false,
        isForfait: true
    });

    
}; 


// ############################### //
// ricevo una notifica di vittoria //
// ############################### //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameEnding = function(params) {

    var self = this;  

    try {

        // #################################################################################################### //
        // ricevo una notifica di vittoria mentre sono ancora nella schermata di presentazione dell'indizio.... //
        // #################################################################################################### //
        if(self.clue_scope && self.clue_scope.clueWnd) {
            self.hideClue(function() {
        
                self.assertEnding({
                    isWon: params.isWon, 
                    isForfait: params.isForfait
                });
            
            });
        }


        // ###################################################### //
        // ricevo una notifica mentre sono nella realtà aumentata //
        // ###################################################### //
        if(self.isInArMode == true) {

            self.isInArMode = false;
            
            client_game_find_multiple_images_mondadori_fr.Instance.WikiService.sendMessageToAR({
                type: 'closeAR'
            });

            self.assertEnding({
                isWon: params.isWon, 
                isForfait: params.isForfait
            });

        }


        // ###################################################################### //
        // ricevo la notifica di vittoria DOPO aver trovato l'immagine e          //  
        // sono in attesa di sapere se ho impiegato meno tempo del mio avversario //
        // ###################################################################### //
        if(self.currentAlertBox) {
             
            self.currentAlertBox.close();
            self.currentAlertBox = null;

            self.assertEnding({
                isWon: params.isWon, 
                isForfait: params.isForfait
            });
            
        }


    } catch(err) {

        alert(err);
    
    }
    
};



client_game_find_multiple_images_mondadori_fr.prototype.assertEnding = function( params ) {

    var self = this;
   
    var title = '';
    var template = '';
    var button = '';

    if(params.isWon) {

        title = self.LanguageService ? self.LanguageService.getLabel("COMPLIMENTI_HAI_VINTO") : 'Félicitations: vous avez gagné';

        if (params.isForfait)
        {
            template = self.LanguageService ? self.LanguageService.getLabelParam("SEMBRA_PROPRIO_CHE_ABBIA_ABBANDONATO_SFIDA",[self.currentGame.user]) : 'Il semble que <strong>' + self.currentGame.user + '</strong> ait abandonné le défi!';
        }
        else
        {
            template = (self.LanguageService ? self.LanguageService.getLabel("HAI_BATTUTO") : 'Vous avez battu') + ' <strong>' + self.currentGame.user + '</strong>. ' + (self.LanguageService ? self.LanguageService.getLabel("HAI_IMPIEGATO_MENO_TEMPO_INDIZIO") : 'Vous avez pris moins de temps pour trouver l\'indice!');
        }
        button = 'button-balanced' ;

    } else {

        title = self.LanguageService ? self.LanguageService.getLabel("PURTROPPO_HAI_PERSO") : 'Malheureusement, vous avez perdu';

        if (params.isForfait)
        {
            template = self.LanguageService ? self.LanguageService.getLabelParam("HAI_CEDUTO_LA_VITTORIA",[self.currentGame.user]) : 'Vous avez rendu la victoire à <strong>' +  self.currentGame.user + '</strong> . La prochaine fois sera mieux!';
        }
        else
        {
            template = (self.LanguageService ? self.LanguageService.getLabelParam("SEI_STATO_SCONFITTO_DA",[self.currentGame.user]) : 'Vous avez été vaincu par <strong>' + self.currentGame.user + '</strong>') + (self.LanguageService ? self.LanguageService.getLabel("HA_IMPIEGATO_MENO_TEMPO_INDIZIO") : 'Il a fallu moins de temps pour trouver l\'indice');
        }
        button = 'button-assertive' ;
    }

       
    self.ionicPopup.alert({
        title: title,              
        template: template,
        okText: 'Ok', 
        okType: button        
    });

    self.currentGame = null;

}



client_game_find_multiple_images_mondadori_fr.prototype.hideClue = function(onClose) {

    var self = this;

    if(self.clue_scope.clueWnd) {

        if(self.clue_scope && self.clue_scope.timer)
        {
            clearInterval(self.clue_scope.timer); //per sicurezza
        }        

        self.clue_scope.clueWnd.remove().then(function() {
            onClose();
        });        

        self.clue_scope.clueWnd = null;
        self.clue_scope.$destroy();
        delete self.clue_scope;

    }
    
}


client_game_find_multiple_images_mondadori_fr.prototype.startGame = function() {

    var self = this;
    
    self.SocketService.emit('message', 'start_game', {}, {     
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.currentGame.identifier,
        match_id: self.currentGame.match_id
    });

    var WTC_PATH = self.config.values['lista_immagine_da_trovare'].wtcPath;
 
    var json = {
        wtc: WTC_PATH,   
        labels: {
            abbandona: self.LanguageService ? self.LanguageService.getLabel("ABBANDONA_LA_PARTITA") : 'Abbandona la partita...',
            conferma_abbandona: self.LanguageService ? self.LanguageService.getLabel("SEI_SICURO_VOLER_ABBANDONARE") : 'Sei sicuro di voler abbandonare e dare la vittoria al tuo avversario?',
            conferma_abbandona_si: self.LanguageService ? self.LanguageService.getLabel("SI") : 'Si',
            conferma_abbandona_no: self.LanguageService ? self.LanguageService.getLabel("NO") : 'No',
        } 
    }

    self.currentAlertBox = self.ionicPopup.show({
        title: '<div>' + (self.LanguageService ? self.LanguageService.getLabel("AVVIO_REALTA_AUMENTATA") : 'La réalité augmentée commence') + '</div>',
        template: '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'        
    });

    self.WikiService.openArForGame(self.config.key, json);
    
}


// il momento in cui mostro l'indizio è anche il momento in cui ho instanziato una sfida
// a questo punto il contatore delle partite giocate aumenta
client_game_find_multiple_images_mondadori_fr.prototype.showClue = function(message) {

    var self = this;        
    self.clue_scope = self.rootScope.$new(true);
    self.clue_scope.clueWnd = null;

    var clues = '';

    for(var j in message.game_data.choosed_image_indexes) {

        var image = self.config.values['lista_immagine_da_trovare'].listaImmagini[message.game_data.choosed_image_indexes[j]];
        if (image.indizio != "") clues += '<li>' + image.indizio + '</li>';

    }

    var image = self.config.values['lista_immagine_da_trovare'].listaImmagini[0];

    self.currentGame = message;
    self.currentGame.game_data.found_image_targets = [];

    self.clue_scope.time = self.config.values['countdown_before_start'];
    self.clue_scope.timer = null;

    self.clue_scope.startGame = function() {

        clearInterval(self.clue_scope.timer);
        self.hideClue(function() {
            self.startGame();    
        });
        
    };

    self.clue_scope.forfaitGame = function() {

        clearInterval(self.clue_scope.timer);
        self.hideClue(function() {
            self.doForfaitGame();    
        });
        
    };

    self.clue_scope.clueWnd = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" class="game-generic-modal" cache-view="false">' +

            '<ion-content scroll="true" padding="false" style="bottom:70px;">' +

                '<div class="game-generic-inner">' +
                    
                    '<div><h2>' + self.config.name + '</h2></div>' +
                    '<div><h3>' + self.config.description + '</h3></div>' +

                    '<div><h2>'+(self.LanguageService ? self.LanguageService.getLabel("IL_TUO_AVVERSARIO_E") : 'Votre adversaire est')+'</h2></div>' +
                    '<div><h3>' + self.currentGame.user + '</h3></div>' +

                    '<div><h2>' + self.config.values['advice'] + '</h2></div>' +
                    
                    '<div><h2>' + (self.LanguageService ? self.LanguageService.getLabelParam("DOVRAI_TROVARE_IMMAGINI_SEGUENTI_INDIZI",[self.config.values['totale_immagini']]) : 'Vous devez trouver ' + self.config.values['totale_immagini'] + ' images indiquées par les indices suivants:') + '</h2></div>' +
                    '<div><ul style="list-style-type: circle; padding: 0px 20px;">' + clues + '</ul></div>' +                        

                '</div>' +

            '</ion-content>' +

            '<ion-footer-bar align-title="left" class="game-generic-footer row">' +
                '<div class="col" style="text-align:center;">' +
                    '<h3 style="line-height: 66px;">'+(self.LanguageService ? self.LanguageService.getLabel("INIZIO_FRA") : 'Commence dans')+' {{time}}</h3>' +
                '</div>' +
                '<div class="col">' +
                    '<button ng-click="forfaitGame()" class="button button-full button-assertive cst-button">' + (self.LanguageService ? self.LanguageService.getLabel("MI_ARRENDO") : 'J\'abandonne...') + '...</button>' +
                '</div>' +
            '</ion-footer-bar>' +

            '</ion-modal-view>',

        {
            scope: self.clue_scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false,
            backdropClickToClose: false
        }

    );

    self.clue_scope.clueWnd.show().then(function() {

        self.clue_scope.timer = setInterval(function() {
             
            self.clue_scope.$apply(function() {

                self.clue_scope.time--;

                if(self.clue_scope.time == 0) {
                    
                    clearInterval(self.clue_scope.timer);
                    self.clue_scope.startGame();
                }

            });

        }, 1000);

    });

}



// ################################################################################## //
// messaggio ricevuto quando provo ad accettare una sfida che nel frattempo è scaduta //
// ################################################################################## //
client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequestExpiredOrCancelled = function(message) {

    var self = this;

    self.ionicPopup.alert({
        title: self.LanguageService ? self.LanguageService.getLabel("TROPPO_TARDI") : 'Trop tard',              
        template: self.LanguageService ? self.LanguageService.getLabelParam("LA_SFIDA_NON_E_PIU_DISPONIBILE",[message.from.user,self.config.name]) : 'Le défi lancé par "' + message.from.user + '" au jeu "' + self.config.name + '" n\'est plus disponible...' ,
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};


client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameRequestAck = function(message) {

    var self = this;

    if(message.status == 'OK') {

        self.waitBox = self.ionicPopup.alert({
          title: self.LanguageService ? self.LanguageService.getLabel("ATTENDI_UTENTE_ACCETTI_SFIDA") : 'Attendez qu\'un utilisateur accepte le défi',
          template: 
            '<div>'+(self.LanguageService ? self.LanguageService.getLabel("PREMERE_ANNULLA_PER_INTERROMPERE") : 'Vous pouvez appuyer sur \'Annuler\' pour arrêter votre demande de jeu')+'<div>' + 
            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>',
          okText: self.LanguageService ? self.LanguageService.getLabel("ANNULLA") : 'Annuler', 
          okType: 'button-assertive'            
        });

        self.hasPendingRequest = true;

        self.waitBox.then(function() {

            self.waitBox = null;
          
            if(self.hasPendingRequest == true) {

                self.hasPendingRequest = false;

                self.SocketService.emit('message', 'cancel_match_make', {}, {     
                    key: message.key,   
                    user: self.rootScope.game_center.nickname,
                    uid: self.GlobalVariables.deviceUUID,
                    identifier: message.identifier
                });
          
            }
          
        });

    } else if(message.status == 'KO') {

        //traduzione messaggio
        var messaggio=message.reason;
        if (message.reason=="Non ci sono utenti disponibili in questo momento")
        {
            messaggio = self.LanguageService ? self.LanguageService.getLabel("NO_USERS_NOW") : message.reason;
        }

        self.waitBox = self.ionicPopup.alert({
            title: self.LanguageService ? self.LanguageService.getLabel("RICHIESTA_GIOCO_RESPINTA") : 'Votre demande de jeu a été rejetée',
            template: messaggio,
            okText: self.LanguageService ? self.LanguageService.getLabel("CHIUDI") : 'Fermer'          
        });

    }

}



client_game_find_multiple_images_mondadori_fr.prototype.onGamePause = function(message) {
 
    var self = this;

    try {

        if(self.waitBox) {

            self.waitBox.close();

            self.SocketService.emit('message', 'cancel_match_make', {}, {     
                key: self.config.key,   
                user: self.rootScope.game_center.nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto
            });

        }

        if((self.clue_scope && self.clue_scope.clueWnd) || self.isInArMode == true) {

            self.SocketService.emit('message', 'forfait_game', {}, {  

                match_id: self.currentGame.match_id,  
                key: self.config.key,   
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto

            });


        }


    } catch(err) {

        alert(err);

    }   
    


}

client_game_find_multiple_images_mondadori_fr.prototype.onGameResume = function(message) {
    
    var self = this;

    try {

        if(self.clue_scope && self.clue_scope.clueWnd) {
            
            self.hideClue(function() { 
                self.assertEnding({
                    isWon: false, 
                    isForfait: true
                });
            });

        }

        if(self.isInArMode == true) {

            self.isInArMode = false;

            setTimeout(function() {
                
                self.WikiService.sendMessageToAR({
                    type: 'closeAR'
                });

                self.assertEnding({
                    isWon: false, 
                    isForfait: true
                });
            

            }, 2000);
                

        }

    } catch(err) {

        alert(err);
    
    }
       
 
}


client_game_find_multiple_images_mondadori_fr.prototype.onReceiveGameMessage = function(message) {

    var self = this;

    if(self.isInArMode == true) {

        self.WikiService.sendMessageToAR({
            type: 'showAlert',
            msg_body: {
                messaggio: self.LanguageService ? self.LanguageService.getLabelParam("AVVERSARIO_HA_TROVATO_IMMAGINI_SU",[self.currentGame.user,message.game_progress.found_images_count,self.config.values['totale_immagini']]) : self.currentGame.user + ' a trouvé ' + message.game_progress.found_images_count + ' images sur ' + self.config.values['totale_immagini'],
                duration: 4000
            }
        });

    }

    
}






