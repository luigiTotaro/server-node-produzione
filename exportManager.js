var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var fs = require('fs');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');
var Path = require('path');


module.exports = function(parameters) {

	var self = this;

	self.start = 0;
	self.limit = 3;
	self.connections = [];
	self.queue = [];

	var today = new Date();
	var giorno = today.getDate();
	var mese = today.getMonth();
	var anno = today.getFullYear();
	var _from = (new Date(anno, mese, giorno, 0, 0, 0, 0)).getTime();
	var _to = (new Date(anno, mese, giorno, 23, 59, 59, 0)).getTime();
	var _string_date = anno + "-" + (mese + 1) + "-" + giorno;

	console.log("COSTRUTTORE EXPORT MANAGER");


	_.each(config.mysql_connections, function(item, label) {		
		self.connections[label] = mysql.createConnection(item);					 		
	});



	_.each(config.appConfigurations, function(item, label) {

		if(item.exports) {

			_.each(item.exports.commands, function(operation) {
	 

				self.queue.push({
					class: operation,
					application_name: label,
					mysql_connection: item.exports.mysql_db
				})
			})

		}
		
	});

	
	self.exporter_classes = [];

	var dir = Path.normalize( __dirname + "/exporter_classes" );

	if(fs.existsSync(dir)) {
	    
	    var files = fs.readdirSync(dir);

	    _.each(files, function(filename) {
	        
	        var fileLibrary = Path.normalize(dir + "/" + filename);
	        var basename = Path.basename(fileLibrary, '.js');
	        eval(fs.readFileSync(fileLibrary, 'utf-8')); 
	        self.exporter_classes[basename] = eval(basename); 
	        
	    });
	  
	}
	
	
	self.fetchFromProcessQueue = function(params) {
		
		if(self.queue.length == 0) {
			
			params.onQueueCompleted();

		} else {


			var operation = self.queue.shift();			
			var _connection = self.connections[operation.mysql_connection];			
			var instance = new self.exporter_classes[operation.class](operation.application_name, _connection, _string_date, _from, _to);
		
			instance.run({

				onComplete: function(err, result) {

					if(err) {
						winston.error("[" + operation.class + "]: " , err);	
					}
					
					self.fetchFromProcessQueue(params);

				}

			});
			

		}

	}


	winston.warn("starting export");

	for(var connectionName in self.connections) {
		self.connections[connectionName].connect();
	}

	self.fetchFromProcessQueue({
		onQueueCompleted: function() {
			
			winston.warn("completed export");
			
			for(var connectionName in self.connections) {
				
				try {
					self.connections[connectionName].end();
				} catch(err) {
					winston.error("erro closing connection " + connectionName, err);
				}
				
			}
		}
	});



};








