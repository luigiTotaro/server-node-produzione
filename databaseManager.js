var MongoClient = require('mongodb').MongoClient;
var config = require('./config');
var _ = require('underscore');
var winston = require('winston');

function DatabaseManager() {};


DatabaseManager.CONNECTION = null;
DatabaseManager.DBS = [];
DatabaseManager.NAMES = [];

DatabaseManager.databaseExists = function(dbName) {
    return DatabaseManager.NAMES.indexOf(dbName) != -1;
};

DatabaseManager.Close = function() {
  DatabaseManager.CONNECTION.close();
}


DatabaseManager.getData = function(dbname, onComplete) {

    if(DatabaseManager.databaseExists(dbname) == false) {
        onComplete("il database '" + dbname + "' non esiste", null);
        return;
    }

    if(!DatabaseManager.DBS[dbname]) {
        DatabaseManager.DBS[dbname] = DatabaseManager.CONNECTION.db(dbname);
    }

    var collezione = DatabaseManager.DBS[dbname].collection('testCollezione');

    collezione.find().toArray(function(err, docs) {

      onComplete(null, docs);

    });

}


DatabaseManager.getTable = function(dbname, table, onComplete) {

    

    if(DatabaseManager.databaseExists(dbname) == false) {
        onComplete("il database '" + dbname + "' non esiste", null);
        return;
    }

    if(!DatabaseManager.DBS[dbname]) {
        DatabaseManager.DBS[dbname] = DatabaseManager.CONNECTION.db(dbname);
    }

    onComplete(null, DatabaseManager.DBS[dbname].collection(table));    

}





DatabaseManager.InitializeConnection = function(onComplete) {

    MongoClient.connect(config.mongodb_url, function(err, db) {

        if(err) {

            if(db) {
                db.close();
            }

            onComplete(err.message);
            return;

        }

        DatabaseManager.CONNECTION = db;

        DatabaseManager.CONNECTION.admin().listDatabases(function(err, dbs) {

            _.each(dbs.databases, function(current_database) {

                DatabaseManager.NAMES.push(current_database.name);

            });

            onComplete(null);

        });

    });

};

module.exports = DatabaseManager;
