var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ##################################################################################### //
// totale accessi applicazione suddivisti per regione con totale utenti e media utilizzo //
// ##################################################################################### //
var AccessiByRegion = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running AccessiByRegion on " + self.application_name);
		
		DatabaseManager.getTable(self.application_name, 'access', function(err, table) {


			if(err) {

				params.onComplete(err, null);
				return;

			}

			table.aggregate([

				{
					$match: {
						last_activity_time: {
							$gt: from,
							$lt: to
						}
					}
				}
				
				, {

					$group: { 
						_id: {
							position: "$position.regione"							
						},
						accessi: { 
							$sum: 1 
						},
						media_fruizione: {
							$avg: "$total_activity_time"
						}
					}
				}

			], function(err, result) {
 				
 				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

 				var values = [];


 				_.each(result, function(row) {

 					var region_name = row['_id'].position ? row['_id'].position.name : "";
 					var region_lat = row['_id'].position ? row['_id'].position.lat : "";
 					var region_lon = row['_id'].position ? row['_id'].position.lon : "";

 					values.push("('" + 
 						self.application_name + "', '" + 
 						string_date + "', " + 
 						connection.escape(region_name) + ", '" + 
 						region_lat + "', '" + 
 						region_lon + "', " + 
 						row['accessi'] + ", " + 
 						Math.round(row['media_fruizione'] / 6000) + 
 					")");
 					 
 					 
 				});
 
 				
 				
				connection.query('INSERT INTO accessi_by_region (testata, data, regione, lat, lon, accessi, media_uso) VALUES ' + values.join(","), function (error) {
		
				  params.onComplete(error, null);

				});
				

			});

             

		});
	
	}

};
