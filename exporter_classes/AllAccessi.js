var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ##################################################################################### //
// totale accessi applicazione suddivisti per os e versione cone tempo medio di utilizzo //
// ##################################################################################### //
var AllAccessi = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running AllAccessi on " + self.application_name);
		
		DatabaseManager.getTable(self.application_name, 'access', function(err, table) {

			if(err) {

				params.onComplete(err, null);
				return;

			}
			
			table.aggregate([

				{
					$match: {
						total_activity_time: {
							$gt: 0
						}
					}
				}

			], function(err, result) {
				
				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

				var values = [];

				_.each(result, function(row) {	
					values.push( 
						"('" + 
							self.application_name + "', " + 
							connection.escape(row.uid) + ", " + 
							connection.escape(row.os) + ", " + 
							connection.escape(row.version) + ", " + 
							connection.escape((row.position != null && row.position.regione.name != null) ? row.position.regione.name : '') + ", " + 
							connection.escape(row.last_activity_time) + ", " + 
							connection.escape(row.total_activity_time) +  
						")"
					);
					
				});
 

				connection.query('INSERT INTO accessi (testata, uuid, os, version, regione, last_activity_time, total_activity_time) VALUES ' + values.join(","), function (error) {
		
					connection.query('DELETE FROM accessi WHERE insert_date < DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 3 MONTH) ', function (error) {
						
						params.onComplete(error, null);
							
					});
				});
				
				

				});
			
			
		});

	
	}

};
