var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ########################## //
// totale accessi per rivista //
// ########################## //
var RivisteViewCounter = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;


	self.run = function(params) {

		console.log("running RivisteViewCounter on " + self.application_name);
		
		DatabaseManager.getTable(self.application_name, 'event', function(err, table) {


			if(err) {
				params.onComplete(err, null);
				return;
			}

			table.aggregate([

				{
					$match: {
						type: 'view_progetto',
						time: {
							$gt: from,
							$lt: to
						}
					}
				}
				
				, {

					$group: { 
						_id: {
							id: "$id",
							nome: "$name",
							descrizione: "$description"
						},
						accessi: { 
							$sum: 1 
						}
					}
				}

			], function(err, result) {

				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

				var values = [];

				_.each(result, function(row) {
					values.push("('" + 
						self.application_name + "', '" + 
						string_date + "', " + 
						row['_id'].id + ", " + 
						connection.escape(row['_id'].nome) + ", " + 
						connection.escape(row['_id'].descrizione) + ", " + 
						row['accessi'] + ")");
				});

				connection.query('INSERT INTO views_by_progetto (testata, data, id_progetto, nome, descrizione, accessi) VALUES ' + values.join(","), function (error) {
				  params.onComplete(error, null);
				});
				

			});

             

		});
	
	}

};