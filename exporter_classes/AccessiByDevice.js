var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ##################################################################################### //
// totale accessi applicazione suddivisti per os e versione cone tempo medio di utilizzo //
// ##################################################################################### //
var AccessiByDevice = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running AccessiByDevice on " + self.application_name);
		
		DatabaseManager.getTable(self.application_name, 'access', function(err, table) {


			if(err) {

				params.onComplete(err, null);
				return;

			}

			table.aggregate([

				{
					$match: {
						last_activity_time: {
							$gt: from,
							$lt: to
						}
					}
				}
				
				, {

					$group: { 
						_id: {
							os: "$os",
							version: "$version"								
						},
						accessi: { 
							$sum: 1 
						},
						media_fruizione: {
							$avg: "$total_activity_time"
						}
					}
				}

			], function(err, result) {

				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

				var values = [];

				_.each(result, function(row) {

					values.push( 
						"('" + 
							self.application_name + "', '" + 
							string_date + "', " + 
							connection.escape(row['_id'].os) + ", " + 
							connection.escape(row['_id'].version) + ", " + 
							row['accessi'] + ", " + 
							Math.round(row['media_fruizione'] / 6000) + 
						")"
					);

				});
 

				connection.query('INSERT INTO use_by_os (testata, data, os, version, accessi, media_uso) VALUES ' + values.join(","), function (error) {
		
				  params.onComplete(error, null);

				});

							

			});

             

		});
	
	}

};
