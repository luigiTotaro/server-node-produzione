var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ####################################### //
// resetto il valore delle attività svolte //
// ####################################### //
var ResetAccessActivityTime = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running ResetAccessActivityTime on " + self.application_name);

		DatabaseManager.getTable(self.application_name, 'access', function(err, table) {

			if(err) {
				params.onComplete(err, null);
				return;
			}

 			table.updateMany({

 			},{

 				$set: {
 					total_activity_time: 0
 				}

 			}, function(err, result) {

 				params.onComplete(err, null);

 			});
            
		});
		
		
	
	}

};

