var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');

// ####################### //
// totale mappe consultate //
// ####################### //
var MappeConsultateViewCounter = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;


	self.run = function(params) {

		console.log("running MappeConsultateViewCounter on " + self.application_name);

		
		DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

			if(err) {
				params.onComplete(err, null);
				return;
			}

			table.count({
				type: 'has_opened_map',
				time: {
					$gt: from,
					$lt: to
				}
			}).then(function(count) {
	

				connection.query('INSERT INTO map_views_by_testata (testata, data, accessi) VALUES ("' + self.application_name + '", "' + string_date + '", ' + count + ')', function (error) {

				  params.onComplete(error, null);

				});

		    });

 
             

		});
	
	}

};