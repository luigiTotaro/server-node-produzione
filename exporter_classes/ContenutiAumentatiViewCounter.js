var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');

// ################################# //
// totale contenuti aumentati aperti //
// ################################# //
var ContenutiAumentatiViewCounter = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;



	self.run = function(params) {

		console.log("running ContenutiAumentatiViewCounter on " + self.application_name);

		
		DatabaseManager.getTable(self.application_name, 'event', function(err, table) {


			if(err) {
				params.onComplete(err, null);
				return;
			}

			table.aggregate([

				{
					$match: {
						type: 'view_augmented_content',
						time: {
							$gt: from,
							$lt: to
						}
					}
				}
				
				, {

					$group: { 
						_id: {
							id: "$id",
							titolo: "$titolo",
							id_progetto: "$id_progetto"
						},
						accessi: { 
							$sum: 1 
						}
					}
				}

			], function(err, result) {

				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

				var values = [];

				_.each(result, function(row) {

					console.log(row);
 
					values.push("('" + 
						self.application_name + "', '" + 
						string_date + "', " + 
						row['_id'].id + ", " + 
						connection.escape(row['_id'].titolo) + ", " + 
						row['_id'].id_progetto + ", " + 
						row['accessi'] + 
					")");
				
				});

				connection.query('INSERT INTO views_by_augmented_content (testata, data, id_contenuto, titolo, id_progetto, accessi) VALUES ' + values.join(","), function (error) {

				  params.onComplete(error, null);

				});
				
				

			});

             

		});
	
	}

};