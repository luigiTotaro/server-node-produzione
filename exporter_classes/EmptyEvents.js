var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');

// ############################## //
// svuoto la tabella degli eventi //
// ############################## //
var EmptyEvents = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running EmptyEvents on " + self.application_name);
		
		
		DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

			if(err) {
				params.onComplete(err, null);
				return;
			}

 			table.deleteMany({},{}, function(err, result) {

 				params.onComplete(err, null);

 			});
            
		});
		
	
	}

};



