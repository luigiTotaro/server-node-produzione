var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ##################################################################################### //
// totale accessi applicazione suddivisti per os e versione cone tempo medio di utilizzo //
// ##################################################################################### //
var AllAccessiDevice = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running AllAccessiDevice on " + self.application_name);
		
		DatabaseManager.getTable(self.application_name, 'access', function(err, table) {

			if(err) {

				params.onComplete(err, null);
				return;

			}
			
			
			table.aggregate([{
				$match: {
					last_activity_time: {
						$gt: 0
					}
				}
			}], function(err, result) {
			 				//console.log("result=" + result);
			 				if(result.length == 0) {
			 					params.onComplete(null, null);
			 					return;
			 				}

			 				var values_device = [];
			 				var device = [];

			 				_.each(result, function(row) {	
			 					var exist = _.findWhere(device, {uuid: row.uid});
			 					
			 					if(exist != null){
			 						if(exist.last_activity_time >row.last_activity_time){
			 							exist.last_activity_time = row.last_activity_time;
			 						}
			 					}else{
			 						device.push({
			 							uuid: row.uid,
			 							os: row.os,
			 							version: row.version,
			 							last_activity_time: row.last_activity_time
			 						});
			 					}
			 					
			 				});
			 				
			 				_.each(device, function(row) {	
			 						values_device.push( 
			 								"(" + 
			 									connection.escape(row.uuid) + ", " + 
			 									connection.escape(row.os) + ", " + 			 									
												connection.escape(row.version) + ", FROM_UNIXTIME(" + 
			 									connection.escape(row.last_activity_time) + "/1000" +
			 								"))"
			 							);
			 					
			 				});
			  

			 				connection.query('INSERT INTO accessi_device (uuid, os, version, insert_date) VALUES ' + values_device.join(",")
	 								+ 'on duplicate key update os=os ', function (error) {
	 							params.onComplete(error, null);
	 							
	 							});

			 							

			 			});

			
		});

	
	}

};
