var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');

// ###################### //
// totale ricette salvate //
// ###################### //
var SavedItemsCounter = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;



	self.run = function(params) {

		console.log("running SavedItemsCounter on " + self.application_name);

		DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

			if(err) {
				params.onComplete(err, null);
				return;
			}

			table.aggregate([

				{
					$match: {
						type: 'has_saved_item',
						time: {
							$gt: from,
							$lt: to
						}
					}
				}
				
				, {

					$group: { 
						_id: {
							descr: "$descr",
							id_progetto: "$id_progetto"
						},
						accessi: { 
							$sum: 1 
						}
					}
				}

			], function(err, result) {

				if(result.length == 0) {
					params.onComplete(null, null);
					return;
				}

				var values = [];

				_.each(result, function(row) {

					values.push("('" + 
						self.application_name + "', '" + 
						string_date + "', " + 
						row['_id'].id_progetto + ", " + 
						connection.escape(row['_id'].descr) + ", " + 
						row['accessi'] + 
					")");
				
				});

				
				connection.query('INSERT INTO items_saved (testata, data, id_progetto, descr, accessi) VALUES ' + values.join(","), function (error) {

				  params.onComplete(error, null);

				});
				
				

			});

             

		});
	
	}

};