var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ###################### //
// totale partite giocate //
// ###################### //
var ExportGameStats = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.run = function(params) {

		console.log("running ExportGameStats on " + self.application_name);
		
		try {

			DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

				if(err) {

					params.onComplete(err, null);
					return;

				}



				table.aggregate([

					{
						$match: {
							type: 'play_game',
							request_time: {
								$gt: from,
								$lt: to
							}
						}
					}
					
					, {

						$group: { 
							_id: {
								identifier: "$identifier",
								key: "$key",
								status: "$status"
							},
							totale: { 
								$sum: 1 
							},
							media_fruizione: {
								$avg: "$duration"
							}
						}
					}

				], function(err, result) {
					
					if(result.length == 0) {
						params.onComplete(null, null);
						return;
					}

					var values = [];

					_.each(result, function(row) {
						values.push( 
							"('" + self.application_name + "', '" + string_date + "', " + row['_id'].identifier + ", '" + row['_id'].key + "', '" + row['_id'].status + "', " + row['totale'] + ", " + Math.round(row['media_fruizione'] / 6000) + ")"
						);
					});
					
					connection.query('INSERT INTO games_play (testata, data, identifier, game_key, status, totale, media_gioco) VALUES ' + values.join(","), function (error) {
					  	params.onComplete(error, null);
					});
					
				});
	             
			});

		} catch(err) {

			params.onComplete(err, null);
 			winston.error("ExportGameStats/" + self.application_name, err);

		}		
				
	
	}

};





