var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');



// ###################### //
// esportazione delle classifiche
// ###################### //
var ExportGameEndings = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;

	self.games_to_check = [];
	self.players = [];

	self.getGamesToExports = function(onComplete) {

		var NOW = (new Date()).getTime();
				
		try {

			DatabaseManager.getTable(self.application_name, 'config', function(err, table) {

				if(err) {

					onComplete(err, null);
					return;

				}

				table.find(

					{
						values: {
							$elemMatch: {
								name: 'data_chiusura_gioco',
								value: {
									$lt: NOW
								}
							}
						},

						exported: {
							$eq: null
						}

					}
					
					 

				).toArray(function(err, result) {
					onComplete(null, result);
				});
	             
			});

		} catch(err) {

			onComplete(err, null);
 			
		}	
	
	};



	self.processPlayers = function(params) {

		console.log("* processing players");
		
		if(self.players.length == 0) {
		
			params.onComplete();	
		
		} else {

			 
			var values = [];

			for(var j=0; self.players.length > 0 && j<2; j++) {

				var player = self.players.shift();

				values.push( 
					"('" + self.application_name + "', '" + string_date + "', '" + player.uid + "', '" + player.user + "', '" + player.email + "', '" + player.key + "', " + player.identifier + ", " + player.total_time + ", " + player.punteggio + ")"
				);
		
			}

			
			console.log('INSERT INTO game_classifica (testata, data, uid, user,email,game_key, identifier, total_time,punteggio) VALUES ' + values.join(","));

			connection.query('INSERT INTO game_classifica (testata, data, uid, user,email,game_key, identifier, total_time,punteggio) VALUES ' + values.join(","), function (error) {
			  	// insert in mysql
				self.processPlayers(params);
			});

			

		}
		

	};


	self.finalizeGameExport = function(params) {


		DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

			if(err) {

				params.onComplete(err, null);
				return;

			}

			table.deleteMany({
				
				identifier: params.identifier,
                key: params.key

			},{}, function(err, result) {

 				DatabaseManager.getTable(self.application_name, 'config', function(err, table) {

					if(err) {

						params.onComplete(err, null);
						return;

					}

					table.findOneAndUpdate({

		                // find section
		                identifier: params.identifier,
                		key: params.key

		              }, {

		                  // modify section
		                  $set: {
		                        exported: (new Date()).getTime()
		                  }

		              }, {
		                 
		                upsert: false
		              }, function(err, result) {
		                
		                params.onComplete();
		                
		            });
		             
				});

 			});
             
		});

	};


	self.fetchTop1000 = function(params) {
						
		try {

			DatabaseManager.getTable(self.application_name, 'game_endings', function(err, table) {

				if(err) {

					params.onComplete(err, null);
					return;

				}

				table.find({
                    identifier: params.identifier,
                    key: params.key
                })
                .sort({
                    punteggio: -1,
                    total_time: 1
                })
                .limit(1000)
                .toArray(function(err, players) {
                    
                    self.players = players;

                    self.processPlayers({
                    	onComplete: function() {


                    		self.finalizeGameExport({

                    			identifier: params.identifier,
                    			key: params.key,
                    			onComplete: function() {
                    				params.onComplete();		
                    			}

                    		});


                    	}
                    })
                    

                }); 
	             
			});

		} catch(err) {

			params.onComplete(err, null);
 			
		}	
	
	};


	self.processGamesToCheck = function(params) {

		console.log("processing gametocheck");
		if(self.games_to_check.length == 0) {

			params.onComplete();

		} else {

			var game_config = self.games_to_check.shift();
			
			self.players = [];

			self.fetchTop1000({
				key: game_config.key,
				identifier: game_config.identifier,
				onComplete: function(err, players) {

					self.processGamesToCheck(params);

				}
			});

		}

	};


	self.run = function(params) {

		console.log("running ExportGameEndings on " + self.application_name);

		self.getGamesToExports(function(err, lista) {

			if(err) {

				winston.error("ExportGameEndings/" + self.application_name, err);
				params.onComplete(err, null);
				 

			} else {

				self.games_to_check = lista;
				
				self.processGamesToCheck({
					onComplete: function() {
						console.log("processamento terminato");
					}
				});				
				

			}



			 

		});
		
		
	}


};





