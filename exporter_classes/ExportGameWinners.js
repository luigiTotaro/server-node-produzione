var mysql = require('mysql');
var config = require('./config');
var _ = require('underscore');
var DatabaseManager = require('./databaseManager');
var winston = require('winston');


// ######################### //
// vincitori partire giocate //
// ######################### //
var ExportGameWinners = function(application_name, connection, string_date, from, to) {
		
	var self = this;
	self.application_name = application_name;
	self.results = null;

	self.current_start = 0;
	self.batch_size = 10;


	self.do_insert = function(params) {

		if(self.results.length == 0) {

			params.onComplete();	
		
		} else {

			var BUFFSIZE = self.batch_size;
			var values = [];

			while(BUFFSIZE > 0 && self.results.length > 0) {

				BUFFSIZE--;				
				var current_record = self.results.pop();

				values.push(
					"('" + self.application_name + "', '" + string_date + "' ," + 
					current_record['_id'].identifier + ", '" + current_record['_id'].key + "', '" + 
					current_record['_id'].winner_uid + "','" + current_record['_id'].winner_user + "'," + 
					current_record['vittorie'] + ")"
				);
			}
			
			

			connection.query('INSERT INTO games_winners (testata, data, identifier, game_key, uid, user, vittorie) VALUES ' + values.join(","), function (error) {
			  	self.current_start += self.batch_size;
				self.do_insert(params);
			});

		}

	};


	self.run = function(params) {

		console.log("running ExportGameWinners on " + self.application_name);
 		
 		try {

 			DatabaseManager.getTable(self.application_name, 'event', function(err, table) {

				if(err) {
					params.onComplete(err, null);
					return;
				}

				table.aggregate([

					{
						$match: {
							type: 'play_game',
							winner :{
								$exists: true
							},
							request_time: {
								$gt: from,
								$lt: to
							}
						}
					}
					
					, {

						$group: { 
							_id: {
								identifier: "$identifier",
								key: "$key",
								winner_uid: "$winner.uid",
								winner_user: "$winner.user"
							},
							vittorie: { 
								$sum: 1 
							}
						}
					}

				], {

					allowDiskUse: true

				},function(err, result) {

					if(result.length == 0) {
						params.onComplete(null, null);
						return;
					}

					self.results = result;

					self.do_insert({
						onComplete: function() {
							params.onComplete(null, null);
						}
					});
					
				});

			});

 		} catch(err) {

 			params.onComplete(err, null);
 			winston.error("ExportGameWinners/" + self.application_name, err);

 		}
		
		
	
	}

};





