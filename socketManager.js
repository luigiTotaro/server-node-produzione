var Config = require('./config');
var _ = require('underscore');
var Path = require('path');
var fs = require('fs');
var expressApplication = require('express')();
var http = require('http').Server(expressApplication);
var io = require('socket.io')(http);
var Helper = require('./helper');
var winston = require('winston');
var bodyParser = require('body-parser');
var DatabaseManager = require('./databaseManager.js');
var RegionsDatabase = require('./regionCoords.js');
expressApplication.use(bodyParser.json()); // support json encoded bodies
expressApplication.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var cors = require('cors');
expressApplication.use(cors());


// dinamic


function SocketManager() {};

SocketManager.IO_NAMESPACES = [];


SocketManager.ProvideAdditionalClasses = function(application_name) {

    var dir = Path.normalize( __dirname + "/repository/" + application_name + "/server" );

    if(fs.existsSync(dir)) {
        
        var files = fs.readdirSync(dir);

        _.each(files, function(filename) {
            
            var fileLibrary = Path.normalize(dir + "/" + filename);
            var basename = Path.basename(fileLibrary, '.js');
			
            /*
            require(fileLibrary)({
              socket_namespace: SocketManager.IO_NAMESPACES[application_name],
              application_name: application_name,
              express_instance: expressApplication
            });
            */
            eval(fs.readFileSync(fileLibrary, 'utf-8'));

            var obj_class = eval(basename);
            
            new obj_class({
                socket_namespace: SocketManager.IO_NAMESPACES[application_name],
                application_name: application_name,
                express_instance: expressApplication
            });
            
            
        });
    
    }

}



SocketManager.InitializeSocketServer = function(onComplete) {

    _.each(Config.appConfigurations, function(index, appName) {

        winston.info('Instanzio namespace  --> "' + appName +'"');

        SocketManager.IO_NAMESPACES[appName] = io.of("/" + appName);
        // SocketManager.IO_NAMESPACES[appName].libraries = [];
        SocketManager.IO_NAMESPACES[appName].additional_callbacks = {};

        // ################################################################### //
        // load di classi di estenzione relative alla particolare applicazione //
        // ################################################################### //
        SocketManager.ProvideAdditionalClasses(appName);

        // ################################################## //
        // evento di connessione su una particolare 'rivista' //
        // ################################################## //
        SocketManager.IO_NAMESPACES[appName].on('connection', function(clientConnection) {

            SocketManager.onConnect(appName, clientConnection);

            // ############################################### //
            // evento di disconnessione da una certa 'rivista' //
            // ############################################### //
            clientConnection.on('disconnect', function() {
                SocketManager.onDisconnect(appName, clientConnection);
            });

            clientConnection.on('message', function(data) {
                SocketManager.processMessage(appName, clientConnection, data);
            });

        });

    });


    http.listen(Config.socket_port, Config.bind_ip, function() {
		
		
		
        new Helper(expressApplication, SocketManager.IO_NAMESPACES);
        onComplete();
        winston.info('listening on ' + Config.bind_ip + ':' + Config.socket_port);
		
		 
		
			 
    });
	
	
	

}




SocketManager.enterRoom = function(room, clientConnection, onComplete) {

    clientConnection.join(room, function(err) {

        if(err) {

            clientConnection.applicationData.rooms[room] = null;
            onComplete(err);

        } else {

            clientConnection.applicationData.rooms[room] = (new Date()).getTime();
            onComplete(null);

        }

    });

};


SocketManager.leaveRoom = function(room, clientConnection, onComplete) {

    clientConnection.leave(room, function(err) {

        if(err) {

            clientConnection.applicationData.rooms[room] = null;
            onComplete(err);

        } else {

            delete clientConnection.applicationData.rooms[room];
            onComplete();

        }

    });

};


SocketManager.onConnect = function(application_name, clientConnection) {

    winston.info("A user with id '" + clientConnection.id + "' connected to app: '" + application_name + "'");

    clientConnection.applicationData = {
      rooms: {}
    };

    // OK: lista di tutti i socket in un dato namespace/application
    // console.log( "Utenti in '" + application_name + "' " + Object.keys(SocketManager.IO_NAMESPACES[application_name].sockets).length );


    // lista di tutte le rooms instanziate su socket io //

    // OK: lista di tutte room in cui un clientConnection è entrato
    // console.log( clientConnection.UserRooms ); //

    // lista di tutti i socket in una room

    _.each(SocketManager.IO_NAMESPACES[application_name].additional_callbacks[Config.SOCKET_EVENTS.CONNECT], function(callback) {
        callback(clientConnection, null);
    });

};



SocketManager.onDisconnect = function(application_name, clientConnection) {

   // SocketManager.setDeviceLogout(application_name, clientConnection);

    _.each(SocketManager.IO_NAMESPACES[application_name].additional_callbacks[Config.SOCKET_EVENTS.DISCONNECT], function(callback) {
        callback(clientConnection, null);
    });

};





/*
SocketManager.setDeviceLogout = function(application_name, clientConnection) {

    if(!clientConnection.applicationData.device_info || !clientConnection.applicationData.device_info.uid) {
      return;
    }

     

    // message.uid
    // message.pushId
    // message.os
    DatabaseManager.getTable(application_name, 'access', function(err, table) {

        if(!err) {    

            var NOW = (new Date()).getTime();
            
            table.findOneAndUpdate({

                // find section
                uid: clientConnection.applicationData.device_info.uid

              }, {

                  // modify section
                  $set: {
                    logout_time: NOW
                  }

              }, {

                // options section
                upsert: true

              }, function(err, result) {

                console.log("complete " + err);
                //onComplete( err ? err : null );

            });


        }

    });

}
*/










SocketManager.setDeviceLogin = function(application_name, clientConnection, message, next) {

    var NOW = (new Date()).getTime();

    clientConnection.applicationData.device_info = {
        uid: message.msg_body.uid,
        last_activity_time: NOW
    };


    // message.uid
    // message.pushId
    // message.os
    DatabaseManager.getTable(application_name, 'access', function(err, table) {

        if(!err) {

            table.findOneAndUpdate({
                // find section
                uid: message.msg_body.uid
              }, {
                  // modify section
                  $set: {
                    uid: message.msg_body.uid,
                    os: message.msg_body.os,
                    version: message.msg_body.version,
                    appVersion: message.msg_body.appVersion,
                    last_activity_time: NOW
                  }

              }, {
                
                // options section
                upsert: true

              }, function(err, result) {

                console.log("setDeviceLogin " + message.msg_body.os);
                
                if(err) {
                  winston.error("setDeviceLogin: " , err);
                }

                if(next) {
                    next(err);
                }

            });

        }

    });

}








SocketManager.setDeviceData = function(application_name, clientConnection, message, next) {

    // message.uid
    // message.pushId
    // message.os
    DatabaseManager.getTable(application_name, 'access', function(err, table) {

        if(!err) {

            table.findOneAndUpdate({
                // find section
                uid: message.msg_body.uid
              }, {
                  // modify section
                  $set: {
                    uid: message.msg_body.uid,
                    pushId: message.msg_body.pushId
                  }

              }, {
                
                // options section
                upsert: true

              }, function(err, result) {
                
                if(err) {
                  winston.error("setDeviceData: " , err);
                }

                if(err) {
                    next(err);
                }

            });

        }

    });

}




SocketManager.hasViewProgetto = function(application_name, clientConnection, message) {

    // message.uid
    // message.id
    // message.description
    // message.name
    
    DatabaseManager.getTable(application_name, 'event', function(err, table) {

        if(!err) {

            var _data = message.msg_body;
            _data.type = 'view_progetto';
            _data.time = (new Date()).getTime();

            table.insertOne(_data);

        }

    });


}




SocketManager.hasOpenedAugmentedContent = function(application_name, clientConnection, message) {

    // message.id
    // message.titolo
    DatabaseManager.getTable(application_name, 'event', function(err, table) {

        if(!err) {

            var _data = message.msg_body;
            _data.type = 'view_augmented_content';
            _data.time = (new Date()).getTime();

            table.insertOne(_data);

        }

    });

}




SocketManager.managePositionUpdate = function(application_name, clientConnection, message, next) {
    
    // message.lat
    // message.lng
    // message.citta
    // message.regione
    clientConnection.applicationData.position = {
        lat: message.msg_body.lat,
        lon: message.msg_body.lng,
        citta: message.msg_body.citta,
        regione: null
    };

    var regionData = _.find(
        RegionsDatabase.REGIONS, function(regionData) { 
            return regionData.names.indexOf(message.msg_body.region) != -1; 
        });


    regionData = regionData ? regionData : RegionsDatabase.REGIONS[0];

    clientConnection.applicationData.position.regione = {
        name: regionData.label,
        lat: regionData.lat,
        lon: regionData.lon
    };
    
    DatabaseManager.getTable(application_name, 'access', function(err, table) {

        if(!err) {

            table.findOneAndUpdate({
                // find section
                uid: clientConnection.applicationData.device_info.uid
              }, {
                  // modify section
                  $set: {
                    position: clientConnection.applicationData.position
                  }

              }, {
                
                // options section
                upsert: true

              }, function(err, result) {
                
                if(err) {
                  winston.error("setDeviceData: " , err);
                }

                if(next) {
                    next(err);
                }

            });

        }

    });    

}






SocketManager.updateActivity = function(application_name, clientConnection) {

    if(!clientConnection.applicationData.device_info || !clientConnection.applicationData.device_info.uid) {
      return;
    }

     
    // message.uid
    // message.pushId
    // message.os
    DatabaseManager.getTable(application_name, 'access', function(err, table) {

        var NOW = (new Date()).getTime();
        var delta = (NOW - clientConnection.applicationData.device_info.last_activity_time);
        clientConnection.applicationData.device_info.last_activity_time = NOW;

        if(!err) {    
    
            table.findOneAndUpdate({

                // find section
                uid: clientConnection.applicationData.device_info.uid

              }, {
                  
                  $inc: {
                    total_activity_time: delta              
                  }                  

              }, {
            
                upsert: false

              }, function(err, result) {

                
            });


        }

    });

};



SocketManager.processMessage = function(application_name, clientConnection, message) {

    if(message.type) {

        switch(message.type) {

            case 'set_device_data':
                SocketManager.setDeviceData(application_name, clientConnection, message);
                break;

            case 'set_login':
                SocketManager.setDeviceLogin(application_name, clientConnection, message);
                break;

            case 'has_leaved_progetto':
                
                var room_name = 'room_progetto_' + message.msg_body.id;

                SocketManager.leaveRoom(room_name, clientConnection, function(err) {
                    console.log('leaved room ' + room_name);
                });
                
                break;

            case 'has_view_progetto':
    
                var room_name = 'room_progetto_' + message.msg_body.id;

                SocketManager.enterRoom(room_name, clientConnection, function(err) {
                    console.log('* joined room ' + room_name);
                });

                SocketManager.hasViewProgetto(application_name, clientConnection, message);

                break;

            case 'has_open_augmented_content':
                SocketManager.hasOpenedAugmentedContent(application_name, clientConnection, message);
                break;

            case 'position':
                SocketManager.managePositionUpdate(application_name, clientConnection, message);
                break;


            case 'has_open_or_reopen_socket':

                SocketManager.setDeviceLogin(application_name, clientConnection, {

                    msg_body: message.msg_body.set_login

                }, function(err) {

                    if(message.msg_body.progetto) {

                        var room_name = 'room_progetto_' + message.msg_body.progetto.id;

                        SocketManager.enterRoom(room_name, clientConnection, function(err) {
                            console.log('* rejoined room ' + room_name);
                        });

                    }

                    if(message.msg_body.position) {

                        SocketManager.managePositionUpdate(application_name, clientConnection, {
                            msg_body: message.msg_body.position
                        });
                    
                    }

                });

                break;

            default:
                break;
        
        }

    }


    SocketManager.updateActivity(application_name, clientConnection);

    _.each(SocketManager.IO_NAMESPACES[application_name].additional_callbacks[Config.SOCKET_EVENTS.MESSAGE], function(callback) {
        callback(clientConnection, message);
    });

}




module.exports = SocketManager;
