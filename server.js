var DatabaseManager = require('./databaseManager');
var SocketManager = require('./socketManager');
var ExportManager = require('./exportManager');
var Config = require('./config');
var _ = require('underscore');
var fs = require('fs');
var Path = require('path');
var CronJob = require('cron').CronJob;
var winston = require('winston');

winston
  .remove(winston.transports.Console)
  .add(winston.transports.Console, {
    levetl: 'info',
    prettyPrint: true,
    colorize: true,
    silent: false,
    timestamp: false
  })
  .add(winston.transports.File, {
    level: 'warn',
    filename: 'logs.txt',
    handleExceptions: false,
    json: false,
    maxsize: 5242880, //5MB
    maxFiles: 5
  });




global.configurables = [];


_.each(Config.appConfigurations, function(index, appName) {

    var path = Path.normalize( __dirname + "/" + Config.repository_root + "/" + appName );

    if(!fs.existsSync( path )) {
      console.log("non esiste la cartella " + path);
      process.exit();
    }

});


DatabaseManager.InitializeConnection(function(err) {

    if(err) {
        winston.error("[connessione chiusa]: " , err);
        process.exit();
    }

    SocketManager.InitializeSocketServer(function(err) {

      winston.info('#########');
      winston.info('app start');
      winston.info('#########');
      
    });

    //ExportManager();

});



new CronJob('0 0 23 * * *', function() {

  console.log('You will see this message every second');
  ExportManager();

}, null, true, 'Europe/Rome');
