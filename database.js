var MongoClient = require('mongodb').MongoClient;
var _ = require('underscore');

function DatabaseWrapper() {};


DatabaseWrapper.CONNECTION = null;
DatabaseWrapper.DBS = [];
DatabaseWrapper.NAMES = [];

DatabaseWrapper.databaseExists = function(dbName) {
    return DatabaseWrapper.NAMES.indexOf(dbName) != -1;
};

DatabaseWrapper.Close = function() {
  DatabaseWrapper.CONNECTION.close();
}


DatabaseWrapper.getData = function(dbname, onComplete) {

    if(DatabaseWrapper.databaseExists(dbname) == false) {
        onComplete("il database '" + dbname + "' non esiste", null);
        return;
    }

    if(!DatabaseWrapper.DBS[dbname]) {
        console.log("creating", dbname);
        DatabaseWrapper.DBS[dbname] = DatabaseWrapper.CONNECTION.db(dbname);
    }

    var collezione = DatabaseWrapper.DBS[dbname].collection('testCollection');


    collezione.find().toArray(function(err, docs) {

      onComplete(null, docs);

    });
}





DatabaseWrapper.Initialize = function(params) {

    MongoClient.connect(params.url, function(err, db) {

        if(err) {

            if(db) {
                db.close();
            }

            params.onComplete(err.message);
            return;

        }

        DatabaseWrapper.CONNECTION = db;

        DatabaseWrapper.CONNECTION.admin().listDatabases(function(err, dbs) {

            _.each(dbs.databases, function(current_database) {

                DatabaseWrapper.NAMES.push(current_database.name);

            });

            params.onComplete(null);

        });

    });

};

module.exports = DatabaseWrapper;
