var MongoClient = require('mongodb').MongoClient;
var config = require('./config');
var _ = require('underscore');
var winston = require('winston');
var DatabaseManager = require('./databaseManager');

function Bot(parameters) {
 	
 	this.classGame=parameters.classGame;
	this.fromUid = parameters.uid;
	this.match_id = parameters.match_id;
	this.socket_id = parameters.socket_id;
	this.user = parameters.user;
	this.identifier = parameters.identifier;
	this.game_data = parameters.game_data;
	this.startIn = parameters.startIn;
	this.application_name = parameters.application_name;
	this.key=parameters.key;

	this.uidBot="12345678-BOTX-ABCD-EFGH-A1B2C3D4E5F6";
	this.nameBot=this.estraiNome(); //"Utente_" + (new Date()).getTime();

	//probabilità di vincita
	var prob = Math.floor((Math.random() * 100) + 1); //da 1 a 100
	this.botVince = (prob<30) ? true : false;
	this.vittoriaPerRisposte = (prob<50) ? true : false;

    console.log("Costruttore Bot");
    console.log("Bot Vincerà: " + this.botVince);
    console.log("Vittoria per numero di risposte: " + this.vittoriaPerRisposte);

};

Bot.prototype.startBot = function() {
    
    self= this;

    this.msg = JSON.parse('{"msg_body": {"from": {"uid": "'+self.fromUid+'"},"match_id": "'+self.match_id+'","identifier": "'+self.identifier+'","key": "'+self.key+'","uid": "'+self.uidBot+'","user": "'+self.nameBot+'","game_data": {"right_answers": 0}, "game_progress":{}}}');

    console.log("bot partito - matchId: " + this.match_id);

    //dopo x secondi faccio accettare la sfida
    setTimeout(function () {
	    console.log("accetto la partita");
    	self.classGame.acceptMatchMake(self,self.msg);
    }, self.startIn*1000);

    //e comunque mi prendo i dati di questa sfida
    DatabaseManager.getTable(self.application_name, 'config', function(err, table) {
        
        if(err) {
            console.log("Recupero dati sfida da Bot - Err 1: " + err);
            return;
        }
                    
        table.findOne({
            
            identifier: self.identifier,
            level: "progetto",
            key: self.key
            
        }, function(err, doc) {
          
            if(err) {
	            console.log("Recupero dati sfida da Bot - Err 2: " + err);
                return;
            }                
            
            if(doc) {
                self.values = doc.values;
            }
                         
        });   
                             
    });


}

//il bot è riuscito ad aggiudicarsi l'invito. La sida può cominciare.... SOLO QUIZ
Bot.prototype.inviteResult = function(result) {
    
    self= this;

    if (result==true)
    {
        setTimeout(function () {
		    console.log("Bot fa partire il gioco");
	    	self.classGame.userStartedGame (self, self.msg);
        }, 100);
    }

}

//L'utente ha completato tutte le domande. Decido se il bot vince, e in che modo. SOLO QUIZ
Bot.prototype.userCompletedQuestions = function(risposte,tempo) {
    
    self= this;

    console.log("userCompletedQuestions - risposte: " + risposte + " - tempo: " + tempo);
	var numero_domande=0;

	for (var i=0;i<self.values.length;i++)
	{
		if (self.values[i].name == "lista_domande")
		{
			var domande=JSON.parse(self.values[i].value);
			var numero_domande=domande.length;
		}
	}

	if (risposte<numero_domande)
	{
		if (self.botVince)
		{
			if (self.vittoriaPerRisposte) self.msg.msg_body.game_data.right_answers=risposte+1;
			else
			{
				self.msg.msg_body.game_data.right_answers=risposte;
				self.msg.msg_body.delta_time=-2000; 
			}
		}
		else
		{
			if (risposte>0)
			{
				if (self.vittoriaPerRisposte) self.msg.msg_body.game_data.right_answers=risposte-1;
				else
				{
					self.msg.msg_body.game_data.right_answers=risposte;
					self.msg.msg_body.delta_time=+2000; 
				}			
			}
			else
			{
				//nessuna risposta giusta... per perdere deve essere maggiore come tempo
				self.msg.msg_body.game_data.right_answers=risposte;
				self.msg.msg_body.delta_time=+2000; 
			}
		}

	}
	else
	{
		if (self.botVince)
		{
			//puo' vincere solo per un tempo inferiore
			self.msg.msg_body.game_data.right_answers=risposte;
			self.msg.msg_body.delta_time=-2000; 
		}
		else
		{
			if (self.vittoriaPerRisposte) self.msg.msg_body.game_data.right_answers=risposte-1;
			else
			{
				self.msg.msg_body.game_data.right_answers=risposte;
				self.msg.msg_body.delta_time=+2000; 
			}			
		}
	}

	self.classGame.allQuestionsAnswered (self, self.msg);

}

//L'utente ha trovato l'unica immagine, o il tempo è scaduto. Decido se il bot vince. SOLO TROVA SINGOLA IMMAGINE
Bot.prototype.checkFindImageWin = function() {
	return !self.botVince;
}

//L'utente ha trovato tutte le immagini, o il tempo è scaduto. Decido se il bot vince. SOLO TROVA IMMAGINI MULTIPLE
Bot.prototype.checkFindMultipleImageWin = function() {
	return !self.botVince;
}

//L'utente ha trovato una immagine. LA faccio trovare di seguito anche al bot, in modo la lasciarlo allineato e decidere in seguito se vincerà o perderà
Bot.prototype.userFoundImage = function(num_immagini) {

	self= this;

	var deltaTime=Math.floor((Math.random() * 5000) + 1000);   //da 1 a 5 secondi
	
	setTimeout(function () {
		console.log("Bot trova una immagine");
		self.msg.msg_body.game_progress.found_images_count=num_immagini;
		self.classGame.userHasMadeGameProgress (self, self.msg);
	}, deltaTime);
}

Bot.prototype.estraiNome = function() {
	var arrayNomi = ["Andrea","Luca","Marco","Francesco","Matteo","Alessandro","Davide","Simone","Federico","Lorenzo","Mattia","Stefano","Giuseppe","Riccardo","Daniele","Michele","Alessio","Antonio","Giovanni","Nicola","Gabriele","Fabio","Alberto","Giacomo","Giulio","Filippo","Gianluca","Paolo","Roberto","Salvatore","Emanuele","Edoardo","Enrico","Vincenzo","Nicolò","Leonardo","Jacopo","Manuel","Mirko","Tommaso","Pietro","Luigi","Giorgio","Angelo","Dario","Valerio","Domenico","Claudio","Alex","Christian","Giulia","Chiara","Francesca","Federica","Sara","Martina","Valentina","Alessia","Silvia","Elisa","Ilaria","Eleonora","Giorgia","Elena","Laura","Alice","Alessandra","Jessica","Arianna","Marta","Veronica","Roberta","Anna","Giada","Claudia","Beatrice","Valeria","Michela","Serena","Camilla","Irene","Cristina","Simona","Maria","Noemi","Stefania","Erika","Sofia","Lucia","Vanessa","Greta","Debora","Nicole","Angela","Paola","Caterina","Monica","Erica","Lisa","Gaia"];
//	console.log("Numero nomi: "+arrayNomi.length);
	var indice=(Math.floor((Math.random() * arrayNomi.length) + 1))-1;
//	console.log("Numero estratto: "+indice);
	//controllo di sicurezza
	if (indice>(arrayNomi.length-1)) indice=arrayNomi.length-1;
	if (indice<0) indice=0;
//	console.log("Nome estratto: " + arrayNomi[indice]);
	return arrayNomi[indice];
}


module.exports = Bot;