

var client_game_trova_immagine_gz = function(  ) {

    this.ionicModal = null;
    this.ionicPopup = null;
    this.HelperService = null;
    this.http = null;
    this.GlobalVariables = null;
    this.LoggerService = null;
    this.FileSystemService = null;
    this.scope = null;
    this.gameCenter = null;
    this.rootScope = null;
    this.WikiService = null;

    this.hasPendingRequest = false;

    this.isInArMode = false;

    this.currentGame = null;

    this.currentAlertBox = null;
    
    // scope dedicato alla vista che indica l'indizio
    this.clue_scope = null;

    this.stats = null;

    this.config = {
        
        name: 'Trova l\'Immagine per Primo',
        description: 'Trova l\'immagine prima del tuo avversario. Vince chi trova prima l\'immagine',
        key: 'mediasoft.trovaimmagine.gz',
        short_key: 'MTG0',
        time_to_live: 60000,
        icon: 'ion-person-stalker',
        color: 'grey',

        parameters: {
            background: {
                type: 'img',
                description: 'Immagine di sfondo usata nella lista dei giochi'
            },
            lista_immagine_da_trovare: {
                type: 'wtc_image_list',
                description: 'lista di immagini da usare per il riconoscimento'
            }
        },

        values: null

    };

}


client_game_trova_immagine_gz.Instance = null;



client_game_trova_immagine_gz.Initialize = function(params) {

    if(client_game_trova_immagine_gz.Instance != null) {
        return;
    }    

    client_game_trova_immagine_gz.Instance = new client_game_trova_immagine_gz();
    client_game_trova_immagine_gz.Instance.ionicModal = params.ionicModal;
    client_game_trova_immagine_gz.Instance.ionicPopup = params.ionicPopup;
    client_game_trova_immagine_gz.Instance.HelperService = params.HelperService;
    client_game_trova_immagine_gz.Instance.http = params.http;
    client_game_trova_immagine_gz.Instance.GlobalVariables = params.GlobalVariables;
    client_game_trova_immagine_gz.Instance.LoggerService = params.LoggerService;
    client_game_trova_immagine_gz.Instance.FileSystemService = params.FileSystemService;
    client_game_trova_immagine_gz.Instance.SocketService = params.SocketService;
    client_game_trova_immagine_gz.Instance.gameCenter = params.GameCenter;
    client_game_trova_immagine_gz.Instance.rootScope = params.RootScope;
    client_game_trova_immagine_gz.Instance.WikiService = params.WikiService;
    
    var SEED = client_game_trova_immagine_gz.Instance.config.short_key;

    client_game_trova_immagine_gz.Instance.gameCenter.loadStats({
        key: client_game_trova_immagine_gz.Instance.config.key,
        title: client_game_trova_immagine_gz.Instance.config.name,
        onComplete: function(err, data) { 
        }
    });
    
    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', function() {
        
        client_game_trova_immagine_gz.Instance.gameCenter.unregisterGame({
            key: client_game_trova_immagine_gz.Instance.config.key,
            title: client_game_trova_immagine_gz.Instance.config.name,
            description: client_game_trova_immagine_gz.Instance.description
        });
       
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite',                      SEED + 'onGameInvite', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite_ack',                  SEED + 'onGameInviteAck', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite_timeout',              SEED + 'onGameInviteTimeout', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite_inviter_accept',       SEED + 'onInviterAccept', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_invite_invited_accept',       SEED + 'onInvitedAccept', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_win_by_forfait',              SEED + 'onGameWinByForfait', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_victory',                     SEED + 'onGameVictory', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key,     'game_lost',                        SEED + 'onGameLost', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setDeviceEventCallback(client_game_trova_immagine_gz.Instance.config.key, 'pause',                            SEED + 'onGamePaused', null);
        client_game_trova_immagine_gz.Instance.gameCenter.setDeviceEventCallback(client_game_trova_immagine_gz.Instance.config.key, 'resume',                           SEED + 'onGameResumed', null);

        client_game_trova_immagine_gz.Instance.WikiService.registerCallback(client_game_trova_immagine_gz.Instance.config.key, SEED + 'respondToMessageFromWiki', null);
        
    });



    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', function() {        
		
		client_game_trova_immagine_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', null);
		client_game_trova_immagine_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.LISTA_PROGETTI, params.LoggerService.ACTION.ENTER, SEED + 'exitDetailProgetto', null);
		client_game_trova_immagine_gz.Instance.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.HOME, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_deallocate', null); 
		
        client_game_trova_immagine_gz.Instance.gameCenter.removeGameStat(client_game_trova_immagine_gz.Instance.config.key);
        client_game_trova_immagine_gz.Instance = null;    

    });


    params.LoggerService.setCallback(params.LoggerService.CONTROLLER_STATES.DETAIL_PROGETTO, params.LoggerService.ACTION.ENTER, SEED + 'gameFindImage_onEnterDetailProgetto', function() {

			console.log("client_game_trova_immagine_gz");
 
            client_game_trova_immagine_gz.Instance.gameCenter.getConfiguration({
                key: client_game_trova_immagine_gz.Instance.config.key,
                onComplete: function(err, configuration) {                     
            

                    if(!err && configuration.values && configuration.visible == true) {                         

                        if(configuration.forDebugOnly == false || (configuration.forDebugOnly == true && client_game_trova_immagine_gz.Instance.GlobalVariables.isDebugDevice == true)) {

                            client_game_trova_immagine_gz.Instance.config.values = [];

                            var image_list_parameter = _.find(configuration.values, function(item) { return item.name == 'lista_immagine_da_trovare' });                
                            client_game_trova_immagine_gz.Instance.config.values['lista_immagine_da_trovare'] = JSON.parse(image_list_parameter.value);                                                    

                            var background = _.find(configuration.values, function(item) { return item.name == 'background' });      
                            background = background.value ? (JSON.parse(background.value)).path : "";
                            client_game_trova_immagine_gz.Instance.config.values['background'] = client_game_trova_immagine_gz.Instance.GlobalVariables.baseUrl + "/" + background;

                            try {

                                client_game_trova_immagine_gz.Instance.gameCenter.registerGame({
                                    key: client_game_trova_immagine_gz.Instance.config.key,
                                    icon: client_game_trova_immagine_gz.Instance.config.icon,
                                    color: client_game_trova_immagine_gz.Instance.config.color,
                                    title: client_game_trova_immagine_gz.Instance.config.name,
                                    description: client_game_trova_immagine_gz.Instance.config.description,
                                    classInstance: client_game_trova_immagine_gz.Instance,
                                    background: client_game_trova_immagine_gz.Instance.config.values['background'],
                                    onClickFunctionName: 'startMatchMaking'
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite', SEED + 'onGameInvite', function(message) {
                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequest(message);
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite_ack', SEED + 'onGameInviteAck', function(message) {
                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequestAck(message);
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite_timeout', SEED + 'onGameInviteTimeout', function(message) {            
                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequestTimeout(message);
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite_expired_or_cancelled', SEED + 'onGameExpiredOrCancel', function(message) {            
                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequestExpiredOrCancelled(message);
                                });
                                
                                // ############################################################################ // 
                                // ricezione di un messaggio che indica che un utente ha accettato la MIA sfida //
                                // ############################################################################ //
                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite_inviter_accept', SEED + 'onInviterAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_trova_immagine_gz.Instance.gameCenter.alterStat({
                                        key: client_game_trova_immagine_gz.Instance.config.key,
                                        title: client_game_trova_immagine_gz.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequestInviterAccept(message);
                                });

                                // ######################################################################################################## //
                                // ricezione di un messaggio che indica che ho correttamente accettato la sfida lanciata da un ALTRO utente //
                                // ######################################################################################################## //
                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_invite_invited_accept', SEED + 'onInvitedAccept', function(message) {        
                                    
                                    // incrementa le partite giocate  
                                    client_game_trova_immagine_gz.Instance.gameCenter.alterStat({
                                        key: client_game_trova_immagine_gz.Instance.config.key,
                                        title: client_game_trova_immagine_gz.Instance.config.name,
                                        stat_name: 'giocate'
                                    });

                                    client_game_trova_immagine_gz.Instance.onReceiveGameRequestInvitedAccept(message);
                                });


                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_win_by_forfait', SEED + 'onGameWinByForfait', function(message) {     
                                    
                                    // incrementa le partite giocate  
                                    client_game_trova_immagine_gz.Instance.gameCenter.alterStat({
                                        key: client_game_trova_immagine_gz.Instance.config.key,
                                        title: client_game_trova_immagine_gz.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    client_game_trova_immagine_gz.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: true
                                    });
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_victory', SEED + 'onGameVictory', function(message) {         
                                    
                                    // incrementa le partite giocate  
                                    client_game_trova_immagine_gz.Instance.gameCenter.alterStat({
                                        key: client_game_trova_immagine_gz.Instance.config.key,
                                        title: client_game_trova_immagine_gz.Instance.config.name,
                                        stat_name: 'vinte'
                                    });

                                    client_game_trova_immagine_gz.Instance.onReceiveGameEnding({
                                        isWon: true,
                                        isForfait: false
                                    });
                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setMessageCallback(client_game_trova_immagine_gz.Instance.config.key, 'game_lost', SEED + 'onGameLost', function(message) {            
                                    client_game_trova_immagine_gz.Instance.onReceiveGameEnding({
                                        isWon: false,
                                        isForfait: false
                                    });
                                });


                                client_game_trova_immagine_gz.Instance.gameCenter.setDeviceEventCallback(client_game_trova_immagine_gz.Instance.config.key, 'pause', SEED + 'onGamePaused', function(message) {            
                                    
                                    client_game_trova_immagine_gz.Instance.onGamePause();

                                });

                                client_game_trova_immagine_gz.Instance.gameCenter.setDeviceEventCallback(client_game_trova_immagine_gz.Instance.config.key, 'resume', SEED + 'onGameResumed', function(message) {    
                                    
                                    client_game_trova_immagine_gz.Instance.onGameResume();
                                    
                                });
                                
                                // ricezione eventi dall'ambiente wikitude dedicato ai giochi collaborativi
                                client_game_trova_immagine_gz.Instance.WikiService.registerCallback(client_game_trova_immagine_gz.Instance.config.key, SEED + 'respondToMessageFromWiki', function(message) {
                                    
                                    if(!(message.type && message.key)) {
                                        return;
                                    }

                                    if(message.type == 'startRA') {

                                        client_game_trova_immagine_gz.Instance.userHasStartedAR(message);

                                    } else if(message.type == 'AR_Forfait') {

                                        client_game_trova_immagine_gz.Instance.WikiService.sendMessageToAR({
                                            type: 'closeAR'
                                        });

                                        client_game_trova_immagine_gz.Instance.doForfaitGame();
                                    
                                    } else if(message.type == 'AR_Image_Tracked') {
                                    
                                        client_game_trova_immagine_gz.Instance.onImageTracked(message.msg_body);
                                    
                                    } 

                                });
                                
                            
                            } catch(err) {

                                alert(err);
                            
                            }

                        }
                        
                    }                 
                }                
            });

        
                    
    });
            


}



client_game_trova_immagine_gz.prototype.onImageTracked = function(message) {


    var self = this;

    var match = _.find(client_game_trova_immagine_gz.Instance.config.values['lista_immagine_da_trovare'].listaImmagini, function(image) {
        return (message.target == image.targetName);
    });

    if(match) {

        if(self.currentGame.game_data.choosed_image_index == match.indice) {
            
             
            self.WikiService.sendMessageToAR({
                type: 'closeAR'
            });

            self.isInArMode = false;    
             
            self.currentAlertBox = self.ionicPopup.show({
                title: 'Complimenti hai trovato l\'indizio!',
                template:
                    '<div>Attendi un istante per sapere se hai battuto <strong>' + self.currentGame.user + '</strong></div>' +
                    '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'
            });

            // ho trovato l'immagine giusta. concludo la partita //
            self.SocketService.emit('message', 'right_image_found', {}, {     
                key: self.config.key,                   
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                match_id: self.currentGame.match_id
            });
            
            
        } else {

            self.WikiService.sendMessageToAR({
                type: 'showAlert',
                msg_body: {
                    messaggio: 'Non è questa l\'immagine',
                    duration: 4000
                }
            });

        }

    }


}


client_game_trova_immagine_gz.prototype.userHasStartedAR = function() {

    var self = this;     
    
    if(self.currentAlertBox) {
        self.currentAlertBox.close();
        self.currentAlertBox = null;    
    }

    if(self.currentGame) {        

        self.currentGame.game_data.game_start_time = (new Date()).getTime();

        self.WikiService.sendMessageToAR({
            type: 'showTimer'
        });

        setTimeout(function() {
            
            self.WikiService.sendMessageToAR({
                type: 'showAlert',
                msg_body: {
                    messaggio: "Trova l'immagine per primo",
                    duration: 4000
                }
            });

        }, 500);

        self.isInArMode = true;
    
    } else {

        // al termine dell'avvio della realtà aumentata potrei aver già vinto per abbandono
        // in tal caso self.currentGame è nullo.
        
        client_game_trova_immagine_gz.Instance.WikiService.sendMessageToAR({
            type: 'closeAR'
        });

        self.isInArMode = false;

    }
    
    
};



// ################################### //
// avvio ricerca giocatori per giocare //
// ################################### //
client_game_trova_immagine_gz.prototype.startMatchMaking = function() {

    var self = this;
    self.isInArMode = false;

    if(!self.HelperService.isNetworkAvailable()) {

        self.ionicPopup.alert({
            title: 'Attenzione <i class="ion-android-alert"></i>',
            template: 'Non sei attualmente connesso ad Internet'
        });

        return;
    }

    self.gameCenter.getNickname({
        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            var choosed_image_index = 
                Math.floor(Math.random() * self.config.values['lista_immagine_da_trovare'].listaImmagini.length); 

            self.SocketService.emit('message', 'start_match_make', {}, {     
                key: self.config.key,   
                user: nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto,
                game_data: {
                    choosed_image_index: choosed_image_index
                }
            });

        }
    });

};



// ######################### //
// ricezione invito gi gioco //
// ######################### //
client_game_trova_immagine_gz.prototype.onReceiveGameRequest = function(message) {

    var self = this;

    self.gameCenter.appendRequest({
        level: 'progetto',
        match_id: message.match_id,
        title: self.config.name,
        key: self.config.key,
        time_to_live: self.config.time_to_live,
        classInstance: self,
        onClickFunctionName: 'acceptInvite',
        from: {
            user: message.user,
            uid: message.uid
        }
        
    });

};


// ########################################## //
// accettazione di una sfida previo richiesta //
// ########################################## //
client_game_trova_immagine_gz.prototype.acceptInvite = function(request) {

    var self = this;
    self.isInArMode = false;

    self.gameCenter.getNickname({

        onComplete: function(nickname) {

            if(!nickname) {
                return;
            }

            self.askGameReq = self.ionicPopup.confirm({
                title: 'Accetti la sfida da <strong>' + request.from.user + '</strong> ?',
                template: 'Premi "Si" per iniziare a giocare a ' + self.config.name + ' contro ' + request.from.user,
                cancelText: 'No', 
                cancelType: 'button-stable',
                okText: 'Si', 
                okType: 'button-balanced'
            });

            self.askGameReq.then(function(res) {

                if(res) {
                    
                    self.gameCenter.removeRequest(request);
                    
                    self.SocketService.emit('message', 'accept_match_make', {}, {    
                        match_id: request.match_id, 
                        key: self.config.key,   
                        user: nickname,
                        uid: self.GlobalVariables.deviceUUID,
                        from: request.from,
                        identifier: self.GlobalVariables.application.currentProgetto.idProgetto
                    });                    

                }  

            });

        }

    });


};



// ############################################################################ //
// il server notifica che dopo un tot tempo la mia richiesta di gioco è scaduta //
// ############################################################################ //
client_game_trova_immagine_gz.prototype.onReceiveGameRequestTimeout = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.ionicPopup.alert({
        title: 'Nessun utente ha accettato la tua sfida',              
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};  




// ##################################################### //
// un utente ha raccolto la sfida che IO ho lanciato ... //
// ##################################################### //
client_game_trova_immagine_gz.prototype.onReceiveGameRequestInviterAccept = function(message) {

    var self = this;

    self.hasPendingRequest = false;
    self.waitBox.close();

    self.gameCenter.closeView();

    //alert('sfida raccolta da: ' + JSON.stringify(message));

    self.showClue(message);

}; 




// ######################################################## //
// vengo notificato per aver accettato la sfida di un altro //
// ######################################################## //
client_game_trova_immagine_gz.prototype.onReceiveGameRequestInvitedAccept = function(message) {

    var self = this;
    self.gameCenter.closeView();
    self.showClue(message);

}; 


// ###################################################### //
// abbandono il gioco esplicitamente vince l'altro utente //
// ###################################################### //
client_game_trova_immagine_gz.prototype.doForfaitGame = function() {

    var self = this;
    
    self.SocketService.emit('message', 'forfait_game', {}, {  

        match_id: self.currentGame.match_id,  
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.GlobalVariables.application.currentProgetto.idProgetto

    });
    

    self.assertEnding({
        isWon: false,
        isForfait: true
    });

    
}; 


// ############################### //
// ricevo una notifica di vittoria //
// ############################### //
client_game_trova_immagine_gz.prototype.onReceiveGameEnding = function(params) {

    var self = this;  

    try {

        // #################################################################################################### //
        // ricevo una notifica di vittoria mentre sono ancora nella schermata di presentazione dell'indizio.... //
        // #################################################################################################### //
        if(self.clue_scope && self.clue_scope.clueWnd) {
            self.hideClue(function() {
        
                self.assertEnding({
                    isWon: params.isWon, 
                    isForfait: params.isForfait
                });
            
            });
        }


        // ###################################################### //
        // ricevo una notifica mentre sono nella realtà aumentata //
        // ###################################################### //
        if(self.isInArMode == true) {

            self.isInArMode = false;
        
            client_game_trova_immagine_gz.Instance.WikiService.sendMessageToAR({
                type: 'closeAR'
            });

            self.assertEnding({
                isWon: params.isWon, 
                isForfait: params.isForfait
            });

        }


        // ###################################################################### //
        // ricevo la notifica di vittoria DOPO aver trovato l'immagine e          //  
        // sono in attesa di sapere se ho impiegato meno tempo del mio avversario //
        // ###################################################################### //
        if(self.currentAlertBox) {
             
            self.currentAlertBox.close();
            self.currentAlertBox = null;

            self.assertEnding({
                isWon: params.isWon, 
                isForfait: params.isForfait
            });
            
        }


    } catch(err) {

        alert(err);
    
    }
    
};



client_game_trova_immagine_gz.prototype.assertEnding = function( params ) {

    var self = this;

    var title = '';
    var template = '';
    var button = '';

    if(params.isWon) {

        title = 'Complimenti: hai vinto';

        template = 
            params.isForfait 
            ? 'Sembra proprio che <strong>' + self.currentGame.user + '</strong> abbia abbandonato la sfida!'
            : 'Hai battuto <strong>' + self.currentGame.user + '</strong>. Hai impiegato meno tempo per trovare l\'indizio!';

        button = 'button-balanced' ;

    } else {

        title = 'Purtroppo hai perso';

        template = 
            params.isForfait
            ? 'Hai ceduto la vittoria a <strong>' +  self.currentGame.user + '</strong>. La prossima volta andrà meglio!'
            : 'Sei stato sconfitto da <strong>' + self.currentGame.user + '</strong>. Ha impegato meno tempo a trovare l\'indizio';

        button = 'button-assertive' ;

    }

       
    self.ionicPopup.alert({
        title: title,              
        template: template,
        okText: 'Ok', 
        okType: button        
    });

    self.currentGame = null;

}



client_game_trova_immagine_gz.prototype.hideClue = function(onClose) {

    var self = this;

    if(self.clue_scope.clueWnd) {

        self.clue_scope.clueWnd.remove().then(function() {
            onClose();
        });        

        self.clue_scope.clueWnd = null;
        self.clue_scope.$destroy();
        delete self.clue_scope;

    }
    
}


client_game_trova_immagine_gz.prototype.startGame = function() {

    var self = this; 

    self.SocketService.emit('message', 'start_game', {}, {     
        key: self.config.key,   
        uid: self.GlobalVariables.deviceUUID,
        identifier: self.currentGame.identifier,
        match_id: self.currentGame.match_id
    });

    var WTC_PATH = self.config.values['lista_immagine_da_trovare'].wtcPath;
 
    var json = {
        wtc: WTC_PATH
    }

    self.currentAlertBox = self.ionicPopup.show({
        title: '<div>Avvio ambiente realtà aumentata</div>',
        template: '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>'        
    });

    self.WikiService.openArForGame(self.config.key, json);
    
}


// il momento in cui mostro l'indizio è anche il momento in cui ho instanziato una sfida
// a questo punto il contatore delle partite giocate aumenta
client_game_trova_immagine_gz.prototype.showClue = function(message) {

    var self = this;        
    self.clue_scope = self.rootScope.$new(true);
    self.clue_scope.clueWnd = null;

    var image_index = message.game_data.choosed_image_index;
    var image = self.config.values['lista_immagine_da_trovare'].listaImmagini[image_index];

    self.currentGame = message;

    self.clue_scope.startGame = function() {

        self.hideClue(function() {
            self.startGame();    
        });
        
    };

    self.clue_scope.forfaitGame = function() {
        
        self.hideClue(function() {
            self.doForfaitGame();    
        });
        
    };

    self.clue_scope.clueWnd = self.ionicModal.fromTemplate(

        '<ion-modal-view padding="true" class="game-generic-modal" cache-view="false">' +

            '<ion-content scroll="true" padding="false" >' +

                '<div class="game-generic-inner">' +
                    
                    '<div><h2>' + self.config.name + '</h2></div>' +
                    '<div><h3>' + self.config.description + '</h3></div>' +

                    '<div><h2>Il tuo avversario è</h2></div>' +
                    '<div><h3>' + self.currentGame.user + '</h3></div>' +
                    
                    '<div><h2>L\'indizio è:</h2></div>' +
                    '<div><h3>' + image.indizio + '</h3></div>' +                        

                '</div>' +

            '</ion-content>' +

            '<ion-footer-bar align-title="left" class="game-generic-footer row">' +
                '<div class="col">' +
                    '<button ng-click="forfaitGame()" class="button button-full button-assertive cst-button">Mi Arrendo...</button>' +
                '</div>' +
                '<div class="col">' +
                    '<button ng-click="startGame()" class="button button-full button-balanced cst-button">Ok Iniziamo!</button>' +
                '</div>' +
            '</ion-footer-bar>' +

            '</ion-modal-view>',

        {
            scope: self.clue_scope,
            focusFirstInput: true,
            animation :'none',
            hardwareBackButtonClose: false,
            backdropClickToClose: false
        }

    );

    self.clue_scope.clueWnd.show();

}



// ################################################################################## //
// messaggio ricevuto quando provo ad accettare una sfida che nel frattempo è scaduta //
// ################################################################################## //
client_game_trova_immagine_gz.prototype.onReceiveGameRequestExpiredOrCancelled = function(message) {

    var self = this;

    self.ionicPopup.alert({
        title: 'Troppo Tardi',              
        template: 'La sfida lanciata da "' + message.from.user + '" al gioco "' + self.config.name + '" non è più disponibile...' ,
        okText: 'Ok', 
        okType: 'button-assertive'            
    });

};


client_game_trova_immagine_gz.prototype.onReceiveGameRequestAck = function(message) {

    var self = this;

    if(message.status == 'OK') {

        self.waitBox = self.ionicPopup.alert({
          title: 'Attendi che un utente accetti la sfida',
          template: 
            '<div>Puoi premere su \'Annulla\' per interrompere la tua richiesta di gioco<div>' + 
            '<div style="text-align:center;"><ion-spinner icon="lines"></ion-spinner></div>',
          okText: 'Annulla', 
          okType: 'button-assertive'            
        });

        self.hasPendingRequest = true;

        self.waitBox.then(function() {

            self.waitBox = null;
          
            if(self.hasPendingRequest == true) {

                self.hasPendingRequest = false;

                self.SocketService.emit('message', 'cancel_match_make', {}, {     
                    key: message.key,   
                    user: self.rootScope.game_center.nickname,
                    uid: self.GlobalVariables.deviceUUID,
                    identifier: message.identifier
                });
          
            }
          
        });

    } else if(message.status == 'KO') {

        self.waitBox = self.ionicPopup.alert({
            title: 'La tua richiesta di gioco è stata respinta',
            template: message.reason,
            okText: 'Chiudi'          
        });

    }

}



client_game_trova_immagine_gz.prototype.onGamePause = function(message) {
 
    var self = this;

    try {

        if(self.waitBox) {

            self.waitBox.close();

            self.SocketService.emit('message', 'cancel_match_make', {}, {     
                key: self.config.key,   
                user: self.rootScope.game_center.nickname,
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto
            });

        }

        if((self.clue_scope && self.clue_scope.clueWnd) || self.isInArMode == true) {

            self.SocketService.emit('message', 'forfait_game', {}, {  

                match_id: self.currentGame.match_id,  
                key: self.config.key,   
                uid: self.GlobalVariables.deviceUUID,
                identifier: self.GlobalVariables.application.currentProgetto.idProgetto

            });


        }


    } catch(err) {

        alert(err);

    }   
    


}

client_game_trova_immagine_gz.prototype.onGameResume = function(message) {
    
    var self = this;

    try {

        if(self.clue_scope && self.clue_scope.clueWnd) {
            
            self.hideClue(function() { 
                self.assertEnding({
                    isWon: false, 
                    isForfait: true
                });
            });

        }

        if(self.isInArMode == true) {

            self.isInArMode = false;

            setTimeout(function() {

                self.WikiService.sendMessageToAR({
                    type: 'closeAR'
                });

                self.assertEnding({
                    isWon: false, 
                    isForfait: true
                });
            

            }, 2000);
                

        }

    } catch(err) {

        alert(err);
    
    }
       
 
}






